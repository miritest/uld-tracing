<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="description" content="ULD Tracing Template v.1">
	<meta name="author" content="Miri Yusifli & Akbar Kazimov">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
    <title>ULD tracing</title>
 
    <!-- start: Css -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">

      <!-- plugins -->
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/plugins/font-awesome.min.css"/>
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/plugins/animate.min.css"/>
	  <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
	<!-- end: Css -->

	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/logomi.png">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

 <body id="mimin" class="dashboard">
      <!-- start: Header -->
        <nav class="navbar navbar-default header navbar-fixed-top">
          <div class="col-md-12 nav-wrapper">
            <div class="navbar-header" style="width:100%;">
              <div class="opener-left-menu is-open">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
              </div>
                <a href="index.html" class="navbar-brand"> 
                 <b>ULD tracing</b>
                </a>

              <ul class="nav navbar-nav search-nav">
                <li>
                   <div class="search">
                    <span class="fa fa-search icon-search" style="font-size:23px;"></span>
                    <div class="form-group form-animate-text">
                      <input type="text" id="findULD" class="form-text" required>
                      <span class="bar"></span>
                      <label class="label-search">Type anywhere to <b>Search</b> </label>
                    </div>
                  </div>
                </li>
              </ul>

              <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name"><span>Miri Yusifli</span></li>
                  <li class="dropdown avatar-dropdown">
                   <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                   <ul class="dropdown-menu user-dropdown">
                     <li><a href="#"><span class="fa fa-user"></span> My Profile</a></li>
                     <li><a href="#"><span class="fa fa-calendar"></span> My Calendar</a></li>
                     <li role="separator" class="divider"></li>
                     <li class="more">
                      <ul>
                        <li><a href=""><span class="fa fa-cogs"></span></a></li>
                        <li><a href=""><span class="fa fa-lock"></span></a></li>
                        <li><a href=""><span class="fa fa-power-off "></span></a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li ><a href="#" class="opener-right-menu"><span class="fa fa-coffee"></span></a></li>
              </ul>
            </div>
          </div>
        </nav>
        <div class="container-fluid mimin-wrapper">
  
          <!-- start:Left Menu -->
            <div id="left-menu">
              <div class="sub-left-menu scroll">
                <ul class="nav nav-list">
                    <li><div class="left-bg"></div></li>
                    <li class="time">
                      <h1 class="animated fadeInLeft">21:00</h1>
                      <p class="animated fadeInRight">Sat,October 1st 2029</p>
                    </li>
                    <li class="active ripple">
                      <a href="/UCM-controller/home"><span class="fa-home fa"></span>Home</a>
                    </li>
                     <li class="active ripple">
                      <a href="/UCM-controller/extra"><span class="fa fa-cogs"></span>Extra</a>
                    </li>
                    <li class="ripple">
                    	<a href="/UCM-controller/ULDtable"><span class="fa fa-table"></span> ULD Table  </a>
                      <!-- <ul class="nav nav-list tree">
                        <li><a href="datatables.html">Data Tables</a></li>
                        <li><a href="handsontable.html">handsontable</a></li>
                        <li><a href="tablestatic.html">Static</a></li>
                      </ul> -->
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa fa-pencil-square"></span>Messages
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="/UCM-controller/message/messages">All Messages</a></li>
                        <li><a href="/UCM-controller/message/new_UCM">New UCM message</a></li>
                        <li><a href="/UCM-controller/message/new_SCM">New SCM message</a></li>
                        <li><a href="/UCM-controller/message/new_LUC">New LUC message</a></li>
                        <li><a href="/UCM-controller/message/new_custom">New custom message</a></li>
                        
                      </ul>
                    </li>
                   <!--  <li class="ripple"><a class="tree-toggle nav-header">
                    <span class="fa-area-chart fa"></span> ULD Control<span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="#">Overview</a></li>
                        <li><a href="#">New ULD</a></li>
                        <li><a href="#">ULD Report</a></li>
                        <li><a href="#">ULD Tags</a></li>
                        <li><a href="#">Leased ULDs</a></li>
                      </ul>
                    </li>
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-check-square-o"></span> Reports  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="#">UCM Reports</a></li>
                        <li><a href="#">UCM statistics</a></li>
                        <li><a href="#">SCM Reports</a></li>
                        <li><a href="#">ULD Balance Report</a></li>
                        <li><a href="#">ULD Missing Statistics</a></li>
                      </ul>
                    </li>
                    
                    <li class="ripple"><a href="calendar.html"><span class="fa fa-calendar-o"></span>Stock</a></li>
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-envelope-o"></span> Repairs <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="#">ULDs</a></li>
                        <li><a href="#">Send to Repair</a></li>
                        <li><a href="#">Repair Forms</a></li>
                        <li><a href="#">Repair Report</a></li>
                        <li><a href="#">Repair Stations</a></li>
                	 </ul>
                    </li>
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-file-code-o"></span> Agents  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="#">ULDs</a></li>
                        <li><a href="#">Send to Agent</a></li>
                        <li><a href="#">Agent Stations</a></li>
                      </ul>
                    </li>
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa "></span> Admin  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="#">Platform Management</a></li>
                        <li><a href="#">Flight Schedules</a></li>
                        <li><a href="#">Damages overview</a></li>
                      </ul>
</li>                    
    -->             </div>
            </div>
          <!-- end: Left Menu -->

  		<!--  <div id="content">
                <div class="panel">
                  <div class="panel-body">
                      <div class="col-md-6 col-sm-12">
                        -->

                       <!-- <div class="dropdown">
  <button class="dropbtn">New message</button>
  <div class="dropdown-content">
    <a href="#"> UCM message</a>
    <a href="#"> SCM message</a>
    <a href="#"> LUC message</a>
    <a href="#"> CPM message</a>
    <a href="#"> custom message</a>
    
    
  </div>
</div> -->




                    </div>
                  
                 
  		
        
        <body>
        
 <jsp:include page="${partial}.jsp" />

</body>
<script src="${pageContext.request.contextPath}/resources/js/plugins/jquery.autocomplete.min.js"></script>
<script type="text/javascript">
$('#findULD').autocomplete({
	
	serviceUrl: '/UCM-controller/ajax/connectULD',
	paramName: "tagname",
	delimiter: ",",
   transformResult: function(response) {
	   

	return {
	  //must convert json to javascript object before process
	  suggestions: $.map($.parseJSON(response), function(item) {

	      return { value:item.type+item.code+item.owner };

	      s
	   })
	   
	   

	}

        }
 });


</script>

      <!-- end: Header -->