    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/plugins/simple-line-icons.css"/>
    


<!-- modal ucun fayllar -->
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<!-- modal ucun fayllar -->




  
  	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<div id="content">

   
             
            <div class="col-md-12 padding-0">
              <div class="col-md-12 padding-0">
                <div class="col-md-12 padding-0">
                  <div class="panel box-shadow-none content-header">
                      <div class="panel-body">
                        <div class="col-md-12">
                            <h3 class="animated fadeInLeft">Messages</h3>
                            <p class="animated fadeInDown" style="line-height:.4;">
                              Welcome To Messages.
                            </p>
                        </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-6">
                <div class="col-md-12">
                
                <div class="panel box-v6">
                     <div class="panel-heading">
                       <h4>Message Details
                      </h4>
                     </div>
                   <div class="responsive-table">
                      <table id="myTable" class="table table-striped">
               <thead>
                 
    </thead>
    <tbody>
      <tr>
        <td>Status</td>
        <td>
          <div class="col-md-6">
          <c:choose>
   			 <c:when test="${message.status=='parsed'}">
				<span class="label label-success label-large">Parsed</span>   			 
			</c:when>   
   			  <c:when test="${message.status=='warning'}">
   			    <span class="label label-warning label-large">Warning</span>
   			 </c:when> 
   			 <c:otherwise>
                 <span class="label label-danger label-large">Error</span>
       
    		</c:otherwise>
		</c:choose>
         
          </div>
        </td>
      </tr>
      <tr>
        <td>Type</td>
        <td>${message.type}</td>
      </tr>
       <c:choose> 
   			 <c:when test="${  empty message.paletActions ||  message.type=='SCM'}">
   			 </c:when>    
   			 <c:otherwise>
                 <tr>
        <td>Carrier</td>
        <td>${message.paletActions.iterator().next().carrier }</td>
      </tr>
       <tr>
        <td>Flight Number</td>
        <td>${message.paletActions.iterator().next().flightNo }</td>
      </tr>
       
    		</c:otherwise>
		</c:choose>
      <tr>
        <td>Sender</td>
        <td>${message.sender}</td>
      </tr>
      <tr>
        <td>Email</td>
        <td>${message.fromEmail }</td>
      </tr>
      <tr>
        <td>Subject</td>
        <td>${message.subject }</td>
      </tr>
      <tr>
        <td>Location</td>
        <c:if test="${not empty message.paletActions}">
        <td>${message.paletActions.iterator().next().arriving }</td>
       </c:if> 
      </tr>
      <tr>
        <td>Receive Date</td>
        <td>${message.receiveDate }</td>
      </tr>
      
        <c:if test = "${message.status!='parsed'}">
      <tr>
        <td>Reason</td>
        <td>${message.reason_user }</td>
      </tr>
       </c:if>
      
      
      

    </tbody>
  </table>
</div>
                      </div>
              </div>
              
                              <div class="col-md-12">
                              	 <div class="col-md-6">
                              		 <div class="panel-body">
                                		<canvas class="doughnut-chart"></canvas>
                             		 </div>
                             	 </div>
                             	 <div class="col-md-6">
                             		 <div class="panel-body">
                                  		<canvas class="pie-chart"></canvas>
                            		  </div>
                             	 </div>
              </div>
              </div>
              
              <div class="col-md-6">
               <div class="panel-heading padding-3 bg-white border-none">
                      <h3 class="animated fadeInLeft" style="margin-left: 2%;">Message</h3>

                       <textarea rows="25" cols="70" disabled >${message.text}</textarea></div>
                    
              </div>
              </div>
            </div>
            <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Data Tables</h3></div>
                    <div class="panel-body">
                      <div class="responsive-table">
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Palet</th>
                          <th>Action Type</th>
                          <th>FlightNo</th>
                          <th>Palet Location</th>
                          <th>Error</th>
                          <th></th>
                          <th></th>
                          

                        </tr>
                      </thead>
                      <tbody>
<!--                       //burda loop uje sehv olacaq js gonderende
 -->                       <c:forEach items="${message.paletActions}" var="paletAction" varStatus="loop">
   							<c:forEach items="${paletAction.actionErrorSet}" var="error" varStatus="loop">
   									
                        <tr id="${loop.index}">
                          <td>${error.palet}</td>
                          <td>${error.action_type }</td>
                          <td>${error.flightNo }</td>
                          <td>${error.palet_location }</td>
                          <td>${error.error}</td>
                          <c:choose>
                          
                           <c:when test = "${error.solved}">
                           <td><center> <span class="label label-success label-large" style="font-size: medium;">SOLVED </span></center></td>
                           <td></td>
                          </c:when>
                          <c:otherwise>
                          <td><center><input type="button" class=" btn btn-round btn-primary" id="connectButton" onClick="connectButton('${error.palet }' , '${loop.index}')"  value="Connect ULD" data-toggle="modal" data-target="#modalConnect" /><br><br>
                          <input type="button" class="btn btn-round btn-warning" id="createButton" value="Create ULD" onClick="createButton('${error.palet}' , '${loop.index}')" data-toggle="modal" data-target="#modalSure" /></center></td>
                          <td><center><input type="button" class="btn btn-round btn-danger" value="Ignore" data-toggle="modal" onClick="ignoreButton('${error.ID}' , '${loop.index}')" data-target="#modalIgnore"/></center></td>
                          </c:otherwise>
                          </c:choose>
                        </tr>
                         </c:forEach>
                         </c:forEach>
                      
                      </tbody>
                        </table>
                      </div>
                  </div>
                </div>
              </div>  
              
           
             
             
             
             
             
              <!-- Modal -->
  <div class="modal fade" id="modalSure" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body-Sure">
 			 
        </div>
        <div class="modal-footer">        
        <input type="text" class="form-control" id="create-input">
        	<br>
          <input type="submit" class="btn btn-primary" data-dismiss="modal" value="OK" name="palet" id="create-yes">
		 </div>
      </div>
      
    </div>
  </div>
  
  
 			 <input type="hidden" id="create-palet" name="create-palet" value="">
 			 <input type="hidden" id="create-old-palet" name="create-palet" value="">

 			 <input type="hidden" id="create-error" name="create-errorID" value="">
 			 <input type="hidden" id="create-row" name="create-rowIndex" value="">
 			 <input type="hidden" id="ignore-error" name="create-rowIndex" value="">
 			 
 			 
 			 
 			 
  
  
    <div class="modal fade" id="modalConnect" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body-Connect">
 			 <input type="hidden" id="connect-palet" name="connect-palet" value="">
 			 <input type="hidden" id="connect-row" name="connect-rowIndex" value="">
 			 
 			         </div>
        <div class="modal-footer">
       <div class="col-md-12">
			<div class="form-group col-md-6">
  				<input type="text" id="findConnectedULD" class="form-control autocomplete"  style="text-transform:uppercase">
			</div>
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="connect-OK">OK</button>
		 </div>
        </div>
      </div>
      
      
      
    </div>
  </div>
  
  
  
   <div class="modal fade" id="modalIgnore" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Are u sure to ignore this palet?</h4>
        </div>
       
        <div class="modal-footer">
       <div class="col-md-12">
		
		  <button type="button" class="btn btn-primary" data-dismiss="modal" id="ignore-OK">YES</button>
		 
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="connect-OK">NO</button>
		 </div>
        </div>
      </div>
      
      
      
    </div>
  </div>
  
  
 
  
  
  <!-- Modal HTML  Successs-->
<div id="myModal" class="modal fade">
	<div class="modal-dialog modal-success">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="material-icons">&#xE876;</i>
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body text-center">
				<h4>Success!</h4>	
				<br>
				<p>Process completed successfully.</p>
			</div>
		</div>
	</div>
</div> 
  
  
  <!-- Modal HTML Error -->
<div id="myModalError" class="modal fade">
	<div class="modal-dialog modal-error">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="material-icons">&#xE5CD;</i>
				</div>				
				<h4 class="modal-title">Sorry</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p>Palet exists,You cannot insert again!</p>
			</div>
			
		</div>
	</div>
</div>     
  
  
   <div class="modal loading-modal" style="display: none">
   		 <div class="center loading-center">
       			<img alt="" src="${pageContext.request.contextPath}/resources/img/loader.gif" />
   		 </div>
	 </div>
  
  
    
             
              </div>
          </div>
          
    
             


          <!-- end: content -->



          <!-- start: right menu -->
            <div id="right-menu">
              <ul class="nav nav-tabs">
                <li class="active">
                 <a data-toggle="tab" href="#right-menu-user">
                  <span class="fa fa-comment-o fa-2x"></span>
                 </a>
                </li>
                <li>
                 <a data-toggle="tab" href="#right-menu-notif">
                  <span class="fa fa-bell-o fa-2x"></span>
                 </a>
                </li>
                <li>
                  <a data-toggle="tab" href="#right-menu-config">
                   <span class="fa fa-cog fa-2x"></span>
                  </a>
                 </li>
              </ul>

              <div class="tab-content">
                <div id="right-menu-user" class="tab-pane fade in active">
                  <div class="search col-md-12">
                    <input type="text" placeholder="search.."/>
                  </div>
                  <div class="user col-md-12">
                   <ul class="nav nav-list">
                    <li class="online">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="gadget">
                        <span class="fa  fa-mobile-phone fa-2x"></span> 
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="away">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="gadget">
                        <span class="fa  fa-desktop"></span> 
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="offline">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="offline">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="online">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="gadget">
                        <span class="fa  fa-mobile-phone fa-2x"></span> 
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="offline">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="online">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="gadget">
                        <span class="fa  fa-mobile-phone fa-2x"></span> 
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="offline">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="offline">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="online">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="gadget">
                        <span class="fa  fa-mobile-phone fa-2x"></span> 
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="online">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="gadget">
                        <span class="fa  fa-mobile-phone fa-2x"></span> 
                      </div>
                      <div class="dot"></div>
                    </li>

                  </ul>
                </div>
                <!-- Chatbox -->
                <div class="col-md-12 chatbox">
                  <div class="col-md-12">
                    <a href="#" class="close-chat">X</a><h4>Akihiko Avaron</h4>
                  </div>
                  <div class="chat-area">
                    <div class="chat-area-content">
                      <div class="msg_container_base">
                        <div class="row msg_container send">
                          <div class="col-md-9 col-xs-9 bubble">
                            <div class="messages msg_sent">
                              <p>that mongodb thing looks good, huh?
                                tiny master db, and huge document store</p>
                                <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                              </div>
                            </div>
                            <div class="col-md-3 col-xs-3 avatar">
                              <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" class=" img-responsive " alt="user name">
                            </div>
                          </div>

                          <div class="row msg_container receive">
                            <div class="col-md-3 col-xs-3 avatar">
                              <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" class=" img-responsive " alt="user name">
                            </div>
                            <div class="col-md-9 col-xs-9 bubble">
                              <div class="messages msg_receive">
                                <p>that mongodb thing looks good, huh?
                                  tiny master db, and huge document store</p>
                                  <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                                </div>
                              </div>
                            </div>

                            <div class="row msg_container send">
                              <div class="col-md-9 col-xs-9 bubble">
                                <div class="messages msg_sent">
                                  <p>that mongodb thing looks good, huh?
                                    tiny master db, and huge document store</p>
                                    <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                                  </div>
                                </div>
                                <div class="col-md-3 col-xs-3 avatar">
                                  <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" class=" img-responsive " alt="user name">
                                </div>
                              </div>

                              <div class="row msg_container receive">
                                <div class="col-md-3 col-xs-3 avatar">
                                  <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" class=" img-responsive " alt="user name">
                                </div>
                                <div class="col-md-9 col-xs-9 bubble">
                                  <div class="messages msg_receive">
                                    <p>that mongodb thing looks good, huh?
                                      tiny master db, and huge document store</p>
                                      <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                                    </div>
                                  </div>
                                </div>

                                <div class="row msg_container send">
                                  <div class="col-md-9 col-xs-9 bubble">
                                    <div class="messages msg_sent">
                                      <p>that mongodb thing looks good, huh?
                                        tiny master db, and huge document store</p>
                                        <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                                      </div>
                                    </div>
                                    <div class="col-md-3 col-xs-3 avatar">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" class=" img-responsive " alt="user name">
                                    </div>
                                  </div>

                                  <div class="row msg_container receive">
                                    <div class="col-md-3 col-xs-3 avatar">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" class=" img-responsive " alt="user name">
                                    </div>
                                    <div class="col-md-9 col-xs-9 bubble">
                                      <div class="messages msg_receive">
                                        <p>that mongodb thing looks good, huh?
                                          tiny master db, and huge document store</p>
                                          <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="chat-input">
                                <textarea placeholder="type your message here.."></textarea>
                              </div>
                              <div class="user-list">
                                <ul>
                                  <li class="online">
                                    <a href=""  data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <div class="user-avatar"><img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name"></div>
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="offline">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="away">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="online">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="offline">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="away">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="offline">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="offline">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="away">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="online">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="away">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="away">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div id="right-menu-notif" class="tab-pane fade">

                            <ul class="mini-timeline">
                              <li class="mini-timeline-highlight">
                               <div class="mini-timeline-panel">
                                <h5 class="time">07:00</h5>
                                <p>Coding!!</p>
                              </div>
                            </li>

                            <li class="mini-timeline-highlight">
                             <div class="mini-timeline-panel">
                              <h5 class="time">09:00</h5>
                              <p>Playing The Games</p>
                            </div>
                          </li>
                          <li class="mini-timeline-highlight">
                           <div class="mini-timeline-panel">
                            <h5 class="time">12:00</h5>
                            <p>Meeting with <a href="#">Clients</a></p>
                          </div>
                        </li>
                        <li class="mini-timeline-highlight mini-timeline-warning">
                         <div class="mini-timeline-panel">
                          <h5 class="time">15:00</h5>
                          <p>Breakdown the Personal PC</p>
                        </div>
                      </li>
                      <li class="mini-timeline-highlight mini-timeline-info">
                       <div class="mini-timeline-panel">
                        <h5 class="time">15:00</h5>
                        <p>Checking Server!</p>
                      </div>
                    </li>
                    <li class="mini-timeline-highlight mini-timeline-success">
                      <div class="mini-timeline-panel">
                        <h5 class="time">16:01</h5>
                        <p>Hacking The public wifi</p>
                      </div>
                    </li>
                    <li class="mini-timeline-highlight mini-timeline-danger">
                      <div class="mini-timeline-panel">
                        <h5 class="time">21:00</h5>
                        <p>Sleep!</p>
                      </div>
                    </li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                  </ul>

                </div>
                <div id="right-menu-config" class="tab-pane fade">
                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Notification</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-info">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch1" checked>
                        <label class="onoffswitch-label" for="myonoffswitch1"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Custom Designer</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-danger">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch2" checked>
                        <label class="onoffswitch-label" for="myonoffswitch2"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Autologin</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-success">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch3" checked>
                        <label class="onoffswitch-label" for="myonoffswitch3"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Auto Hacking</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-warning">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch4" checked>
                        <label class="onoffswitch-label" for="myonoffswitch4"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Auto locking</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch5" checked>
                        <label class="onoffswitch-label" for="myonoffswitch5"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>FireWall</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch6" checked>
                        <label class="onoffswitch-label" for="myonoffswitch6"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>CSRF Max</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-warning">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch7" checked>
                        <label class="onoffswitch-label" for="myonoffswitch7"></label>
                      </div>
                    </div>
                  </div>


                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Man In The Middle</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-danger">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch8" checked>
                        <label class="onoffswitch-label" for="myonoffswitch8"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Auto Repair</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-success">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch9" checked>
                        <label class="onoffswitch-label" for="myonoffswitch9"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <input type="button" value="More.." class="btnmore">
                  </div>

                </div>
              </div>
            </div>  
          <!-- end: right menu -->
          
      </div>

      <!-- start: Mobile -->
      <div id="mimin-mobile" class="reverse">
        <div class="mimin-mobile-menu-list">
            <div class="col-md-12 sub-mimin-mobile-menu-list animated fadeInLeft">
                <ul class="nav nav-list">
                    <li class="active ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa-home fa"></span>Dashboard 
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                          <li><a href="dashboard-v1.html">Dashboard v.1</a></li>
                          <li><a href="dashboard-v2.html">Dashboard v.2</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa-diamond fa"></span>Layout
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="topnav.html">Top Navigation</a></li>
                        <li><a href="boxed.html">Boxed</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa-area-chart fa"></span>Charts
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="chartjs.html">ChartJs</a></li>
                        <li><a href="morris.html">Morris</a></li>
                        <li><a href="flot.html">Flot</a></li>
                        <li><a href="sparkline.html">SparkLine</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa fa-pencil-square"></span>Ui Elements
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="color.html">Color</a></li>
                        <li><a href="weather.html">Weather</a></li>
                        <li><a href="typography.html">Typography</a></li>
                        <li><a href="icons.html">Icons</a></li>
                        <li><a href="buttons.html">Buttons</a></li>
                        <li><a href="media.html">Media</a></li>
                        <li><a href="panels.html">Panels & Tabs</a></li>
                        <li><a href="notifications.html">Notifications & Tooltip</a></li>
                        <li><a href="badges.html">Badges & Label</a></li>
                        <li><a href="progress.html">Progress</a></li>
                        <li><a href="sliders.html">Sliders</a></li>
                        <li><a href="timeline.html">Timeline</a></li>
                        <li><a href="modal.html">Modals</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                       <span class="fa fa-check-square-o"></span>Forms
                       <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="formelement.html">Form Element</a></li>
                        <li><a href="#">Wizard</a></li>
                        <li><a href="#">File Upload</a></li>
                        <li><a href="#">Text Editor</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa fa-table"></span>Tables
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="datatables.html">Data Tables</a></li>
                        <li><a href="handsontable.html">handsontable</a></li>
                        <li><a href="tablestatic.html">Static</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a href="calendar.html">
                         <span class="fa fa-calendar-o"></span>Calendar
                      </a>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa fa-envelope-o"></span>Mail
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="mail-box.html">Inbox</a></li>
                        <li><a href="compose-mail.html">Compose Mail</a></li>
                        <li><a href="view-mail.html">View Mail</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa fa-file-code-o"></span>Pages
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="forgotpass.html">Forgot Password</a></li>
                        <li><a href="login.html">SignIn</a></li>
                        <li><a href="reg.html">SignUp</a></li>
                        <li><a href="article-v1.html">Article v1</a></li>
                        <li><a href="search-v1.html">Search Result v1</a></li>
                        <li><a href="productgrid.html">Product Grid</a></li>
                        <li><a href="profile-v1.html">Profile v1</a></li>
                        <li><a href="invoice-v1.html">Invoice v1</a></li>
                      </ul>
                    </li>
                     <li class="ripple"><a class="tree-toggle nav-header"><span class="fa "></span> MultiLevel  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="view-mail.html">Level 1</a></li>
                        <li><a href="view-mail.html">Level 1</a></li>
                        <li class="ripple">
                          <a class="sub-tree-toggle nav-header">
                            <span class="fa fa-envelope-o"></span> Level 1
                            <span class="fa-angle-right fa right-arrow text-right"></span>
                          </a>
                          <ul class="nav nav-list sub-tree">
                            <li><a href="mail-box.html">Level 2</a></li>
                            <li><a href="compose-mail.html">Level 2</a></li>
                            <li><a href="view-mail.html">Level 2</a></li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li><a href="credits.html">Credits</a></li>
                  </ul>
            </div>
        </div>       
      </div>
      <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
        <span class="fa fa-bars"></span>
      </button>
       <!-- end: Mobile -->


<script type="text/javascript">

function createButton(palet,rowIndex) {
    document.querySelector('#modalSure .modal-title').innerHTML="Are you sure create "+palet+"?";
   	document.querySelector('#create-input').value=palet;
    document.querySelector('#create-palet').value=palet;
    document.querySelector('#create-row').value=rowIndex;
    document.querySelector('#create-old-palet').value=palet;

    

}

function connectButton(palet,rowIndex) {
    document.querySelector('.modal-body-Connect #connect-palet').value=palet;
    document.querySelector('.modal-body-Connect #connect-row').value=rowIndex;
	$("#findConnectedULD").val('');


}

function ignoreButton(errorID,rowIndex) {
   	document.querySelector('#ignore-error').value=errorID;
    document.querySelector('#create-row').value=rowIndex;


    

}






</script>
































<!-- start: Javascript -->
<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.ui.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>



<!-- plugins -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/jquery.autocomplete.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/flot/jquery.flot.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/jquery.datatables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/datatables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/flot/jquery.flot.time.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/flot/jquery.flot.navigate.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/flot/jquery.flot.stack.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/plugins/chart.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/plugins/jquery.nicescroll.js"></script>


<!-- custom -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();
 
    
    $('#findConnectedULD').autocomplete({
    	
		serviceUrl: '/UCM-controller/ajax/connectULD',
		paramName: "tagname",
		delimiter: ",",
	   transformResult: function(response) {
		   

		return {
		  //must convert json to javascript object before process
		  suggestions: $.map($.parseJSON(response), function(item) {

		      return { value: item.type+item.code+item.owner };

		   })
		   

		}

            }
	 });

   
    $("#ignore-OK").click(function(code){
    	var errorID=$("#ignore-error").val();
       	var rowIndex =$("#create-row").val();

    	
    	
    		$.ajax({
    			  type: "POST",
    			  url: "/UCM-controller/ajax/ignore-OK",
    			  data: {errorID:errorID},
    		      //contentType : 'application/json',
      			  success:function(data){
   
    				if(data==true){
						$('table#datatables-example tr#'+rowIndex).find('td:nth-child(6)').html('<center> <span class="label label-success label-large" style="font-size: medium;">SOLVED </span></center>');
    					
    					$('table#datatables-example tr#'+rowIndex).find('td:nth-child(7)').html('');
    					    					$("#myModal").modal();

    					
    				}
    				
    				else {
    					$("#myModalError").modal();

    				}
    				
    			  }
            
            		

    			  });
    	
    });

   
    
    $("#connect-OK").click(function(code){
    	var connectedpalet=$("#findConnectedULD").val();
    	var palet=$("#connect-palet").val();
    	var rowIndex =$("#connect-row").val();
    	
    	
    	if(connectedpalet.length<10){alert("ULD should be at least 10 character.");}
    	else{
    	
    	
    		$.ajax({
    			  type: "POST",
    			  url: "/UCM-controller/ajax/Connect-OK-ULD",
    			  data: {palet: palet,connectedpalet:connectedpalet},
    		      //contentType : 'application/json',
      			  success:function(data){
   
    				if(data==true){
						$('table#datatables-example tr#'+rowIndex).find('td:nth-child(6)').html('<center> <span class="label label-success label-large" style="font-size: medium;">SOLVED </span></center>');
    					
    					$('table#datatables-example tr#'+rowIndex).find('td:nth-child(7)').html('');
    					    					$("#myModal").modal();

    					
    				}
    				
    				else {
    					$("#myModalError").modal();

    				}
    				
    			  }
            
            		

    			  });
    	}
    });

	
    
    
	
    $("#create-yes").click(function(code){
    	
     	var palet=$("#create-input").val();
       	var rowIndex =$("#create-row").val();
       	var palet_old=$("#create-old-palet").val();
       	
    		$.ajax({
    			  type: "POST",
    			  url: "/UCM-controller/ajax/createULD",
    			  data: {palet: palet,palet_old:palet_old},
    		      //contentType : 'application/json',
    		       beforeSend: function () {
               $(".modal").show();
            },
            complete: function () {
                $(".modal").hide();
            }, 		
      			  success:function(data){
    				if(data==true){
						$('table#datatables-example tr#'+rowIndex).find('td:nth-child(6)').html('<center> <span class="label label-success label-large" style="font-size: medium;">SOLVED </span></center>');
    					
    					$('table#datatables-example tr#'+rowIndex).find('td:nth-child(7)').html('');
    					
    					$("#myModal").modal();

    				}
    				else {

    					
    					$("#myModalError").modal();

    				}
    			  }
            
            		

    			  });
    });

    
    
  });
  
  
</script>
     <script type="text/javascript">
      (function(jQuery){
       
      
   

        <c:if test="${not empty typeStat && not empty ownerStat}">
       
  
         var doughnutData1 = ${typeStat}
        var doughnutData = ${ownerStat}

        
        
             window.onload = function(){
                var ctx = $(".doughnut-chart")[0].getContext("2d");
                window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
                    responsive : true,
                    showTooltips: true
                });

                var ctx2 = $(".pie-chart")[0].getContext("2d");
                window.myPie = new Chart(ctx2).Pie(doughnutData1, {
                    responsive : true,
                    showTooltips: true
                });

               
            };
            
            
            </c:if>
            
        })(jQuery);
     </script>
