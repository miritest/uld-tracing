  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/plugins/simple-line-icons.css"/>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/plugins/jquery.steps.css"/>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/plugins/bootstrap-material-datetimepicker.css"/>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

  
    <div id="content">
             <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                     <div class="col-md-12">
                        <h3 class="animated fadeInLeft">Messages</h3>
                        <p class="animated fadeInDown">
                         Messages <span class="fa-angle-right fa"></span> New SCM message
                        </p>
                    </div>                  </div>
              </div>
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading"></div>
                    <div class="panel-body">
                        <form id="example1" action="#">
                            <h3>Account</h3>
                            <fieldset>
                              <legend>Account Information</legend>
                              <div class="col-md-12 col-xs-12">
                                <div class="col-md-6 col-xs-6">
                                  <select class="form-control" name="receiver" id="receiver" required >
                                    <option value="">Receiver</option>
                                    
                                     <c:forEach items="${owners}" var="owner">
        								 <option value="${owner}">${owner}</option>
   									 </c:forEach>
                                     
                                  </select>
                                </div>

                              <div class="col-md-6 col-xs-6">
                                  <select class="form-control" name="transferredBy" id="transferredBy" required >
                                    <option value="">Transferred By</option>
                                    
                                    <c:forEach items="${owners}" var="owner">
        								 <option value="${owner}">${owner}</option>
   									 </c:forEach>
                                     
                                    
                                  </select>
                              </div>
                              </div>
                              <br>
                              <br>
                              <br>
                              <br>
                              <br>
                              <br>
                              <div class="col-md-12">
                                 <div class="col-md-6">
                                        <select class="form-control" name="destination" id="destination" required >
                                          <option value="">Original Destination</option>
                                             <c:forEach items="${airports}" var="airport">
        								 <option value="${airport.iata}">${airport.iata}-${airport.name}(${airport.country})</option>

                                       		</c:forEach>
                                        </select>
                                    </div>
                                <div class="col-md-6">
                                        <select class="form-control" name="location" id="location" required >
                                          <option value="">Transfer Location</option>
                                            <c:forEach items="${airports}" var="airport">
        								 <option value="${airport.iata}">${airport.iata}-${airport.name}(${airport.country})</option>

                                       		</c:forEach>
                                        </select>
                                    </div>

                              </div>
                              <br>
                              <br>
                              <br>
                              <br>
                              <br>
                              <br>
                               <div class="col-md-12">
                                     <div class="form-animate-text col-md-6">
                                  <input type="text" class="form-text datetime"  id="date" required>
                                  <span class="bar"></span>
                                  <label><span class="fa fa-calendar"></span> Date Time Picker</label>
                                </div>
                                    
                                   </div>     

                            </fieldset>
                          <h3>Palets</h3>
                            <fieldset style="overflow: scroll;">
                              <div class="col-md-12">
                                <div class="form-group form-animate-text col-md-3 " style="margin-top:40px !important;">
                                  <input type="number" class="form-text" id="code" maxlength="5" >
                                  <span class="bar"></span>
                                  <label>Code</label>
                                </div>
                                <div class="form-group form-animate-text col-md-3 " style="margin-top:40px !important;">
                                  <select class="form-control" id="type">
                                    <option value="">Type</option>
                                    <c:forEach items="${types}" var="type">
        								 <option value="${type}">${type}</option>
   									 </c:forEach>
                                  </select>
                                </div>
                                <div class="form-group form-animate-text col-md-3 " style="margin-top:40px !important;">
                                   <select class="form-control" id="owner">
                                    <option value="">Owner</option>
                                    <c:forEach items="${owners}" var="owner">
        								 <option value="${owner}">${owner}</option>
   									 </c:forEach>
                                  </select>
                                </div>
                                <div class="form-group form-animate-text col-md-3" style="margin-top:40px !important;">
                                  <select class="form-control" id="airport">
                                    <option value="">Airport</option>
                                    <c:forEach items="${airportList}" var="airport">
        								 <option value="${airport.iata}">${airport.iata}-${airport.name}(${airport.country})</option>
   									 </c:forEach>
                                  </select>
                                </div>
                              </div>
                          

                              <input type="button" class=" btn btn-round btn-primary" id="search" value="Search"/>
                              <br>
                              
                              
                              <div>
                              <input type="button" class=" btn btn-round btn-primary" value="Add all" onclick="addAll()" />

                              </div>
                               
                               <div class="col-md-12"> 


                              <div class="responsive-table col-md-6">
                                <h3>Found</h3>
                                <table id="founded"  class="table table-striped table-bordered">
                                  <thead>
                                   
                                  </thead>
                                  <tbody>
                                
                                  </tbody>
                                </table>
                              </div> 


                              <div class="responsive-table col-md-6">
                                       <h3  style="display:inline-block;">Selected </h3>
                                       <!-- inline-block -->
                                        <div class="checkbox"  id="makeAllServiceable"  style="display:none;margin-left:47%;">
  												<label><input type="checkbox" checked onChange="makeAllServicable(this.checked)" >Make All Serviceable</label>
											</div>
					
                                <table id="selected" class="table table-striped table-bordered" >
                                  <thead>
                                   
                                  </thead>
                                  <tbody>
                                   
                                  </tbody>
                                </table>
                              </div> 
                              
				                   <div class="modal loading-modal" style="display: none">
   										 <div class="center loading-center">
       										 <img alt="" src="${pageContext.request.contextPath}/resources/img/loader.gif" />
   										 </div>
								   </div>


							</div>
    
                  </fieldset>
                              <h3>Result</h3>
                            <fieldset>
                              <legend>Result</legend>
                             
                              <textarea rows="17" cols="100"  id="message" name="message" >
                              
                              </textarea>
                              
                            </fieldset>
                         
                            
                        </form>


                    </div>
                </div>
            </div>
          </div>
        <!-- end: content -->


    
          <!-- start: right menu -->
            <div id="right-menu">
              <ul class="nav nav-tabs">
                <li class="active">
                 <a data-toggle="tab" href="#right-menu-user">
                  <span class="fa fa-comment-o fa-2x"></span>
                 </a>
                </li>
                <li>
                 <a data-toggle="tab" href="#right-menu-notif">
                  <span class="fa fa-bell-o fa-2x"></span>
                 </a>
                </li>
                <li>
                  <a data-toggle="tab" href="#right-menu-config">
                   <span class="fa fa-cog fa-2x"></span>
                  </a>
                 </li>
              </ul>

              <div class="tab-content">
                <div id="right-menu-user" class="tab-pane fade in active">
                  <div class="search col-md-12">
                    <input type="text" placeholder="search.."/>
                  </div>
                  <div class="user col-md-12">
                   <ul class="nav nav-list">
                    <li class="online">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="gadget">
                        <span class="fa  fa-mobile-phone fa-2x"></span> 
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="away">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="gadget">
                        <span class="fa  fa-desktop"></span> 
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="offline">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="offline">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="online">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="gadget">
                        <span class="fa  fa-mobile-phone fa-2x"></span> 
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="offline">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="online">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="gadget">
                        <span class="fa  fa-mobile-phone fa-2x"></span> 
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="offline">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="offline">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="online">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="gadget">
                        <span class="fa  fa-mobile-phone fa-2x"></span> 
                      </div>
                      <div class="dot"></div>
                    </li>
                    <li class="online">
                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                      <div class="name">
                        <h5><b>Bill Gates</b></h5>
                        <p>Hi there.?</p>
                      </div>
                      <div class="gadget">
                        <span class="fa  fa-mobile-phone fa-2x"></span> 
                      </div>
                      <div class="dot"></div>
                    </li>

                  </ul>
                </div>
                <!-- Chatbox -->
                <div class="col-md-12 chatbox">
                  <div class="col-md-12">
                    <a href="#" class="close-chat">X</a><h4>Akihiko Avaron</h4>
                  </div>
                  <div class="chat-area">
                    <div class="chat-area-content">
                      <div class="msg_container_base">
                        <div class="row msg_container send">
                          <div class="col-md-9 col-xs-9 bubble">
                            <div class="messages msg_sent">
                              <p>that mongodb thing looks good, huh?
                                tiny master db, and huge document store</p>
                                <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                              </div>
                            </div>
                            <div class="col-md-3 col-xs-3 avatar">
                              <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" class=" img-responsive " alt="user name">
                            </div>
                          </div>

                          <div class="row msg_container receive">
                            <div class="col-md-3 col-xs-3 avatar">
                              <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" class=" img-responsive " alt="user name">
                            </div>
                            <div class="col-md-9 col-xs-9 bubble">
                              <div class="messages msg_receive">
                                <p>that mongodb thing looks good, huh?
                                  tiny master db, and huge document store</p>
                                  <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                                </div>
                              </div>
                            </div>

                            <div class="row msg_container send">
                              <div class="col-md-9 col-xs-9 bubble">
                                <div class="messages msg_sent">
                                  <p>that mongodb thing looks good, huh?
                                    tiny master db, and huge document store</p>
                                    <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                                  </div>
                                </div>
                                <div class="col-md-3 col-xs-3 avatar">
                                  <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" class=" img-responsive " alt="user name">
                                </div>
                              </div>

                              <div class="row msg_container receive">
                                <div class="col-md-3 col-xs-3 avatar">
                                  <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" class=" img-responsive " alt="user name">
                                </div>
                                <div class="col-md-9 col-xs-9 bubble">
                                  <div class="messages msg_receive">
                                    <p>that mongodb thing looks good, huh?
                                      tiny master db, and huge document store</p>
                                      <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                                    </div>
                                  </div>
                                </div>

                                <div class="row msg_container send">
                                  <div class="col-md-9 col-xs-9 bubble">
                                    <div class="messages msg_sent">
                                      <p>that mongodb thing looks good, huh?
                                        tiny master db, and huge document store</p>
                                        <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                                      </div>
                                    </div>
                                    <div class="col-md-3 col-xs-3 avatar">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" class=" img-responsive " alt="user name">
                                    </div>
                                  </div>

                                  <div class="row msg_container receive">
                                    <div class="col-md-3 col-xs-3 avatar">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" class=" img-responsive " alt="user name">
                                    </div>
                                    <div class="col-md-9 col-xs-9 bubble">
                                      <div class="messages msg_receive">
                                        <p>that mongodb thing looks good, huh?
                                          tiny master db, and huge document store</p>
                                          <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="chat-input">
                                <textarea placeholder="type your message here.."></textarea>
                              </div>
                              <div class="user-list">
                                <ul>
                                  <li class="online">
                                    <a href=""  data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <div class="user-avatar"><img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name"></div>
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="offline">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="away">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="online">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="offline">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="away">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="offline">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="offline">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="away">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="online">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="away">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                  <li class="away">
                                    <a href="" data-toggle="tooltip" data-placement="left" title="Akihiko avaron">
                                      <img src="${pageContext.request.contextPath}/resources/img/avatar.jpg" alt="user name">
                                      <div class="dot"></div>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div id="right-menu-notif" class="tab-pane fade">

                            <ul class="mini-timeline">
                              <li class="mini-timeline-highlight">
                               <div class="mini-timeline-panel">
                                <h5 class="time">07:00</h5>
                                <p>Coding!!</p>
                              </div>
                            </li>

                            <li class="mini-timeline-highlight">
                             <div class="mini-timeline-panel">
                              <h5 class="time">09:00</h5>
                              <p>Playing The Games</p>
                            </div>
                          </li>
                          <li class="mini-timeline-highlight">
                           <div class="mini-timeline-panel">
                            <h5 class="time">12:00</h5>
                            <p>Meeting with <a href="#">Clients</a></p>
                          </div>
                        </li>
                        <li class="mini-timeline-highlight mini-timeline-warning">
                         <div class="mini-timeline-panel">
                          <h5 class="time">15:00</h5>
                          <p>Breakdown the Personal PC</p>
                        </div>
                      </li>
                      <li class="mini-timeline-highlight mini-timeline-info">
                       <div class="mini-timeline-panel">
                        <h5 class="time">15:00</h5>
                        <p>Checking Server!</p>
                      </div>
                    </li>
                    <li class="mini-timeline-highlight mini-timeline-success">
                      <div class="mini-timeline-panel">
                        <h5 class="time">16:01</h5>
                        <p>Hacking The public wifi</p>
                      </div>
                    </li>
                    <li class="mini-timeline-highlight mini-timeline-danger">
                      <div class="mini-timeline-panel">
                        <h5 class="time">21:00</h5>
                        <p>Sleep!</p>
                      </div>
                    </li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                  </ul>

                </div>
                <div id="right-menu-config" class="tab-pane fade">
                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Notification</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-info">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch1" checked>
                        <label class="onoffswitch-label" for="myonoffswitch1"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Custom Designer</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-danger">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch2" checked>
                        <label class="onoffswitch-label" for="myonoffswitch2"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Autologin</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-success">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch3" checked>
                        <label class="onoffswitch-label" for="myonoffswitch3"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Auto Hacking</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-warning">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch4" checked>
                        <label class="onoffswitch-label" for="myonoffswitch4"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Auto locking</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch5" checked>
                        <label class="onoffswitch-label" for="myonoffswitch5"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>FireWall</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch6" checked>
                        <label class="onoffswitch-label" for="myonoffswitch6"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>CSRF Max</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-warning">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch7" checked>
                        <label class="onoffswitch-label" for="myonoffswitch7"></label>
                      </div>
                    </div>
                  </div>


                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Man In The Middle</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-danger">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch8" checked>
                        <label class="onoffswitch-label" for="myonoffswitch8"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6 padding-0">
                      <h5>Auto Repair</h5>
                    </div>
                    <div class="col-md-6">
                      <div class="mini-onoffswitch onoffswitch-success">
                        <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="myonoffswitch9" checked>
                        <label class="onoffswitch-label" for="myonoffswitch9"></label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <input type="button" value="More.." class="btnmore">
                  </div>

                </div>
              </div>
            </div>  
          <!-- end: right menu -->
          
      </div>

      <!-- start: Mobile -->
      <div id="mimin-mobile" class="reverse">
        <div class="mimin-mobile-menu-list">
            <div class="col-md-12 sub-mimin-mobile-menu-list animated fadeInLeft">
                <ul class="nav nav-list">
                    <li class="active ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa-home fa"></span>Dashboard 
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                          <li><a href="dashboard-v1.html">Dashboard v.1</a></li>
                          <li><a href="dashboard-v2.html">Dashboard v.2</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa-diamond fa"></span>Layout
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="topnav.html">Top Navigation</a></li>
                        <li><a href="boxed.html">Boxed</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa-area-chart fa"></span>Charts
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="chartjs.html">ChartJs</a></li>
                        <li><a href="morris.html">Morris</a></li>
                        <li><a href="flot.html">Flot</a></li>
                        <li><a href="sparkline.html">SparkLine</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa fa-pencil-square"></span>Ui Elements
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="color.html">Color</a></li>
                        <li><a href="weather.html">Weather</a></li>
                        <li><a href="typography.html">Typography</a></li>
                        <li><a href="icons.html">Icons</a></li>
                        <li><a href="buttons.html">Buttons</a></li>
                        <li><a href="media.html">Media</a></li>
                        <li><a href="panels.html">Panels & Tabs</a></li>
                        <li><a href="notifications.html">Notifications & Tooltip</a></li>
                        <li><a href="badges.html">Badges & Label</a></li>
                        <li><a href="progress.html">Progress</a></li>
                        <li><a href="sliders.html">Sliders</a></li>
                        <li><a href="timeline.html">Timeline</a></li>
                        <li><a href="modal.html">Modals</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                       <span class="fa fa-check-square-o"></span>Forms
                       <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="formelement.html">Form Element</a></li>
                        <li><a href="#">Wizard</a></li>
                        <li><a href="#">File Upload</a></li>
                        <li><a href="#">Text Editor</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa fa-table"></span>Tables
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="datatables.html">Data Tables</a></li>
                        <li><a href="handsontable.html">handsontable</a></li>
                        <li><a href="tablestatic.html">Static</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a href="calendar.html">
                         <span class="fa fa-calendar-o"></span>Calendar
                      </a>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa fa-envelope-o"></span>Mail
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="mail-box.html">Inbox</a></li>
                        <li><a href="compose-mail.html">Compose Mail</a></li>
                        <li><a href="view-mail.html">View Mail</a></li>
                      </ul>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa fa-file-code-o"></span>Pages
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li><a href="forgotpass.html">Forgot Password</a></li>
                        <li><a href="login.html">SignIn</a></li>
                        <li><a href="reg.html">SignUp</a></li>
                        <li><a href="article-v1.html">Article v1</a></li>
                        <li><a href="search-v1.html">Search Result v1</a></li>
                        <li><a href="productgrid.html">Product Grid</a></li>
                        <li><a href="profile-v1.html">Profile v1</a></li>
                        <li><a href="invoice-v1.html">Invoice v1</a></li>
                      </ul>
                    </li>
                     <li class="ripple"><a class="tree-toggle nav-header"><span class="fa "></span> MultiLevel  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="view-mail.html">Level 1</a></li>
                        <li><a href="view-mail.html">Level 1</a></li>
                        <li class="ripple">
                          <a class="sub-tree-toggle nav-header">
                            <span class="fa fa-envelope-o"></span> Level 1
                            <span class="fa-angle-right fa right-arrow text-right"></span>
                          </a>
                          <ul class="nav nav-list sub-tree">
                            <li><a href="mail-box.html">Level 2</a></li>
                            <li><a href="compose-mail.html">Level 2</a></li>
                            <li><a href="view-mail.html">Level 2</a></li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li><a href="credits.html">Credits</a></li>
                  </ul>
            </div>
        </div>       
      </div>
      <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
        <span class="fa fa-bars"></span>
      </button>
       <!-- end: Mobile -->

<!-- start: Javascript -->
<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.ui.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/myJs.js"></script>



<!-- plugins -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/jquery.steps.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/jquery.nicescroll.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/select2.full.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/bootstrap-material-datetimepicker.js"></script>



<!-- custom -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function(){



    var form = $("#example1").show();
     
    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex)
            {
                return true;
            }
            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18)
            {
                return false;
            }
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex)
            {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {
        	
        	if(currentIndex == 2){
		      	generateMessage();

			}
        	
        	
            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
            {
                form.steps("next");
            }
            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3)
            {
                form.steps("previous");
            }
        },
        onFinishing: function (event, currentIndex)
        {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            alert("Submitted!");
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password-2"
            }
        }
    });
    
  // date i avto insert
    var weekdays=new Array(7);
    weekdays[0]="Monday";
    weekdays[1]="Tuesday";
    weekdays[2]="Wednesday";
    weekdays[3]="Thursday";
    weekdays[4]="Friday";
    weekdays[5]="Saturday";
    weekdays[6]="Sunday";

    var months = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];



        var now = new Date();
     
        var day = ("0" + now.getDate()).slice(-2);
        
        var hour=now.getHours();
        var minute=now.getMinutes();

       
        if(hour<10){hour="0"+hour;}
  		if(minute<10){minute="0"+minute;}

        var today = weekdays[now.getDay()]+" "+day+" "+months[now.getMonth()]+" "+now.getFullYear()+" - "+(hour)+":"+(minute) ;
       


       $('#date').val(today);
   

   

  });
  
  function changePalette(select){
	  var text=select.options[select.selectedIndex].value;
  	  var x = select.closest('tr').rowIndex;	  	  
      var table1 = document.getElementById("founded");
      
      
  	  if(x==0){
  		 for (var i = 0; i < table1.rows.length; i++) {

  		      table1.rows[i].cells[0].innerHTML=table1.rows[i].cells[0].innerHTML.split('/')[0]+"/"+text;
  		     // alert(table1.rows[i].cells[2].firstChild.value=text);
  		      table1.rows[i].cells[2].firstChild.value=text;


  		 }
  	  }
  	  else {
      table1.rows[x].cells[0].innerHTML=table1.rows[x].cells[0].innerHTML.split('/')[0]+"/"+text;
  	  }
	
  }
  
  
  function add(column) {
	  

		/* $.each(airports, function(idx, obj) {
			alert(obj);
		}); */
		
		
  	
  	
  	  var x = column.closest('tr').rowIndex;	 
      var table1 = document.getElementById("founded");
      var table2 = document.getElementById("selected");
      
      if(table2.rows.length==0){
    	  document.getElementById('makeAllServiceable').style.display = 'inline-block';

      }
      //test
	      
      
      
      
      

    var cell=table1.rows[x].cells[0].innerHTML;
      table1.deleteRow(x);

      var row = table2.insertRow(0);
      var cell0 = row.insertCell(0);
      var cell1 = row.insertCell(1);
	  var cell2 = row.insertCell(2);


      cell0.innerHTML = cell;
      cell1.innerHTML = "<input type='button' onclick='deleteRow(this);' class='btn btn-round btn-primary' value='Delete'/>";
      cell2.innerHTML = " <div class='checkbox'> <label><input type='checkbox' class='serviceable' checked>Servicable</label></div>";


      
  }

  
  
  

  function generateMessage(){
  	 var month=['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];
       
  	   	
  		var result="LUC\n";
  		
  		var date=$("#date").val().split(" ");
  		
  		var hour=date[5].split(":")[0];
  		var minute=date[5].split(":")[1];
  		
  		if(hour<10){hour="0"+hour;}
  		if(minute<10){minute="0"+minute;}
  		
  		
  		date=date[1]+date[2].substring(0,3).toUpperCase()+date[3].substring(2,4)+"/"+hour+minute;

  		 var table2 = document.getElementById("selected");
	      var rows=table2.rows;
	      var palets = Array.prototype.slice.call( rows )
		
  		
	      var receiver=$("#receiver").val();
	      var transferredBy=$("#transferredBy").val();
	      var destination=$("#destination").val();
	      var location=$("#location").val();
  		
  		
  		for (var i =0; i <palets.length;i++) {
    		
    		result=result+palets[i].cells[0].innerHTML+"/"+date+"/"+receiver+"/"+transferredBy+"/"+location+"/000-00000000/"+destination+"/";
    		
    		if(palets[i].getElementsByTagName("td")[2].getElementsByTagName('input')[0].checked==true){
    			result=result+"SER";
    		}
    		else{result=result+"DAM";}
    		
    		result=result+"\n";
    	}
	      
	      
  		
		$("#message").val(result);

  		
  }
function makeAllServicable(checked){
	
	var elements=document.getElementsByClassName("serviceable");
	
	for(var i=0; i<elements.length; i++) {
	    elements[i].checked = checked;
	}
	

}
  
  
  
  
  
  
  function deleteRow(column) {
	  //var iatas = ${iatas};
		
		
	  
	  
  	var x = column.closest('tr').rowIndex;	 

      var table1 = document.getElementById("founded");
      var table2 = document.getElementById("selected");

      if(table2.rows.length==1){
    	  document.getElementById('makeAllServiceable').style.display = 'none';

      }
      
      
      var cell=table2.rows[x].cells[0].innerHTML;

      var row = table1.insertRow(0);
          table2.deleteRow(x);

      var cell0 = row.insertCell(0);
      var cell1 = row.insertCell(1);
      var cell2 = row.insertCell(2);
      cell0.innerHTML = cell;
      cell1.innerHTML = "<input type='button' class='btn btn-round btn-primary' value='Add'/>";
      cell1.onclick=function(){return add(cell1);}

      
  }

  function addAll(){

      var table1 = document.getElementById("founded");
      var table2 = document.getElementById("selected");
      var rows=table1.rows;
    
      for (var i =0; i <rows.length;) {
        add(rows[i]);
         
      
  }


  }



  function check(x){
  	var table1 = document.getElementById("selected");

  var rows=table1.rows;




  for (var i =0; i <rows.length;i++) {
    var cell=rows[i].cells[0].innerHTML;
    if(cell==x)return false;

  }
  return true;
  }
  
  
  
  
  
  //ajax
  
  
$(document).ready(function(){
	
    $("#search").click(function(code){
       
    	
    	
    	
    	
    	
    	var code=$("#code").val();
    	var type=$("#type").val();
    	var owner=$("#owner").val();
    	var airport=$("#airport").val();
		
    		$.ajax({
    			  type: "POST",
    			  url: "/UCM-controller/ajax/findULD",
    			  data: {code: code,type: type, owner: owner, position: airport},
    		      //contentType : 'application/json',
    		      beforeSend: function () {
               $(".modal").show();
            },
            complete: function () {
                $(".modal").hide();
            }, 		
    			  success:function(data){
    				
    				  
    				  if($("#founded tbody").children().length!=0){
    				  $("#founded tbody").empty();}
    				 
    				   $.each(JSON.parse(data), function(idx, obj) {
     					  if(check(obj.type+obj.code+obj.owner)){
     						  
     						  var row="<tr><td>"+obj.type+obj.code+obj.owner+"</td>"+
                               "<td><input type='button' class='btn btn-round btn-primary' value='Add'  onclick='add(this)' /></td>"
     						  $("#founded tbody").append(row);
     						  
     						  
     						  
     						  
     					
     					  
     					  }
     					  
     				  }); 
    				  

    	  
    			  }
            
            		

    			  });
    });




    $('.dateAnimate').bootstrapMaterialDatePicker({ weekStart : 0, time: false,animation:true});
    $('.date').bootstrapMaterialDatePicker({ weekStart : 0, time: false});
    $('.time').bootstrapMaterialDatePicker({ date: false,format:'HH:mm',animation:true});
    $('.datetime').bootstrapMaterialDatePicker({ format : 'dddd DD MMMM YYYY - HH:mm',animation:true});
    $('.date-fr').bootstrapMaterialDatePicker({ format : 'DD/MM/YYYY HH:mm', lang : 'fr', weekStart : 1, cancelText : 'ANNULER'});
    $('.min-date').bootstrapMaterialDatePicker({ format : 'DD/MM/YYYY HH:mm', minDate : new Date() });




      
});

  


</script>
<!-- end: Javascript -->
</body>
</html>