<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
      	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      	<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
      	
    

 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/plugins/datatables.bootstrap.min.css"/>


          <div id="content">
            <div class="tabs-wrapper text-center">             
             <div class="panel box-shadow-none text-left content-header">
                  <div class="panel-body" style="padding-bottom:0px;">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">ULD information</h3>
                        <p class="animated fadeInDown">
                         ULD <span class="fa-angle-right fa"></span> detail information
                        </p>
                    </div>
                   
                  </div>
              </div>
            <div class="col-md-12 tab-content">

              <div role="tabpanel" class="tab-pane fade active in" id="tabs-area-demo" aria-labelledby="tabs1">
                <div class="col-md-12">
                  <div class="col-md-12">
                    <div class="col-md-12 tabs-area">
                      <div class="liner"></div>
                      <ul class="nav nav-tabs nav-tabs-v5" id="tabs-demo6">
                        <li class="active">
                         <a href="#tabs-demo6-area1" data-toggle="tab" title="detail">
                          <span class="round-tabs one">
                            <i class="glyphicon glyphicon-info-sign"></i>
                          </span> 
                        </a>
                      </li>

                      <li>
                        <a href="#tabs-demo6-area2" data-toggle="tab" title="messages">
                         <span class="round-tabs two">
                           <i class="glyphicon glyphicon-envelope"></i>
                         </span> 
                       </a>
                     </li>

                     <li>
                      <a href="#tabs-demo6-area3" data-toggle="tab" title="logs">
                       <span class="round-tabs three">
                        <i class="glyphicon glyphicon-calendar"></i>
                      </span> </a>
                    </li>
<!-- 
                    <li>
                      <a href="#tabs-demo6-area4" data-toggle="tab" title="blah blah">
                       <span class="round-tabs four">
                         <i class="glyphicon glyphicon-comment"></i>
                       </span> 
                     </a>
                   </li>

                   <li><a href="#tabs-demo6-area5" data-toggle="tab" title="completed">
                     <span class="round-tabs five">
                      <i class="glyphicon glyphicon-ok"></i>
                    </span> </a>
                  </li> -->
                </ul>
                <div class="tab-content tab-content-v5">
                  <div class="tab-pane fade in active" id="tabs-demo6-area1">







                    <h3 class="head text-center">Information about <span style="text-decoration: underline;">${palet.type}${palet.code}${palet.owner}</span></h3>
                    <h4 class="narrow text-center">
                      You can get detailed information about 
                    </h4>
 <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Detail Information</h3></div>
                    <div class="panel-body">
                      <div class="responsive-table">
                      <table id="palet-table" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <!-- <th>Attribute</th>
                          <th>Value</th>    -->
                       </tr>
                      </thead>
                      <tbody>
                       <tr>
                          <td width=50%; >ULD</td>
                          <td>${palet.type}${palet.code}${palet.owner}</td>   
                       </tr>
                         <tr>
                          <td>Type</td>
                          <td>${palet.type}</td>   
                       </tr>
                        <tr>
                          <td>Owner</td>
                          <td>${palet.owner}</td>   
                       </tr>
                        <tr>
                          <td>Current airport</td>
                          <td>${palet.position}</td>   
                       </tr>
                       <tr>
                          <td>ULD status</td>
                            <c:choose>
   			 <c:when test="${palet.active==true}">
                          <td><span class="label label-success label-large">Active</span></td>
   			 </c:when>   
   			  <c:when test="${palet.active==false}">
                        <td><span class="label label-warning label-large">Not Active</span></td>
   			 </c:when> 
   			  
		</c:choose>
                       </tr>
                      <tr>
                          <td>Last Message Received</td>
                          <td id="last-message"></td>
                       </tr>
                       <tr>
                          <td>Creation date</td>
                          <td>${palet.insert_date}</td>   
                       </tr>
                       <tr>
                          <td>Dwelling time</td>
                          <td id="dwelling"></td>   
                       </tr>
                      </tbody> 
                        </table>
                      </div>
                  </div>
                </div>
              </div>  
              </div>





<!-- 
                    <p class="text-center">
                      <a href="" class="btn btn-success btn-round green"> start using Mimin <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></a>
                    </p> -->
                  </div>
                  <div class="tab-pane fade" id="tabs-demo6-area2">
                    <h3 class="head text-center"><span style="text-decoration: underline;">${palet.type}${palet.code}${palet.owner}</span> founded in these messages</h3>
                    <p class="narrow text-center">
                    </p>

 <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Messages</h3></div>
                    <div class="panel-body">
                      <div class="responsive-table">
                    <table id="message-table" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Type</th>
                       	  <th>Message Date</th>
                          <th>Email</th>   
                          <th>Subject</th>
                          <th>Sender</th>
                          <th>Status</th>
                          
                          
                        </tr>
                      </thead>
                      <tbody>
                      
                         <c:forEach items="${palet.paletActions}" var="paletAction">
                      
                        <tr>
                          <td><a href="/UCM-controller/message/message_detail?id=${paletAction.message.ID}">${paletAction.message.type}</a></td>
                          <td><fmt:formatDate value="${paletAction.message.receiveDate}" pattern="dd-MMM-yyyy" /></td>
                          <td>${paletAction.message.fromEmail}</td>
                          <td>${paletAction.message.subject}</td>
                          <td>${paletAction.message.sender}</td>
                               <c:choose>
   			 <c:when test="${paletAction.message.status=='parsed'}">
                          <td><span class="label label-success label-large">Parsed</span></td>
   			 </c:when>   
   			  <c:when test="${paletAction.message.status=='warning'}">
                        <td><span class="label label-warning label-large">Warning</span></td>
   			 </c:when> 
   			  <c:when test="${paletAction.message.status=='error'}">
                        <td><span class="label label-danger label-large">Error</span></td>
       
    		</c:when>
		</c:choose>
                   </tr>
                       </c:forEach>
                      
                      </tbody>
                        </table>
                      </div>
                  </div>
                </div>
              </div>  
              </div>


<!-- 
                    <p class="text-center">
                      <a href="" class="btn btn-success btn-round green"> create your profile <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></a>
                    </p>
 -->
                  </div>
                  <div class="tab-pane fade" id="tabs-demo6-area3">
                    <h3 class="head text-center"><span style="text-decoration: underline;">${palet.type}${palet.code}${palet.owner}</span> Logs</h3>


 <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Logs</h3></div>
                    <div class="panel-body">
                      <div class="responsive-table">
                       <table id="logs-table" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                    	  <th>Carrier</th>
                    	  <th>Flight Number</th>
                    	  <th>Date</th>
                          <th>Location</th>   
                          <th>Destination</th>
                          
                          
                        </tr>
                      </thead>
                      <tbody>
                       <c:forEach items="${palet.paletActions}" var="paletAction">
                      
                        <tr>
                          <td>${paletAction.carrier}</td>
                          <td>${paletAction.flightNo}</td>                       
                          <td><fmt:formatDate value="${paletAction.flightDate}"  pattern="dd-MMM-yyyy" /></td>
	                      <td>${paletAction.arriving}</td>
                          <td>${paletAction.departing}</td>
                        </tr>
                       </c:forEach>
                      
                      </tbody>
                        </table> --%>
                      </div>
                  </div>
                </div>
              </div>  
              </div>









                   <!--  <p class="text-center">
                      <a href="" class="btn btn-success btn-round green"> start using Mimin <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></a>
                    </p> -->
                  </div>
                  <div class="tab-pane fade" id="tabs-demo6-area4">
                    <h3 class="head text-center">Drop comments!</h3>
                    <p class="narrow text-center">
                      Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                    </p>

                    <p class="text-center">
                      <a href="" class="btn btn-success btn-round green"> start using Mimin <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></a>
                    </p>
                  </div>
                  <div class="tab-pane fade" id="tabs-demo6-area5">
                    <div class="text-center">
                      <i class="img-intro icon-checkmark-circle"></i>
                    </div>
                    <h3 class="head text-center">thanks for staying tuned! <span style="color:#f48250;">♥</span> Bootstrap</h3>
                    <p class="narrow text-center">
                      Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                    </p>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>      
              </div>
             
              
                </div>
               
              </div>
            </div>
          </div>

<!-- start: Javascript -->
<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.ui.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>


<!-- plugins -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/jquery.nicescroll.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/jquery.datatables.min.js"></script>

<!-- datatable buttons -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/dataTables.buttons.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/buttons.html5.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/jszip.min.js"></script>


 <script src="//cdn.datatables.net/plug-ins/1.10.16/sorting/date-dd-MMM-yyyy.js"></script>
 
<!-- custom -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	
	 
  
	  $('#logs-table').DataTable({
		    "columnDefs" : [{"targets":2, type: 'date-dd-mmm-yyyy'}],
		    "bProcessing": true,
	        "bServerSide": false,
	        "iDisplayLength": 25,
		    "dom": 'Bfrtip',
	        "buttons": [
	            { extend: 'csv', className: 'btn btn-info glyphicon glyphicon-save' },
	            { extend: 'excel', className: 'btn btn-info glyphicon glyphicon-save' }
	        ],
	         
	        "paging":   false,

	    	 "order": [[ 2, "desc" ]]

	  });
	  
	  
	  $('#message-table').DataTable({
		  
		    "bProcessing": true,
	        "bServerSide": false,
	        "iDisplayLength": 25,

		    "columnDefs" : [{"targets":1,type: 'date-dd-mmm-yyyy'}],
		    "dom": 'Bfrtip',
	        "buttons": [
	            { extend: 'csv', className: 'btn btn-info glyphicon glyphicon-save' },
	            { extend: 'excel', className: 'btn btn-info glyphicon glyphicon-save' }
	        ],
	        "paging":   false,

	    	 "order": [[ 1, "desc" ]]

	  });
	  	  
    $(".nav-tabs a").click(function (e) {
      e.preventDefault();  
      $(this).tab('show');
    });

    
    
    

    
    var x = document.getElementById("logs-table").rows[1].cells[2].innerHTML;
    x=x.split("-")[2]+"-"+x.split("-")[1]+"-"+x.split("-")[0];
   
    
    var sdt = new Date(x);
    
    var difdt = new Date(new Date() - sdt);
   var result=(difdt.toISOString().slice(0, 4) - 1970) + "Y " + (difdt.getMonth()) + "M " + (difdt.getDate()) + "D";

$("#palet-table #last-message").html(document.getElementById("message-table").rows[1].cells[1].innerHTML);


   
   
$("#palet-table #dwelling").html(result);

   

  });
  
  
  
 

 
</script>
<!-- end: Javascript -->

</body>
</html>