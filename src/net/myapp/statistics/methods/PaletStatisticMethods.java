package net.myapp.statistics.methods;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.myapp.dao.form.models.Statistic;
import net.myapp.dao.models.Palet;
import net.myapp.statistics.model.PaletStatistics;

public class PaletStatisticMethods {

	public static PaletStatistics getPaletStatistics(Set<Palet>palets,boolean typeStatistic,boolean ownerStatistic){
		List<String>types=null;
		List<String>owners=null;
		
		if(typeStatistic) {types=new ArrayList<>();}
		if(ownerStatistic){owners=new ArrayList<>();}

		PaletStatistics paletStatistics=new PaletStatistics();
		
		if (palets==null) {
			return null;
		}
		for (Palet palet : palets) {
			if(typeStatistic){
				types.add(palet.getType());

			}
			if(ownerStatistic){
			owners.add(palet.getOwner());
			}
		}
		
		if(typeStatistic){
		Set<Statistic>statistics=new HashSet<>();
		Set<String> uniqueSet = new HashSet<String>(types);
		int i=0;
		for (String temp : uniqueSet) {
			Statistic statistic=new Statistic(Collections.frequency(types, temp),Statistic.getColor(i),temp);
			statistics.add(statistic);
			i++;

		}
		paletStatistics.setTypeStatistics(statistics);
		}
		
		if(ownerStatistic){
		Set<Statistic>statistics=new HashSet<>();
		Set<String> uniqueSet = new HashSet<String>(owners);
		int i=0;
		
		for (String temp1 : uniqueSet) {
			Statistic statistic=new Statistic(Collections.frequency(owners, temp1),Statistic.getColor(i),temp1);
			statistics.add(statistic);
			i++;
		}
		paletStatistics.setOwnerStatistics(statistics);

		}
		
		return paletStatistics;
		
	}
}
