package net.myapp.statistics.model;

import java.util.List;
import java.util.Set;

import net.myapp.dao.form.models.Statistic;

public class PaletStatistics {

	private Set<Statistic>typeStatistics;
	private Set<Statistic>ownerStatistics;
	
	public PaletStatistics(Set<Statistic> typeStatistics, Set<Statistic> ownerStatistics) {
		super();
		this.typeStatistics = typeStatistics;
		this.ownerStatistics = ownerStatistics;
	}

	public PaletStatistics() {
		super();
	}

	public Set<Statistic> getTypeStatistics() {
		return typeStatistics;
	}

	public void setTypeStatistics(Set<Statistic> typeStatistics) {
		this.typeStatistics = typeStatistics;
	}

	public Set<Statistic> getOwnerStatistics() {
		return ownerStatistics;
	}

	public void setOwnerStatistics(Set<Statistic> ownerStatistics) {
		this.ownerStatistics = ownerStatistics;
	}
	
	
}
