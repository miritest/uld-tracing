package net.myapp.spring.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;

import net.myapp.dao.form.methods.FormDAO_ADT;
import net.myapp.utils.StaticVariables;


public class RunScheduler {
	@Autowired(required=true)
	@Qualifier("FormDAO")
	private FormDAO_ADT formDAO ; 
    
    
   //@Scheduled( initialDelay = 1 * 1000, fixedDelay = 1 * 1000)
	@Scheduled(cron="0 0 0/1 * * *")
    public void writeCurrentTime() {
       System.out.println("-----------Background Task Running----------------");
		StaticVariables.fillFormList(formDAO);
        System.out.println("-----------Background Task Ending----------------");

		
		
         
        
         
    }
 
}
