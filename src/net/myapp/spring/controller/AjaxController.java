package net.myapp.spring.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import net.myapp.common.logging.impl.Log;
import net.myapp.common.web.holders.RequestHelper;
import net.myapp.dao.methods.ErrorDAO_ADT;
import net.myapp.dao.methods.MessageDAO_ADT;
import net.myapp.dao.methods.PaletDAO_ADT;
import net.myapp.dao.models.Message;
import net.myapp.dao.models.ActionError;
import net.myapp.dao.models.Palet;
import net.myapp.dao.models.PaletAction;
import net.myapp.dao.required.columns.MessageColumns;
import net.myapp.dao.required.columns.PaletColumns;
import net.myapp.exceptions.MyException;
import net.myapp.utils.ParsingTools;

@Controller
@RequestMapping("/ajax")
public class AjaxController {


@Autowired(required=true)
@Qualifier("PaletDAO")
private PaletDAO_ADT paletDAO ;


@Autowired(required=true)
@Qualifier("MessageDAO")
private MessageDAO_ADT messageDAO;


@Autowired(required=true)
@Qualifier("ErrorDAO")
private ErrorDAO_ADT errorDAO;


	@ResponseBody
	@RequestMapping(value="/findULD")
	public String findULD(Palet palet){
		PaletColumns paletColumns=new PaletColumns();
		paletColumns.setType(true);
		paletColumns.setCode(true);
		paletColumns.setOwner(true);
		
		List<Palet>paletList= paletDAO.getPaletsByCriteria(palet,paletColumns,-1);
		
		
    	ObjectMapper objectMapper = new ObjectMapper();
    	String json="";
		try {
			json = objectMapper.writeValueAsString(paletList);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return json;
		
		
	}
	
	@ResponseBody
	@RequestMapping(value="/getMessage")
	public String getMessages(HttpServletRequest  request){
		//Fetch the page number from client
		Integer page = 0;
    	if (null != request.getParameter("start"))
          page = (Integer.valueOf(request.getParameter("start")) / 10) + 1;
    	
		
		ObjectMapper objectMapper = new ObjectMapper();

    	String json="";
   
    		MessageColumns columns=new MessageColumns();
		columns.setText(true);
		columns.setSender(true);
		columns.setSubject(true);
		columns.setFromEmail(true);
		columns.setStatus(true);
		columns.setID(true);
		columns.setType(true);
		columns.setReceiveDate(true);
		List<Message>messageList=messageDAO.getMessagesByCriteria(null,columns,page);
		
		try {
			
			json = objectMapper.writeValueAsString(messageList);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return json;
		
		
	}
	
	@ResponseBody
	@RequestMapping(value="/getPalets")
	public String getPalets(HttpServletRequest  request){
		//Fetch the page number from client
		Integer page = 0;
    	if (null != request.getParameter("start"))
          page = (Integer.valueOf(request.getParameter("start")) / 10) + 1;
    	
		
		ObjectMapper objectMapper = new ObjectMapper();

    	String json="";
		
			PaletColumns paletColumns=new PaletColumns();
			paletColumns.setID(true);
			paletColumns.setType(true);
			paletColumns.setCode(true);
			paletColumns.setOwner(true);
			paletColumns.setPosition(true);
			paletColumns.setChecked(true);
			paletColumns.setActive(true);
			paletColumns.setReason(true);
			paletColumns.setInsert_Date(true);
			
			List<Palet>messageList=paletDAO.getPaletsByCriteria(null,paletColumns, page);
			
			try {
				json = objectMapper.writeValueAsString(messageList);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		
		
		return json;
		
		
	}
	
	
	@ResponseBody
	@RequestMapping(value="/createULD")
	public boolean createULD(String palet,String palet_old){
		
		try {
			System.out.println("OLD PALET IS "+palet_old);
			return messageDAO.solveErrorByCreatingPalet(ParsingTools.parseSinglePalet(palet),ParsingTools.parseSinglePalet(palet_old));
		} catch (MyException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			Log.error(e.getReason());
			return false;
		}
		
		
		
	}
	
	@ResponseBody
	@RequestMapping(value="/connectULD")
	public List<Palet> connectULD(String tagname){
	
			PaletColumns paletColumns=new PaletColumns();
			paletColumns.setType(true);
			paletColumns.setCode(true);
			paletColumns.setOwner(true);
			
			
			return paletDAO.getPaletsByCriteria(tagname.toUpperCase(), paletColumns);
			

		
		
	}
	@ResponseBody
	@RequestMapping(value="/Connect-OK-ULD")
	public boolean connectOK(String palet,String connectedpalet){
		
		
		
		try {
			messageDAO.solveErrorByConnectingPalet(ParsingTools.parseSinglePalet(palet),ParsingTools.parseSinglePalet(connectedpalet));
		} catch (MyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
		
		
	}
	
	@ResponseBody
	@RequestMapping(value="/ignore-OK")
	public boolean ignoreOK(int errorID){
		
		try {
			return messageDAO.ignoreError(errorID);

		} catch (Exception e) {
			return false;
		}
		
		
		
	}
	
	
}
