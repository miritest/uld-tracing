package net.myapp.spring.controller;


import java.text.ParseException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

import net.myapp.common.web.holders.RequestHelper;
import net.myapp.dao.methods.MessageDAO_ADT;
import net.myapp.dao.methods.PaletActionDAO_ADT;
import net.myapp.dao.methods.PaletDAO_ADT;
import net.myapp.dao.models.ActionError;
import net.myapp.dao.models.Message;
import net.myapp.dao.models.Palet;
import net.myapp.exceptions.MyException;
import net.myapp.utils.*;


@Controller
public class FirstController {


@Autowired(required=true)
@Qualifier("PaletDAO")
private PaletDAO_ADT paletDAO ;

	

@Autowired(required=true)
@Qualifier("PaletActionDAO")
private PaletActionDAO_ADT paletActionDAO;


@Autowired(required=true)
@Qualifier("MessageDAO")
private MessageDAO_ADT messageDAO;

//@Autowired
//ServletContext context;

	
	
	@GetMapping("/test")
	public String test(){
		String ucm="UCM\r\n"+
"J29226/24JAN.ISWIB.TBS\r\n"+
"OUT\r\n"+
".PGA55555PO/BRL\r\n"+
".PMC00172ZP.PMC00455ZP/NYR";
		Palet palet=null;
		try {
			 palet=ParsingTools.getPaletFromMessage(ucm, "PMC00172ZP");
		} catch (MyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("type:"+palet.getType());
		System.out.println("code:"+palet.getCode());
		System.out.println("owner:"+palet.getOwner());
		System.out.println("position:"+palet.getPosition());
		/*
String ucm2="UCM\r\n"+
"IU332/31DEC.ISWIB.GYD\r\n"+
"OUT\r\n"+
".PGA07085FF/BRL\r\n"+
".PMC00172ZP/HKG.PMC00455ZP/NYR.PMC00729ZP/HKG.PMC00789ZP/HKG";

String ucm3="UCM\r\n"+
"IU332/24JAN.ISWIB.BRL\r\n"+
"IN\r\n"+
".PGA55555PO.PMC99999ZP.PMC00455ZP.PMC00729ZP\r\n"+
".PMC00789ZP.PMC00893ZP";

		
		String scm="SCM\r\n"+
				"SAW.29NOV/0632\r\n"+
				".PMC.00172ZP/00455ZP/00729ZP/00893ZP/99999ZP.T5";
		
		Set<Palet> paletsSet=null;
		try {
			 paletsSet=Parse.parse(ucm3);
		} catch (MyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println("incoming message:\n"+ucm3+"\n");
		//System.out.println("palet say:"+paletsSet.size());
		for (Palet palet : paletsSet) {
			System.out.println("kod:"+palet.getCode());
		}*/
		/*
		Parse parse=new Parse();
		Set<Palet> paletsSet=null;
		try {
			 paletsSet=parse.parseTextMessage(ucm3);
		} catch (MyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("palet say:"+paletsSet.size());
		for (Palet palet : paletsSet) {
			System.out.println("kod:"+palet.getCode());
		}*/
		//PaletStatisticMethods.getPaletStatistics(paletsSet, false, false);
		
		paletDAO.DeleteALL();


		return "new_uld";
	}

	@GetMapping("/home")
	public String homeGet() {
		Set<Message> daoMessagesSet=null;
	
		Mail mail=new Mail();
		

			try {
				daoMessagesSet=
						mail.read();
			} catch (Exception e1) {

				e1.printStackTrace();
			}
		

		
			for (Message message : daoMessagesSet) {
			
			try {
				
				System.err.println("incoming message:\n"+message.getText()+"\n");

				Set<Message> messageSet=Parse.parse(message);
				
				if (messageSet!=null) {
					for (Message message2 : messageSet) {
						
						ParsingTools.checkPaletActionsSize(message2);

					if (message2.getType().equals("IN")) {
						messageDAO.insertParsedUCM_IN(message2);
					}else if(message2.getType().equals("OUT")){
						messageDAO.insertParsedUCM_OUT(message2);
					}
					else{
						//System.out.println("Message is "+message2);
							messageDAO.insertParsedSCM(message2);
					}
				}
				}

			} catch (MyException | ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				message.setStatus("error");
				if (e.getClass()==MyException.class) {
					message.setReason(((MyException) e).getReason());
					message.setReason_user(((MyException) e).getReason_user());
				}else{
					message.setReason(e.getMessage());
					message.setReason_user("Parsing Exception"); 
				}

				messageDAO.insert(message);
			}
		}


		
		return "home";
	}
	
	
	
	@GetMapping("/ULDtable")
	public String ULD_tableGet(){
		
		return "ULD-table";
	}
	
	@GetMapping("/ULD_details")
	public String ULD_detailGet(@RequestParam(value = "ID", required = false, defaultValue = "0") int ID,String paletString){
		Palet palet=new Palet();
		
		if(!Utils.isNull(paletString))
		{
			try {
				palet=ParsingTools.parseSinglePalet(paletString);
			} catch (MyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		palet.setID(ID);
			
			List<Palet>test=paletDAO.getPaletDetailedInfo(palet);
			 
			RequestHelper.setAttribute("palet", test.get(0));	

		
		
		return "ULD_detail";
	}
	
	@GetMapping("/extra")
	public String extra(){
	return "extra_functions";
	}

	
}
