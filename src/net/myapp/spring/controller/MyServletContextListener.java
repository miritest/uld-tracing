package net.myapp.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import net.myapp.dao.form.methods.FormDAO_ADT;
import net.myapp.utils.StaticVariables;


@Component
public class MyServletContextListener implements ApplicationListener<ContextRefreshedEvent> {
	
	
	@Autowired(required=true)
	@Qualifier("FormDAO")
	private FormDAO_ADT formDAO ;
	
	  @Override
	  public void onApplicationEvent(final ContextRefreshedEvent event) {
	    // do whatever you need here 
			StaticVariables.fillFormList(formDAO);

	  }
}
