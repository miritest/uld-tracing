package net.myapp.spring.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.myapp.common.web.holders.RequestHelper;
import net.myapp.dao.form.methods.AirportDAO_ADT;
import net.myapp.dao.form.methods.FormDAO_ADT;
import net.myapp.dao.form.models.Airport;
import net.myapp.dao.form.models.Statistic;
import net.myapp.dao.methods.MessageDAO_ADT;
import net.myapp.dao.models.Message;
import net.myapp.dao.models.Palet;
import net.myapp.exceptions.MyException;
import net.myapp.statistics.methods.PaletStatisticMethods;
import net.myapp.statistics.model.PaletStatistics;
import net.myapp.utils.Parse;
import net.myapp.utils.ParsingTools;
import net.myapp.utils.StaticVariables;
import net.myapp.utils.messagekinds.UCM;


@Controller
@RequestMapping("message")
public class MessageController {

	@Autowired(required=true)
	@Qualifier("FormDAO")
	private FormDAO_ADT formDAO ;


	@Autowired(required=true)
	@Qualifier("AirportDAO")
	private AirportDAO_ADT airportDAO;
	
	@Autowired(required=true)
	@Qualifier("MessageDAO")
	private MessageDAO_ADT messageDAO;
	
	
	
	
	//private static final Logger logger = Logger.getLogger(MessageController.class);
	@GetMapping("/new_custom")
	public String custom(){
		long a=System.currentTimeMillis();
		System.out.println("Sorted basladi");
		formDAO.getSortedTypes();
		System.out.println("Sorted bitdi:"+(System.currentTimeMillis()-a));
		
		
		long b=System.currentTimeMillis();
		System.out.println("hazir basladi");
		StaticVariables.getPalet_types();
		System.out.println("hazir bitdi:"+(System.currentTimeMillis()-b));
		
		
		return "message/new_custom";
	}
	
	@GetMapping("/new_LUC")
	public String Luc(){
		RequestHelper.setAttribute("owners", StaticVariables.getPalet_owners());
		RequestHelper.setAttribute("airports",airportDAO.getAirports());
		RequestHelper.setAttribute("types",StaticVariables.getPalet_types());
		return "message/new_LUC";
	}
	
	@GetMapping("/new_SCM")
	public String SCM(){
List<Airport> airportList=airportDAO.getAirports();
		
		ObjectMapper objectMapper = new ObjectMapper();
		List<String> iatas=new ArrayList<String>();
		for (Airport airport : airportList) {
			iatas.add(airport.getIata());
			
		}
		String json="";
		try {
			json = objectMapper.writeValueAsString(iatas);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		RequestHelper.setAttribute("iatas",json);
		RequestHelper.setAttribute("airportList",airportList);
		RequestHelper.setAttribute("owners",StaticVariables.getPalet_owners());
		RequestHelper.setAttribute("types", StaticVariables.getPalet_types());

		
		return "message/new_SCM";
	}
	
	@GetMapping("/new_UCM")
	public String UCM_get(){
		
		
		List<Airport> airportList=airportDAO.getAirports();
		
		ObjectMapper objectMapper = new ObjectMapper();
		List<String> iatas=new ArrayList<String>();
		for (Airport airport : airportList) {
			iatas.add(airport.getIata());
			
		}
		String json="";
		try {
			json = objectMapper.writeValueAsString(iatas);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		RequestHelper.setAttribute("iatas",json);
		RequestHelper.setAttribute("airportList", airportList);
		RequestHelper.setAttribute("owners",StaticVariables.getPalet_owners());
		RequestHelper.setAttribute("types", StaticVariables.getPalet_types());
		return "message/new_UCM";
	}
	
	@PostMapping("/new_UCM")
	public String UCM_post(String message){
		try{
		Message messageObject=new Message();
		messageObject=ParsingTools.fillManualMessage(messageObject);
		messageObject.setType("UCM");
		try {
			messageObject.setText(ParsingTools.smoothMessage(message));
		} catch (MyException e1) {
			System.err.println("ERROR---------"+e1.getReason());
			messageObject.setReason(e1.getReason());
			messageObject.setReason_user(e1.getReason_user());
		}
		
		try {
			
			System.err.println("incoming message:\n"+messageObject.getText()+"\n");

			Set<Message> messageSet=Parse.parse(messageObject);
			
			if (messageSet!=null) {
				for (Message parsedMessage : messageSet) {
					
					ParsingTools.checkPaletActionsSize(parsedMessage);
					

				if (parsedMessage.getType().equals("IN")) {
					messageDAO.insertParsedUCM_IN(parsedMessage);
				}else if(parsedMessage.getType().equals("OUT")){
					messageDAO.insertParsedUCM_OUT(parsedMessage);
				}
				else{
					//System.out.println("Message is "+message2);
						messageDAO.insertParsedSCM(parsedMessage);
				}
			}
			}

		} catch (MyException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			messageObject.setStatus("error");
			if (e.getClass()==MyException.class) {
				messageObject.setReason(((MyException) e).getReason());
				messageObject.setReason_user(((MyException) e).getReason_user());
			}else{
				messageObject.setReason(e.getMessage());
				messageObject.setReason_user("Parsing Exception"); 
			}
			messageDAO.insert(messageObject);
		}
		System.out.println("girdi");
		} catch (Exception e) {
			if (e.getClass()==MyException.class) {
		           System.err.println(((MyException)e).getReason());
			}else{
				e.printStackTrace();
			}
	           }
		return null;
	}
	
	@PostMapping("/new_SCM")
	public String SCM_post(String message){
		try{
		Message messageObject=new Message();
		messageObject=ParsingTools.fillManualMessage(messageObject);
		messageObject.setType("SCM");
		try {
			messageObject.setText(ParsingTools.smoothMessage(message));
		} catch (MyException e1) {
			System.err.println("ERROR---------"+e1.getReason());
			messageObject.setReason(e1.getReason());
			messageObject.setReason_user(e1.getReason_user());
			
		}
		try {
			
			System.err.println("incoming message:\n"+messageObject.getText()+"\n");

			Set<Message> messageSet=Parse.parse(messageObject);
			
			if (messageSet!=null) {
				for (Message parsedMessage : messageSet) {
					
					ParsingTools.checkPaletActionsSize(parsedMessage);

				if (parsedMessage.getType().equals("IN")) {
					messageDAO.insertParsedUCM_IN(parsedMessage);
				}else if(parsedMessage.getType().equals("OUT")){
					messageDAO.insertParsedUCM_OUT(parsedMessage);
				}
				else{
						messageDAO.insertParsedSCM(parsedMessage);
				}
			}
			}
		} catch (MyException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			messageObject.setStatus("error");
			if (e.getClass()==MyException.class) {
				messageObject.setReason(((MyException) e).getReason());
				messageObject.setReason_user(((MyException) e).getReason_user());
			}else{
				messageObject.setReason(e.getMessage());
				messageObject.setReason_user("Parsing Exception"); 
			}
			messageDAO.insert(messageObject);
		}
		System.out.println("girdi");
		} catch (Exception e) {
			if (e.getClass()==MyException.class) {
		           System.err.println(((MyException)e).getReason());
			}else{
				e.printStackTrace();
			}
	           }
		return null;
	}
	
	@GetMapping("/message_detail")
	public String message_detailGet(int id){
		Message message=messageDAO.getMessagesByID(id);
		RequestHelper.setAttribute("message",message);
		System.out.println("MESSAGE IS "+message);
		Set<Palet> palets=null;
		try {
			palets = Parse.parse(message.getText());
			
		} catch (MyException e1) {
			System.err.println("ERROR---------"+e1.getReason());
		}
		
		PaletStatistics paletStatistics=PaletStatisticMethods.getPaletStatistics(palets, true, true);
	    	
		if (paletStatistics!=null) {	
		
		ObjectMapper objectMapper = new ObjectMapper();
    	String typeStat="";
    	String ownerStat="";


			
    	
		try {
			typeStat = objectMapper.writeValueAsString(paletStatistics.getTypeStatistics());
			ownerStat = objectMapper.writeValueAsString(paletStatistics.getOwnerStatistics());
			RequestHelper.setAttribute("typeStat", typeStat);
			RequestHelper.setAttribute("ownerStat", ownerStat);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  
    	}
		


		return "message/message-details";
	}
	
	@GetMapping("/messages")
	public String messagesAll_Get(){
		
		return "message/all_messages";
	}
	
}
