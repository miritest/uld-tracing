package net.myapp.utils;

public class StaticPatterns {
	private static final String ucmHeadingFormat="UCM\r\n[0-9A-Z]{5,7}(\\/)[0-9A-Z]{2,5}(\\.)";
	private static final String scmHeadingFormat="SCM\r\n[0-9A-Z]{3}(\\.)[0-9A-Z]{5,7}(\\/)";
	private static final String lucHeadingFormat="LUC\r\n[0-9A-Z]{9,14}(\\/)[0-9A-Z]{5,7}(\\/)";
	
	private static final String ucmOutLastPaletteFormat = "((?s).*[\r\n].*)(\\.)([0-9A-Z]{3})(\\d{4,5})([0-9A-Z]{2,6})(((\\/)([A-Z]{3}))||())";
	private static final String ucmInLastPaletteFormat = "((?s).*[\r\n].*)(\\.)([0-9A-Z]{3})(\\d{4,5})([0-9A-Z]{2,6})";
	private static final String scmLastPaletteFormat = "((?s).*[\r\n].*)((\\/)|(\\.))([0-9A-Z]{6,11})(\\.)(T)(\\d+)";
	private static final String lucLastPaletteFormat = "((?s).*[\r\n].*)([0-9A-Z]{9,14})(\\/)(.*)(\\/)([A-Z]{3})";
	
	private static final String scmPaletteCountFormat="(.*)((\\/)||(\\.))([0-9A-Z]{6,11})(\\.)(T)";
	private static final String onlyDigitPattern = "(\\d+)";
	private static final String onlyWordPattern = "[a-zA-Z]+";
	private static final String inNillFormat="IN((\r\n)*).N((\r\n)*)";
	private static final String outNillFormat="OUT((\r\n)*).N((\r\n)*)";
	private static final String nillFormat=".N";
	private static final String inFormat="IN\r\n";
	private static final String outFormat="OUT\r\n";
	


	public static String getInformat() {
		return inFormat;
	}

	public static String getOutformat() {
		return outFormat;
	}

	public static String getNillformat() {
		return nillFormat;
	}

	public static String getOnlydigitpattern() {
		return onlyDigitPattern;
	}

	public static String getOnlywordpattern() {
		return onlyWordPattern;
	}

	public static String getUcmoutlastpaletteformat() {
		return ucmOutLastPaletteFormat;
	}

	public static String getUcminlastpaletteformat() {
		return ucmInLastPaletteFormat;
	}

	public static String getScmlastpaletteformat() {
		return scmLastPaletteFormat;
	}

	public static String getUcmheadingformat() {
		return ucmHeadingFormat;
	}

	public static String getScmheadigformat() {
		return scmHeadingFormat;
	}

	public static String getScmheadingformat() {
		return scmHeadingFormat;
	}

	public static String getInnillformat() {
		return inNillFormat;
	}

	public static String getOutnillformat() {
		return outNillFormat;
	}

	
	public static String getScmpalettecountformat() {
		return scmPaletteCountFormat;
	}

	public static String getLucheadingformat() {
		return lucHeadingFormat;
	}

	public static String getLuclastpaletteformat() {
		return lucLastPaletteFormat;
	}
	
	
	
}
