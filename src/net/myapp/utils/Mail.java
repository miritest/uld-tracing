package net.myapp.utils;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import net.myapp.common.logging.impl.Log;
import net.myapp.exceptions.MyException;




public class Mail   {
	private Set<String> messages=new HashSet<>();
	//private Set<net.myapp.dao.models.Message> daoMessageSet;
	
	public Set<net.myapp.dao.models.Message> read() throws Exception {
	 
	        Properties properties = new Properties();
	       Message[] messages=null;
	         try {
	       /* 	//	InputStream config = Mail.class.getClassLoader().getResourceAsStream("properties/mail.properties");
	        		
	        		//properties.load(config);
	        		properties.put("mail.pop3.port","110");
	        		properties.put("mail.pop3.host","192.168.22.5");
	        		//properties.put("mail.pop3.starttls.enable","true");
	        		System.out.println(properties.getProperty("mail.pop3.host"));
	        		System.out.println(properties.getProperty("mail.pop3.port"));

	        		
	        		
	            Session session = Session.getDefaultInstance(properties, null);
	 
	            Store store = session.getStore("pop3");
	            
	            
	            store.connect("192.168.22.5", "irtest", "Kilembr87");
//            		exmb03.bct.az


	              //  Folder inbox= store.getFolder("[Gmail]/Spam");


	                  Folder inbox = store.getFolder("inbox");
	            inbox.open(Folder.READ_WRITE);
	 
	 
	            messages = inbox.getMessages();
	            System.out.println("------------------------------");
	            Message[] copy=new Message [messages.length];
	            
	            
	            for (int i = 0; i < messages.length; i++) {
	            		copy[i]=new MimeMessage((MimeMessage)messages[i]);
					//messages[i].setFlag(Flag.DELETED, true);
				}
	            

	   		 	System.out.println("Mesaj sayi budur="+messages.length);
	   		 	
	   		 	//Set<Message>messageList=new HashSet<>(Arrays.asList(messages));

	   		 
	            for (int i = 0; i <messages.length; i++) {
	                System.out.println("Mail Subject:- " + messages[i].getSubject());
	                System.out.println("Mail sender:-"+messages[i].getFrom()[0]);
	                System.out.println("Mail Date:-"+messages[i].getReceivedDate());
	                System.out.println("Mail Content:-"+messages[i].getContent().toString());
	                System.out.println("Mail Content Type:-"+messages[i].getContentType());
	            
	                if(messages[i].isMimeType("text/plain")){
	        			System.out.println("Message content Type:::: text/plain"); 
	        		}else if(messages[i].isMimeType("text/html")){
	        			System.out.println("Message content Type:::: html");
	        		}else if(messages[i].isMimeType("multipart/alternative")){
	        			System.out.println("Message content Type:::: alternative");
	        		}else if(messages[i].isMimeType("multipart/*")){
	        			System.out.println("Message content Type:::: multipart");
	        		}
	                
	                
    }  */    
	        	InputStream config = Mail.class.getClassLoader().getResourceAsStream("properties/mail.properties");
        		
        		properties.load(config);
		 
            Session session = Session.getDefaultInstance(properties, null);
 
            Store store = session.getStore(properties.getProperty("connection_type"));
            
            
            store.connect(properties.getProperty("host"), properties.getProperty("username"), properties.getProperty("pass"));



              Folder inbox= store.getFolder("[Gmail]/Starred");


           //        Folder inbox = store.getFolder("inbox");
            inbox.open(Folder.READ_ONLY);
 
 
             messages = inbox.getMessages();
            System.out.println("------------------------------"+messages.length);
            Message[] copy=new Message [messages.length];
            
            
            for (int i = 0; i < messages.length; i++) {
            		copy[i]=new MimeMessage((MimeMessage)messages[i]);
				//messages[i].setFlag(Flag.DELETED, true);
			}
	            inbox.close(true);
	            store.close();
	           
	            for (int i = 0; i <copy.length; i++) {
	            	  Enumeration headers = copy[i].getAllHeaders();
	            	  while (headers.hasMoreElements()) {
	            		  Header h = (Header) headers.nextElement();
	            		  System.out.println(h.getName() + ": " + h.getValue());
	            		  }
	            		  System.out.println("------------------");
	            		  
	            }
	            return convertMailToMesageSET(copy);

	 
	        } catch (Exception e) {
	         	e.printStackTrace();
	        	throw new Exception();
	            //return convertMailToMesageSET(messages);
/*	            	e.printStackTrace();
				if (e.getClass()==MyException.class) {
					Log.error(((MyException)e).getReason());
					throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
				}else{
					Log.error(e.getMessage());
					throw new MyException(e.getMessage(), "Parsing error");
				}
*/	        }
}

	public Set<net.myapp.dao.models.Message> convertMailToMesageSET(Message[] messages) throws Exception{
		
	    	Set<net.myapp.dao.models.Message> daoModelMessages=new HashSet<>();
	    	for (int i = 0; i < messages.length; i++) {
				net.myapp.dao.models.Message daoMessage=new net.myapp.dao.models.Message();
				//try {
					daoMessage=getMessageText(daoMessage,messages[i]);
					daoMessage=getMessageType(daoMessage, messages[i]);
					daoMessage=getFromEmail(daoMessage,messages[i]);
					daoMessage=getSubject(daoMessage,messages[i]);
					daoMessage=getReceiveDate(daoMessage,messages[i]);
				
					daoModelMessages.add(daoMessage);
				/*} catch (Exception e) {
					
					Log.error(MyException.getErrorLocation("Mail", "convertMailToMesageSET", getTextFromMessage(messages[i])));
					throw new MyException(MyException.getErrorLocation("Mail", "convertMailToMesageSET", getTextFromMessage(messages[i])),getTextFromMessage(messages[i]));
				}*/
			}
	    	return daoModelMessages;
	    }
	    	 
	private net.myapp.dao.models.Message getReceiveDate(net.myapp.dao.models.Message daoMessage, Message message) {
			// TODO Auto-generated method stub
			try {
				if (message.getReceivedDate()==null) {
	            	Enumeration headers = message.getAllHeaders();
            		for (int i = 0; i <8; i++) {
                		headers.nextElement();
					}
            		Header h=(Header) headers.nextElement();
            		System.out.println("DATE IS "+h.getValue());
    				daoMessage.setReceiveDate(new Date(h.getValue()));
				}else
				daoMessage.setReceiveDate(message.getReceivedDate());
				if(message.getReceivedDate()==null){
					System.out.println(message.getSubject()+"___null");
				}else{
					System.out.println(message.getReceivedDate()+"___");
				}
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return daoMessage;
		}

	private net.myapp.dao.models.Message getSubject(net.myapp.dao.models.Message daoMessage, Message message) {
			// TODO Auto-generated method stub
			try {
				daoMessage.setSubject(message.getSubject());
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return daoMessage;
		}

	private net.myapp.dao.models.Message getFromEmail(net.myapp.dao.models.Message daoMessage, Message message) {
			// TODO Auto-generated method stub
			try {
				Address[] addresses=message.getFrom();
				InternetAddress internetAddress=new InternetAddress(addresses[0].toString());
				daoMessage.setFromEmail(internetAddress.getAddress());
				daoMessage.setSender(internetAddress.getPersonal());
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			return daoMessage;
		}

	private net.myapp.dao.models.Message getMessageText(net.myapp.dao.models.Message daoMessage, Message message) throws Exception /*throws Exception*/ {
			// TODO Auto-generated method stub
		/*	String messageText=ParsingTools.smoothMessage(getTextFromMessage(message));
			ParsingTools.removeAllNills(ParsingTools.smoothMessage(getTextFromMessage(message)));
*/
		String mailContent=getTextFromMessage(message);
		try {
			daoMessage.setText(ParsingTools.smoothMessage(mailContent));
		} catch (Exception e) {
			daoMessage.setText(mailContent);
/*			Log.error(MyException.getErrorLocation("Mail", "getMessageText", "Incoming message::"+mailContent));
			throw new MyException(MyException.getErrorLocation("Mail", "getMessageText", "Incoming message::"+mailContent),"Incoming message::"+mailContent);
	*/
		}
			
			return daoMessage;
		}

	private net.myapp.dao.models.Message getMessageType(net.myapp.dao.models.Message daoMessage, Message message) throws Exception{
		String text=daoMessage.getText();
		text=ParsingTools.getFirstLineOf(text).trim();
		
			daoMessage.setType(text.substring(0, 3));
		return daoMessage;
		
	}

	public Set<String> getMainMessages(Set<String> emailMessagesList) throws MyException{

			Set<String> mainMessages=new HashSet<>();
			for (String message : emailMessagesList) {
				mainMessages.add(ParsingTools.smoothMessage(message));
			}
			return mainMessages;
		}
		
	public Set<String> getMessageList(Set<javax.mail.Message> mailMessagesList) throws Exception{

			//Set<javax.mail.Message> mailMessagesList=mail.read();

			for (javax.mail.Message message : mailMessagesList) {
				String result=getTextFromMessage(message);
				
				//result+=getDAOMessageFieldsString(message);
				
				messages.add(result);
				
			}
			return messages;
		}
			
	private String getTextFromEmailMessage(BodyPart bodyPart2) throws Exception{

			String result = "";
	        MimeMultipart mimeMultipart = (MimeMultipart)bodyPart2.getContent();
	        
	        int count = mimeMultipart.getCount();
	        for (int i = 0; i < 1; i ++){
	            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
	          //  System.out.println("body part type is  "+bodyPart.getContentType());
	         
	            if(bodyPart.isMimeType("multipart/*")){
	            	return getTextFromEmailMessage(bodyPart);
	            }else if (bodyPart.isMimeType("text/plain")){
	            	
	                result = result + "\n" + bodyPart.getContent();
	               
	            } 
	        }
	 
	        return result;
			
			
			
		}
		
	private String getTextFromMessage(javax.mail.Message message) throws Exception {

			
			//message.getFolder().open(Folder.READ_ONLY);
			
		    if (message.isMimeType("text/plain")){
		    	//message.getFolder().close(true);
		    	//System.out.println("mime type is "+message.getContentType());
		        return message.getContent().toString();
		    }else if (message.isMimeType("multipart/*")) {
		    	//System.out.println("mime type is "+message.getContentType());
		        String result = "";
		        MimeMultipart mimeMultipart = (MimeMultipart)message.getContent();
		        
		       // int count = mimeMultipart.getCount();
		        for (int i = 0; i < 1; i ++){
		            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
		            //System.out.println("body part type is  "+bodyPart.getContentType());
		        
		            if(bodyPart.isMimeType("multipart/*")){
		            	//System.out.println("getdi metoda="+bodyPart.getContentType());

		            	return getTextFromEmailMessage(bodyPart);
		            }else if (bodyPart.isMimeType("text/plain")){
		            	
		                result = result + "\n" + bodyPart.getContent();
		            }
		        }
		  
		        return result;
		    }

		    return "oxunmadi";
		}
		
	}
