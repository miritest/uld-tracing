package net.myapp.utils.messagekinds;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.myapp.common.logging.impl.Log;
import net.myapp.dao.models.Message;
import net.myapp.dao.models.Palet;
import net.myapp.dao.models.PaletAction;
import net.myapp.exceptions.MyException;
import net.myapp.utils.ParsingTools;

public class SCM {
	//main parsing
		public static Set<Message> parse(Message messageObject, PaletAction paletAction) throws MyException, ParseException{
			//message=new Message();
			//paletAction=new PaletAction();
			Set<PaletAction> paletActionsSet=new HashSet<>();
			Set<Message> messagesSet=new HashSet<>();

			parseFlightInformation(getFlightInfo(messageObject),paletAction,messageObject);

			messagesSet.add(parsePaletteMessage(messageObject,paletAction,paletActionsSet));

			return messagesSet;
		}

	
		private static Message parsePaletteMessage(Message messageObject, PaletAction paletAction, Set<PaletAction> paletActionsSet) throws MyException{
			String paletteMessage=getPaletteMessage(messageObject);
			String[] palettesArray=null;
			int[] counts=null;
			try {
				palettesArray=ParsingTools.splitByDotT(paletteMessage);
				counts=ParsingTools.getAmountsOfPalettes(paletteMessage);
			for (int i = 0; i < palettesArray.length; i++) {
				palettesArray[i]=palettesArray[i].trim();
			//	System.out.println("elements  "+i+"="+palettesArray[i]);
				HashSet<Palet> palets=parsePalet(palettesArray[i]);
				if (palets.size()==counts[i]) {
					for (Palet palet : palets) {

					palet.setPosition(paletAction.getArriving());
					paletAction.addPalet(palet);
									
				}	
				}else{
					Log.error(MyException.getErrorLocation("SCM", "parsePaletteMessage",  "T"+counts[i]+" yazilan sirada palet sayi yanlisdir"));
					throw new MyException(MyException.getErrorLocation("SCM", "parsePaletteMessage", "T"+counts[i]+" yazilan sirada palet sayi yanlisdir"), "T"+counts[i]+" yazilan sirada palet sayi yanlisdir");
				}
			
			}
			paletActionsSet.add(paletAction);

			} catch (MyException e) {
				// TODO: handle exception
				Log.error(e.getReason());
				throw new MyException(e.getReason(),e.getReason_user());

		}
		paletActionsSet.add(paletAction);	
		messageObject.setPaletActions(paletActionsSet);
		messageObject=ParsingTools.checkMessage(messageObject);
		return messageObject;
		}

		private static HashSet<Palet> parsePalet(String palette) throws MyException{
			
			String paletteCode=null;
			String paletteOwner=null;

			HashSet<Palet> palets=new HashSet<>();

			//PALETTE TYPE
			String paletType=getPaletteType(palette);
/*			if (!ParsingTools.containingOnlyLetters(paletType)) {
				Log.error(MyException.getErrorLocation("SCM", "parsePalet", "Wrong Palette Type "+paletType));
				throw new MyException(MyException.getErrorLocation("SCM", "parsePalet", "Wrong Palette Type "+paletType),"Wrong Palette Type "+paletType);
			}*/
			String[] paletteCodesArray=ParsingTools.splitBySlash(ParsingTools.splitByDot(palette)[2]);

			for (int i = 0; i < paletteCodesArray.length; i++) {
				paletteCodesArray[i]=paletteCodesArray[i].trim();
			//	System.out.println("array:"+paletteCodesArray[i]);
					Palet palet=new Palet();
					
					palet.setType(paletType);
					
					try {
					
					System.out.println("HEY!\n"+paletteCodesArray[i]);
					int index=ParsingTools.allocateCodePlusOwner(paletteCodesArray[i]);
					System.out.println(paletteCodesArray[i].substring(0, index)+"---"+paletteCodesArray[i].substring(index));
					
					//PALETTE CODE
					palet.setCode(paletteCodesArray[i].substring(0, index));
					//PALETTE OWNER
					palet.setOwner(paletteCodesArray[i].substring(index));
					} catch (Exception e) {
						if (e.getClass()==MyException.class) {
							Log.error(((MyException)e).getReason());
							throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
						}else{
							Log.error(e.getMessage());
							throw new MyException(e.getMessage(), "Parsing error");
					}				
				}
					/*
					try {
						//PALETTE CODE
						paletteCode=paletteCodesArray[i].substring(0, 5);
						
						if (!ParsingTools.containingOnlyDigits(paletteCode)) {
							Log.error(MyException.getErrorLocation("SCM", "parsePalet", "Wrong Palette Code "+paletteCode));
							throw new MyException(MyException.getErrorLocation("SCM", "parsePalet", "Wrong Palette Code "+paletteCode),"Wrong Palette Code "+paletteCode);
						}else{
							palet.setCode(paletteCode);

						}
					
					} catch (Exception e) {
						if (e.getClass()==MyException.class) {
							Log.error(((MyException)e).getReason());
							throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
						}else{
							Log.error(e.getMessage());
							throw new MyException(e.getMessage(), "Parsing error");
						}
					}
					
					try {
						//PALETTE OWNER
					//	System.err.println("owner:"+paletteOwner);
						paletteOwner=paletteCodesArray[i].substring(5);
						//if (ParsingTools.containingOnlyLetters(paletteOwner)) {
							palet.setOwner(paletteOwner);
						}else{
							Log.error(MyException.getErrorLocation("SCM", "parsePalet", "Wrong Palette Owner:"+paletteOwner));
							throw new MyException(MyException.getErrorLocation("SCM", "parsePalet", "Wrong Palette Owner:"+paletteOwner),"Wrong Palette Owner:"+paletteOwner);
						}

					} catch (Exception e) {
						if (e.getClass()==MyException.class) {
							Log.error(((MyException)e).getReason());
							throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
						}else{
							Log.error(e.getMessage());
							throw new MyException(e.getMessage(), "Parsing error");
						}
					}*/
					
					palets.add(palet);
				}
				
			return palets;
		
	}

		private static String getPaletteType(String palette){
			return ParsingTools.splitByDot(palette)[1];
		}
		
		private static String getPaletteMessage(Message messageObject) throws MyException{
			
			try {	
				return messageObject.getText().substring(messageObject.getText().lastIndexOf(getFlightInfo(messageObject))+getFlightInfo(messageObject).length());
			} catch (MyException e) {
				Log.error(e.getReason());
				throw new MyException(e.getReason(), e.getReason_user());
			}
		}

		private static void parseFlightInformation(String flightInfo, PaletAction paletAction, Message messageObject) throws MyException, ParseException{
			try{
			//System.out.println("flight info scm "+flightInfo);
			String[] flightInfoPartsBySlash=ParsingTools.splitBySlash(flightInfo);
			//System.out.println("2222 "+flightInfoPartsBySlash[0]);
			String arriving=ParsingTools.splitByDot(flightInfoPartsBySlash[0].trim())[0];
			//System.out.println("111 1 "+arriving);
			paletAction.setArriving(arriving);
			String date=ParsingTools.splitByDot(flightInfoPartsBySlash[0].trim())[1];
			String time=flightInfoPartsBySlash[1].trim();
			//System.out.println("time "+time);

/*			if (!time.contains("\\.")) {
				time=time.substring(0, 2)+"."+time.substring(2);
			}*/
			date=date+Calendar.getInstance().get(Calendar.YEAR)+time;
			System.out.println("time "+time);
			
			//date=ParsingTools.parseDateString(date);
			//System.out.println("mmmn+"+date);
			//try {
		//	System.out.println("scm dateler:"+date);
			System.out.println("first date "+date);

			messageObject.setReceiveDate(ParsingTools.toDate(date));
		
			/*	} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			//System.out.println(this.message.getDate());
			}
			catch (MyException e) {
				// TODO: handle exception
				Log.error(e.getReason());
				 throw new MyException(e.getReason(),e.getReason_user());

			}
		}

		private static String getFlightInfo(Message messageObject) throws MyException{
			 try{
				 
				 return ParsingTools.splitByLine(messageObject.getText())[1].trim();

			 } catch (Exception e) {
				 // TODO: handle exception	
				 Log.error(MyException.getErrorLocation("SCM", "getFlightInfo", "Flight Information", e));
				 throw new MyException(MyException.getErrorLocation("SCM", "getFlightInfo", "Flight Information", e),"Flight Information");

			 }
		}

		
		public static Set<Palet> parse(String messageText, Set<Palet> paletsSet) throws MyException {
			messageText=ParsingTools.removeFirstLineOf(messageText);
			messageText=ParsingTools.removeFirstLineOf(messageText);
			return parsePaletteMessage(messageText, paletsSet);
		}
		
		private static Set<Palet> parsePaletteMessage(String messageText, Set<Palet> paletsSet) throws MyException{
			String[] palettesArray=null;
			int[] counts=null;

			try {
				palettesArray=ParsingTools.splitByDotT(messageText);
				counts=ParsingTools.getAmountsOfPalettes(messageText);

			for (int i = 0; i < palettesArray.length; i++) {
				//System.out.println("elements  "+i+"="+palettesArray[i]);
				HashSet<Palet> palets=parsePalet(palettesArray[i]);
				if (palets.size()==counts[i]) {
				for (Palet palet : palets) {
					paletsSet.add(palet);
					//null a gore dusmur bazaya
					//this.paletAction.setPalet(palet);
					//this.actionSet.add(new PaletAction(this.paletAction));
				//	palet.setPosition(paletAction.getArriving());
					//paletAction.addPalet(palet);
					
				//	System.out.println("palet--"+palet.getType()+"."+palet.getCode()+palet.getOwner());
					
				}}else{
					
					Log.error(MyException.getErrorLocation("SCM", "parsePaletteMessage", "T"+counts[i]+" yazilan sirada palet sayi yanlisdir"));
					throw new MyException(MyException.getErrorLocation("SCM", "parsePaletteMessage", "T"+counts[i]+" yazilan sirada palet sayi yanlisdir"),"T"+counts[i]+" yazilan sirada palet sayi yanlisdir");
				}

			}
			//paletActionsSet.add(paletAction);

			} catch (MyException e) {
				// TODO: handle exception
				Log.error(e.getReason());
				throw new MyException(e.getReason(),e.getReason_user());

		}
//		paletActionsSet.add(paletAction);	
		//messageObject.setPaletActions(paletActionsSet);
		//messageObject=ParsingTools.checkMessage(messageObject);
		return paletsSet;
		}

	
}
