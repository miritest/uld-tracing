package net.myapp.utils.messagekinds;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import net.myapp.common.logging.impl.Log;
import net.myapp.dao.models.Message;
import net.myapp.dao.models.Palet;
import net.myapp.dao.models.PaletAction;
import net.myapp.exceptions.MyException;
import net.myapp.utils.ParsingTools;

public class LUC {
	/*
	 * 
	 * 
	
	
	LUC 
	PAG030038D/07FEB18/1430/FX/ZP/DWC/463-00000000/DWC/SER 
	PAG030718D/07FEB18/1430/FX/ZP/DWC/463-10000000/DWC/SER 
	PAG030368D/07FEB18/1430/FX/ZP/DWC/463-20000000/DWC/SER 
	PAG030528D/07FEB18/1430/FX/ZP/DWC/463-30000000/DWC/SER 
	PAG030608D/07FEB18/1430/FX/ZP/DWC/463-40000000/DWC/SER 
	PAG030378D/07FEB18/1430/FX/ZP/DWC/463-50000000/DWC/SER 
	PAG030708D/07FEB18/1430/FX/J2/GYD/463-60000000/HKG/SER
	
	PALETLERIN EYNI ACTIONA AID OLMASI UCUN CARRIER+TARIX+ARRIVING+DEPARTING EYNI OLMALIDIR
	HER HANSI BIRININ FERQLI OLMASI HALINDA YENI ACTION YARADILIR
	
	UCM
	ZP000/07FEB181430.DWC
	OUT
	.PAG030038D/DWC.PAG030718D/DWC
	
	UCM
	J2000/07FEB181430.HKG
	OUT
	.PAG030708D/GYD
	
	LUC
	PMC00001ZP/22FEB18/1102/ZP/CV/DME/GYD

	UCM
	ZP807/22Feb.GYD
	OUT
	.PMC00001ZP/DME/B
	
	*/
	
	
	public static Set<Message> parse(Message messageObject,PaletAction paletAction) throws MyException{
		Set<Message> messagesSet=new HashSet<>();
		messageObject.addPaletAction(paletAction);
		
		//Set<LucAction> lucActionSet=new HashSet<>();
		String[] lucActionLines=ParsingTools.splitByLine(ParsingTools.removeFirstLineOf(messageObject.getText()));
		

		
		
		for (int i = 0; i < lucActionLines.length; i++) {
			messagesSet.add(parseLUCActionLine(lucActionLines[i],messageObject));
		}
		
/*		System.err.println("LUC parcalari");
		int a=0,b=0,c=0;
		for (Message message : messagesSet) {
			a++;
			System.err.println(a+". mesaj");
			System.out.println("fromEmail:"+message.getFromEmail());
			System.out.println("insert date:"+message.getInsertDate());
			System.out.println("reason:"+message.getReason());
			System.out.println("reason_user:"+message.getReason_user());
			System.out.println("receive date:"+message.getReceiveDate());
			System.out.println("sender:"+message.getSender());
			System.out.println("status:"+message.getStatus());
			System.out.println("type:"+message.getType());
			
			for (PaletAction paletAction2 : message.getPaletActions()) {
				b++;
				System.err.println(b+". paletaction");
				System.out.println("carrier:"+paletAction2.getCarrier());
				System.out.println("flight date:"+paletAction2.getFlightDate());
				System.out.println("arriving:"+paletAction2.getArriving());
				System.out.println("departing:"+paletAction2.getDeparting());
				for (Palet palet : paletAction2.getPaletSet()) {
					c++;
					System.err.println(c+". palet");
					System.out.println("type:"+palet.getType());
					System.out.println("code:"+palet.getCode());
					System.out.println("owner:"+palet.getOwner());
				}
				
			}
		}*/
		return messagesSet;
	}
	
	///message object qaytar
	
	private static Message parseLUCActionLine(String line,Message messageObject) throws MyException{
		String[] actionLinePartitions=ParsingTools.splitBySlash(line);
		if (actionLinePartitions.length==7) {
			messageObject=fillFirstFiveElements(actionLinePartitions, messageObject,6);
			
/*			String receiptNumber=null;
			System.out.println("receiptNumber::"+receiptNumber);
			
			String finalLocation=actionLinePartitions[6];
			System.out.println("finalLocation::"+finalLocation);
			
			String paletteCondition=null;
			System.out.println("paletteCondition::"+paletteCondition);
			*/
		}else if (actionLinePartitions.length==8) {
			String lastElement=actionLinePartitions[7];
			if (lastElement.equals("SER") || lastElement.equals("DAM") ) {
				messageObject=fillFirstFiveElements(actionLinePartitions,messageObject,6);

/*				String receiptNumber=null;
				System.out.println("receiptNumber::"+receiptNumber);

				String finalLocation=actionLinePartitions[6];
				System.out.println("finalLocation::"+finalLocation);

				String paletteCondition=lastElement;
				System.out.println("paletteCondition::"+paletteCondition);
*/
			}else{
				messageObject=fillFirstFiveElements(actionLinePartitions,messageObject,7);

/*				String receiptNumber=lastElement;
				System.out.println("receiptNumber::"+receiptNumber);

				String finalLocation=actionLinePartitions[7];
				System.out.println("finalLocation::"+finalLocation);

				String paletteCondition=null;
				System.out.println("paletteCondition::"+paletteCondition);
*/
			}
		}else if (actionLinePartitions.length==9) {
			messageObject=fillFirstFiveElements(actionLinePartitions, messageObject,7);
/*			String receiptNumber=actionLinePartitions[6];
			System.out.println("receiptNumber::"+receiptNumber);

			String finalLocation=actionLinePartitions[7];
			System.out.println("finalLocation::"+finalLocation);

			String paletteCondition=actionLinePartitions[8];
			System.out.println("paletteCondition::"+paletteCondition);
*/
			
			
			
			
		}else{
			 Log.error(MyException.getErrorLocation("LUC", "parseLUCActionLine", "wrong format:"+line));
			 throw new MyException(MyException.getErrorLocation("LUC", "parseLUCActionLine", "wrong format:"+line),"wrong format:"+line);

		}
		
		return messageObject;
	}
	
	private static Message fillFirstFiveElements(String[] actionLinePartitions,Message messageObject,int finalLocIndex) throws MyException{
		Palet palet=ParsingTools.parseSinglePalet(actionLinePartitions[0]);
	//	System.out.println("palet type::"+palet.getType()+" palet code::"+palet.getCode()+" palet owner::"+palet.getOwner());
		
		Date date=allocateDate(actionLinePartitions[1]+actionLinePartitions[2]);
	//	System.out.println("date::"+actionLinePartitions[1]+" time::"+actionLinePartitions[2]);
		
		String recCarrier=actionLinePartitions[3];
//		System.out.println("receive carrier::"+recCarrier);

/*		String senCarrier=actionLinePartitions[4];
		System.out.println("sender Carrier::"+senCarrier);
*/
		String transferLocation=actionLinePartitions[5];
	//	System.out.println("transfer location::"+transferLocation);

		String finalLocation=actionLinePartitions[finalLocIndex];
	//	System.out.println("finalLocation::"+finalLocation);
 
		
		PaletAction newPaletAction=null;
		int i=messageObject.getPaletActions().size();
		for (PaletAction paletAction : messageObject.getPaletActions()) {
			i--;
			if (paletAction.getFlightDate()==null && paletAction.getArriving()==null &&
					paletAction.getCarrier()==null && paletAction.getDeparting()==null) {
				paletAction.addPalet(palet);
				paletAction.setFlightDate(date);
				paletAction.setCarrier(recCarrier);
				paletAction.setArriving(transferLocation);
				paletAction.setDeparting(finalLocation);
			}else if (paletAction.getFlightDate().equals(date) && paletAction.getCarrier().equals(recCarrier)
					&& paletAction.getArriving().equals(transferLocation) && paletAction.getDeparting().equals(finalLocation)) {

				paletAction.addPalet(palet);

			}else if(i==0){

				newPaletAction=new PaletAction();
				newPaletAction.addPalet(palet);
				newPaletAction.setFlightDate(date);
				newPaletAction.setCarrier(recCarrier);
				newPaletAction.setArriving(transferLocation);
				newPaletAction.setDeparting(finalLocation);
			}

		}
		if (newPaletAction!=null) {
			messageObject.addPaletAction(newPaletAction);
		}
		
		return messageObject;
	}
	
	private static Date allocateDate(String dateString){
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("ddMMMyyhhmm");
		Date date=new Date();
		
		try {
			date=dateFormat.parse(dateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	

	
}
