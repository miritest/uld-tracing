package net.myapp.utils.messagekinds;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.myapp.common.logging.impl.Log;
import net.myapp.dao.models.Message;
import net.myapp.dao.models.Palet;
import net.myapp.dao.models.PaletAction;
import net.myapp.utils.ParsingTools;
import net.myapp.exceptions.MyException;

public class UCM {
	//main parsing
		public static Set<Message> parse(Message messageObject, PaletAction paletAction) throws MyException, ParseException{
			 Set<Message> messagesSet=new HashSet<>();
			 Map<String, PaletAction> mapPaletAction=new HashMap<>();
			 
			
			String messageText=messageObject.getText().trim();


			messageText=ParsingTools.removeAllNills(messageText);	

			messageText=removeUCM(messageText);

			messageText=parseAndRemoveFlightInformation(messageText,paletAction);
			
			
			if (messageText.equals("")) {

						messageObject.setPaletActions(addPaletAction(ParsingTools.convertMapToSet(mapPaletAction), paletAction));
						messageObject=ParsingTools.checkMessage(messageObject);
						messagesSet.add(messageObject);
						
			}else if(messageText.contains("IN")){
				if (messageText.contains("OUT")) {
					messagesSet=parseDoubleMessage(messageText,messageObject,paletAction, messagesSet, mapPaletAction);
				}else{
				messagesSet=parseSingleMessage(messageText,messageObject,paletAction, messagesSet, mapPaletAction);
				}
			}else {
				messagesSet=parseSingleMessage(messageText,messageObject,paletAction, messagesSet, mapPaletAction);
			}
			


			
			return messagesSet;
		}

		private static Set<Message> parseSingleMessage(String messageText,Message messageObject, PaletAction paletAction,Set<Message> messagesSet,Map<String,PaletAction> mapPaletAction) throws MyException{
			//6. message type teyin edilir
			//System.err.println("\n before type:"+messageObject.getType());
			//messageText=setUCMMessageType(messageText, messageObject);
					
			messageObject.setType(getUCMMessageType(messageText));

			//remove message type
			messageText=ParsingTools.removeFirstLineOf(messageText);
		//	System.err.println("\n then type:"+messageObject.getType());
			//7. PALETLERI PARS ET

			messageObject.setPaletActions(ParsingTools.convertMapToSet(parsePalettes(messageText,messageObject,paletAction,mapPaletAction)));
		//	System.err.println("\n after actions size:"+messageObject.getPaletActions().size());
			messageObject=ParsingTools.checkMessage(messageObject);
			messagesSet.add(messageObject);
			return messagesSet;
		}

		private static Set<Message> parseDoubleMessage(String messageText,Message messageObject, PaletAction paletAction,  Set<Message> messagesSet, Map<String, PaletAction> mapPaletAction) throws MyException{
		//	System.out.println("after removal of IN palets:"+ParsingTools.removeINPalettes(messageObject.getText()));
			//System.out.println("after removal of OUT palets:"+ParsingTools.removeOUTPalettes(messageObject.getText()));
			
			PaletAction paletActionForDouble=new PaletAction(paletAction);
			Map<String, PaletAction> paletActionMapForDouble=new HashMap<>();
			Message messageObjectForDouble=new Message(messageObject.getType(), ParsingTools.removeOUTPalettes(messageObject.getText()), messageObject.getReceiveDate(), messageObject.getStatus(), messageObject.getReason(), messageObject.getSender(), messageObject.getFromEmail(), messageObject.getSubject());
			String messageText2=ParsingTools.removeOUTPalettes(messageText);
		//	System.err.println("\n before type:"+messageObject.getType());
			//messageText2=setUCMMessageType(messageText2,messageObjectForDouble);
			messageObjectForDouble.setType(getUCMMessageType(messageText2));
			messageText2=ParsingTools.removeFirstLineOf(messageText2);
			//System.err.println("\n then type:"+messageObject.getType());
		//	System.out.println("double ici:"+messageText2+" mesaj type:"+messageObjectForDouble.getType());
			
			messageObjectForDouble.setPaletActions(ParsingTools.convertMapToSet(parsePalettes(messageText2, messageObjectForDouble, paletActionForDouble, paletActionMapForDouble)));
			messageObjectForDouble=ParsingTools.checkMessage(messageObjectForDouble);
			messagesSet.add(messageObjectForDouble);
			
			messageText2=ParsingTools.removeINPalettes(messageText);
		//	System.err.println("\n before type:"+messageObject.getType());
		//	messageText2=setUCMMessageType(messageText2,messageObject);	
			messageObject.setType(getUCMMessageType(messageText2));

			messageText2=ParsingTools.removeFirstLineOf(messageText2);
			//System.err.println("\n then type:"+messageObject.getType());
			//System.out.println("double ici:"+messageText2+" mesaj type:"+messageObject.getType());
			
			messageObject.setText(ParsingTools.removeINPalettes(messageObject.getText()));	
			messageObject.setPaletActions(ParsingTools.convertMapToSet(parsePalettes(messageText2, messageObject, paletAction, mapPaletAction)));
			messageObject=ParsingTools.checkMessage(messageObject);
			messagesSet.add(messageObject);
		//	System.out.println("ucm mes claS: messSet size:"+messagesSet.size());
			return messagesSet;
		}
		
		private static Map<String, PaletAction> parsePalettes(String messageText, Message messageModel, PaletAction paletActionModel, Map<String, PaletAction> paletActionMap) throws MyException{
			String[] palettesMassive=ParsingTools.splitByDot(messageText);
			
			if (messageModel.getType().equals("OUT")) {
				for (int i = 0; i < palettesMassive.length; i++) {
					if (palettesMassive[i].trim().equals("")) {
						continue;
					}
					String[] palettePartitions=ParsingTools.splitBySlash(palettesMassive[i].trim());
					/*for (int j = 0; j < palettePartitions.length; j++) {
						System.out.println(palettePartitions[j]);
					}*/
					Palet palet=getPaletFromOUTMessage(palettePartitions,paletActionModel);
					String depart=ParsingTools.getDepart(palettePartitions, paletActionModel.getCarrier()+paletActionModel.getFlightNo(),paletActionModel.getArriving());
					//PaletAction depart
					paletActionModel.setDeparting(depart);
					//Palet position
					palet.setPosition(paletActionModel.getArriving()+"->"+depart);
					if(paletActionMap.containsKey(depart)){
						for(Map.Entry m:paletActionMap.entrySet()){  
							  if (m.getKey().equals(depart)) {
								((PaletAction)m.getValue()).addPalet(palet);
							}
							 //  System.out.println(m.getKey()+" "+m.getValue());  
							  } 
					}else{
						Set<Palet> paletSet=new HashSet<Palet>();
						paletActionModel.setPaletSet(paletSet);
						PaletAction newPaletAction=new PaletAction(paletActionModel);
						newPaletAction.addPalet(palet);
						newPaletAction.setDeparting(depart);
						paletActionMap.put(newPaletAction.getDeparting(), newPaletAction);
					}
				}
				
			}else if(messageModel.getType().equals("IN")){
				for (int i = 0; i < palettesMassive.length; i++) {
					if (palettesMassive[i].trim().equals("")) {
						continue;
					}
					paletActionModel.addPalet(getPaletFromINMessage(palettesMassive[i].trim(),paletActionModel));
				}	
				paletActionMap.put("d", paletActionModel);

			}
			
		//	System.out.println("map size:"+paletActionMap.size());
			return paletActionMap;
			
		}

		private static Palet getPaletFromINMessage(String paletString, PaletAction  paletAction) throws MyException{
			Palet palet=new Palet();
			
			//PALETTE TYPE

			try {

			
		//	if (ParsingTools.containingOnlyLetters(paletteType)) {
				palet.setType(paletString.substring(0,3));
/*			}else{
				Log.error(MyException.getErrorLocation("Parse", "parsePaletteInMSG", "Wrong PALETTE TYPE "+paletteType));
				throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteInMSG", "Wrong PALETTE TYPE "+paletteType),"Wrong PALETTE TYPE "+paletteType);

			}*/
			} catch (Exception e) {
				if (e.getClass()==MyException.class) {
					Log.error(((MyException)e).getReason());
					throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
				}else{
					Log.error(e.getMessage());
					throw new MyException(e.getMessage(), "Parsing error");
				}				
			}
			
			try {
			String codePlusOwner=paletString.substring(3);
			System.out.println("HEY!\n"+codePlusOwner);
			int index=ParsingTools.allocateCodePlusOwner(codePlusOwner);
			System.out.println(codePlusOwner.substring(0, index)+"---"+codePlusOwner.substring(index));
			
			//PALETTE CODE
			palet.setCode(codePlusOwner.substring(0, index));
			//PALETTE OWNER
			palet.setOwner(codePlusOwner.substring(index));
			} catch (Exception e) {
				if (e.getClass()==MyException.class) {
					Log.error(((MyException)e).getReason());
					throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
				}else{
					Log.error(e.getMessage());
					throw new MyException(e.getMessage(), "Parsing error");
			}				
		}
			

			
			
			
			
/*			//PALETTE CODE

			try {
				String paletteCode=paletString.substring(3,8);
		
		    if (ParsingTools.containingOnlyDigits(paletteCode)) {
				palet.setCode((paletteCode));
			}else{
				Log.error(MyException.getErrorLocation("Parse", "parsePaletteInMSG", "Wrong PALETTE CODE "+paletteCode));
				throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteInMSG", "Wrong PALETTE CODE "+paletteCode),"Wrong PALETTE CODE "+paletteCode);

			}
			} catch (Exception e) {
				if (e.getClass()==MyException.class) {
					Log.error(((MyException)e).getReason());
					throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
				}else{
					Log.error(e.getMessage());
					throw new MyException(e.getMessage(), "Parsing error");
				}	
			}
			

		      	
		    //PALETTE OWNER
			try {
			String paletteOwner=paletString.substring(8);
			System.out.println("owner:"+paletteOwner);
			//if (ParsingTools.containingOnlyLetters(paletteOwner)) {
				palet.setOwner(paletteOwner);
			}else{
				Log.error(MyException.getErrorLocation("Parse", "parsePaletteInMSG", "Wrong PALETTE OWNER "+paletteOwner));
				throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteInMSG", "Wrong PALETTE OWNER "+paletteOwner),"Wrong PALETTE OWNER "+paletteOwner);
	
			}
			} catch (Exception e) {
				if (e.getClass()==MyException.class) {
					Log.error(((MyException)e).getReason());
					throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
				}else{
					Log.error(e.getMessage());
					throw new MyException(e.getMessage(), "Parsing error");
				}	}*/		
			
			//PALETTE POSITION
			palet.setPosition(paletAction.getArriving());
		
			return palet;
		}

		private static Palet getPaletFromOUTMessage(String[] paletArray, PaletAction paletAction) throws MyException{
			Palet palet=new Palet();
			
			//PALETTE TYPE
			try {
				String paletteType=paletArray[0].substring(0,3);
				if (ParsingTools.containingOnlyLetters(paletteType)) {
					palet.setType(paletteType);
				}else{
					Log.error(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "Wrong PALETTE TYPE "+paletteType));
					throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "Wrong PALETTE TYPE "+paletteType),"Wrong PALETTE TYPE "+paletteType);				
				}
			
			} catch (Exception e) {
				if (e.getClass()==MyException.class) {
					Log.error(((MyException)e).getReason());
					throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
				}else{
					Log.error(e.getMessage());
					throw new MyException(e.getMessage(), "Parsing error");
				}
				}
			
			try {
			String codePlusOwner=paletArray[0].substring(3);
			System.out.println("HEY!\n"+codePlusOwner);
			int index=ParsingTools.allocateCodePlusOwner(codePlusOwner);
			System.out.println(codePlusOwner.substring(0, index)+"---"+codePlusOwner.substring(index));
			
			//PALETTE CODE
			palet.setCode(codePlusOwner.substring(0, index));
			//PALETTE OWNER
			palet.setOwner(codePlusOwner.substring(index));
		} catch (Exception e) {
			if (e.getClass()==MyException.class) {
				Log.error(((MyException)e).getReason());
				throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
			}else{
				Log.error(e.getMessage());
				throw new MyException(e.getMessage(), "Parsing error");
		}				
	}
			
			
/*			//PALETTE CODE
			try {
						String paletteCode=paletArray[0].substring(3,8);
		
				if (ParsingTools.containingOnlyDigits(paletteCode)) {
					palet.setCode((paletteCode));
				}	else{
					Log.error(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "Wrong PALETTE CODE "+paletteCode));
					throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "Wrong PALETTE CODE "+paletteCode),"Wrong PALETTE CODE "+paletteCode);

				}
				
			} catch (Exception e) {
				if (e.getClass()==MyException.class) {
					Log.error(((MyException)e).getReason());
					throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
				}else{
					Log.error(e.getMessage());
					throw new MyException(e.getMessage(), "Parsing error");
				}
			}

			
		      	
		    //PALETTE OWNER
				try {
					
					String paletteOwner=paletArray[0].substring(8);
		//	if (ParsingTools.containingOnlyLetters(paletteOwner)) {
				palet.setOwner(paletteOwner);
			}else{
				Log.error(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "Wrong PALETTE OWNER "+paletteOwner));
				throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "Wrong PALETTE OWNER "+paletteOwner),"Wrong PALETTE OWNER "+paletteOwner);

			}
				} catch (Exception e) {
					if (e.getClass()==MyException.class) {
						Log.error(((MyException)e).getReason());
						throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
					}else{
						Log.error(e.getMessage());
						throw new MyException(e.getMessage(), "Parsing error");
					}
				}*/

				//CHECK
				palet.setChecked(false);
				
	/*		//DEPART
				String depart;
			try {
				// depart=paletArray[1].substring(0, 3).trim();
				depart=ParsingTools.getDepart(paletArray);
			//	 System.out.println("partitions:  "+palettePartitions[1]);
				
				 if (ParsingTools.containingOnlyLetters(depart)) {
					 paletAction.setDeparting(depart);
			}	
				} catch (Exception e) {
					// TODO: handle exception	
					Log.error(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "depart", e));
					throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "depart", e));
				}
					
			
			//POSITION
				palet.setPosition("On Air-->  "+depart);
	*/

			return palet;
		}


		//only messageText parsing
		public static Set<Palet> parse(String messageText, Set<Palet> paletsSet) throws MyException{
			messageText=ParsingTools.removeAllNills(messageText);
		//	System.out.println("\nall nill remove:"+messageText);
			messageText=removeUCM(messageText);
		//	System.out.println("\nucm remove:"+messageText);

			String arrive=ParsingTools.allocateArriving(ParsingTools.getFirstLineOf(messageText));
		//	System.out.println("\n arrive:"+arrive);
			//remove flight line
			messageText=ParsingTools.removeFirstLineOf(messageText);
		//	System.out.println("\n flight info remove:"+messageText);
			
			if (messageText.equals("")) {
				
			}else if(messageText.contains("IN")){
				if (messageText.contains("OUT")) {
					//paletsSet=parseDoubleMessage(messageText, paletsSet);
				}else{
					paletsSet=parseSingleMessage(messageText, paletsSet,arrive);
				}
			}else if(messageText.contains("OUT")){
				paletsSet=parseSingleMessage(messageText, paletsSet,arrive);
			}
			return paletsSet;
		}
		
		private static Set<Palet> parseSingleMessage(String messageText, Set<Palet> paletsSet, String arriving) throws MyException{
	/*		//remove message type
			messageText=ParsingTools.removeFirstLineOf(messageText);*/
			String messageType=ParsingTools.getFirstLineOf(messageText);
			messageText=ParsingTools.removeFirstLineOf(messageText);
			paletsSet=parsePalettes(messageText, paletsSet, arriving, messageType);
			
			return paletsSet;
		}
		
		private static Set<Palet> parsePalettes(String messageText, Set<Palet> paletsSet, String arriving,String messageType) throws MyException{
			String[] palettesMassive=ParsingTools.splitByDot(messageText);
			
			if (messageType.equals("OUT")) {
				for (int i = 0; i < palettesMassive.length; i++) {
					if (palettesMassive[i].trim().equals("")) {
						continue;
					}
					String[] palettePartitions=ParsingTools.splitBySlash(palettesMassive[i].trim());
					/*for (int j = 0; j < palettePartitions.length; j++) {
						System.out.println(palettePartitions[j]);
					}*/
					Palet palet=getPaletFromOUTMessage(palettePartitions);
					String depart=ParsingTools.getDepart(palettePartitions,"","");

					//Palet position
					palet.setPosition(arriving+"->"+depart);
					paletsSet.add(palet);

				
				}
				
			}else if(messageType.equals("IN")){
				for (int i = 0; i < palettesMassive.length; i++) {
					if (palettesMassive[i].trim().equals("")) {
						continue;
					}
					paletsSet.add(getPaletFromINMessage(palettesMassive[i].trim(),arriving));
				}	
			}
			return paletsSet;
		}
		
		private static Palet getPaletFromINMessage(String paletString, String arriving) throws MyException{
			Palet palet=new Palet();
			
			//PALETTE TYPE

			try {
			String paletteType=paletString.substring(0,3);
			
			if (ParsingTools.containingOnlyLetters(paletteType)) {
				palet.setType(paletteType);
			}else{
				Log.error(MyException.getErrorLocation("Parse", "parsePaletteInMSG", "Wrong PALETTE TYPE "+paletteType));
				throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteInMSG", "Wrong PALETTE TYPE "+paletteType),"Wrong PALETTE TYPE "+paletteType);

			}
			} catch (Exception e) {
				if (e.getClass()==MyException.class) {
					Log.error(((MyException)e).getReason());
					throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
				}else{
					Log.error(e.getMessage());
					throw new MyException(e.getMessage(), "Parsing error");
				}
			}
		
			try {
			String codePlusOwner=paletString.substring(3);
			System.out.println("HEY!\n"+codePlusOwner);
			int index=ParsingTools.allocateCodePlusOwner(codePlusOwner);
			System.out.println(codePlusOwner.substring(0, index)+"---"+codePlusOwner.substring(index));
			
			//PALETTE CODE
			palet.setCode(codePlusOwner.substring(0, index));
			//PALETTE OWNER
			palet.setOwner(codePlusOwner.substring(index));
			} catch (Exception e) {
				if (e.getClass()==MyException.class) {
					Log.error(((MyException)e).getReason());
					throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
				}else{
					Log.error(e.getMessage());
					throw new MyException(e.getMessage(), "Parsing error");
			}				
		}
			
			
/*			//PALETTE CODE

			try {
				String paletteCode=paletString.substring(3,8);
		
		    if (ParsingTools.containingOnlyDigits(paletteCode)) {
				palet.setCode((paletteCode));

			}else{
				Log.error(MyException.getErrorLocation("Parse", "parsePaletteInMSG", "Wrong PALETTE CODE "+paletteCode));
				throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteInMSG", "Wrong PALETTE CODE "+paletteCode),"Wrong PALETTE CODE "+paletteCode);

			}
			} catch (Exception e) {
				if (e.getClass()==MyException.class) {
					Log.error(((MyException)e).getReason());
					throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
				}else{
					Log.error(e.getMessage());
					throw new MyException(e.getMessage(), "Parsing error");
				}
			}
			

		      	
		    //PALETTE OWNER
			try {
			String paletteOwner=paletString.substring(8);
			
	//		if (ParsingTools.containingOnlyLetters(paletteOwner)) {
				palet.setOwner(paletteOwner);
			}	else{
				Log.error(MyException.getErrorLocation("Parse", "parsePaletteInMSG", "Wrong PALETTE OWNER "+paletteOwner));
				throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteInMSG", "Wrong PALETTE OWNER "+paletteOwner),"Wrong PALETTE OWNER "+paletteOwner);

			}
			} catch (Exception e) {
				if (e.getClass()==MyException.class) {
					Log.error(((MyException)e).getReason());
					throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
				}else{
					Log.error(e.getMessage());
					throw new MyException(e.getMessage(), "Parsing error");
				}}	*/
			
			
			//PALETTE POSITION
			palet.setPosition(arriving);
			
			return palet;
		}

		private static Palet getPaletFromOUTMessage(String[] paletArray) throws MyException{
			Palet palet=new Palet();
			
			//PALETTE TYPE
			try {
				String paletteType=paletArray[0].substring(0,3);
				if (ParsingTools.containingOnlyLetters(paletteType)) {
					palet.setType(paletteType);
				}else{
					Log.error(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "Wrong PALETTE TYPE "+paletteType));
					throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "Wrong PALETTE TYPE "+paletteType),"Wrong PALETTE TYPE "+paletteType);

				}
			
			} catch (Exception e) {
				if (e.getClass()==MyException.class) {
					Log.error(((MyException)e).getReason());
					throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
				}else{
					Log.error(e.getMessage());
					throw new MyException(e.getMessage(), "Parsing error");
				}
				}
			try {
			String codePlusOwner=paletArray[0].substring(3);
			System.out.println("HEY!\n"+codePlusOwner);
			int index=ParsingTools.allocateCodePlusOwner(codePlusOwner);
			System.out.println(codePlusOwner.substring(0, index)+"---"+codePlusOwner.substring(index));
			
			//PALETTE CODE
			palet.setCode(codePlusOwner.substring(0, index));
			//PALETTE OWNER
			palet.setOwner(codePlusOwner.substring(index));
		} catch (Exception e) {
			if (e.getClass()==MyException.class) {
				Log.error(((MyException)e).getReason());
				throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
			}else{
				Log.error(e.getMessage());
				throw new MyException(e.getMessage(), "Parsing error");
		}				
	}
			
			
/*			//PALETTE CODE
			try {
						String paletteCode=paletArray[0].substring(3,8);
		
				if (ParsingTools.containingOnlyDigits(paletteCode)) {
					palet.setCode((paletteCode));
				}else{
					Log.error(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "Wrong PALETTE CODE "+paletteCode));
					throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "Wrong PALETTE CODE "+paletteCode),"Wrong PALETTE CODE "+paletteCode);

				}
				
			} catch (Exception e) {
				if (e.getClass()==MyException.class) {
					Log.error(((MyException)e).getReason());
					throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
				}else{
					Log.error(e.getMessage());
					throw new MyException(e.getMessage(), "Parsing error");
				}
			}

			
		      	
		    //PALETTE OWNER
				try {
					
					String paletteOwner=paletArray[0].substring(8);
	//		if (ParsingTools.containingOnlyLetters(paletteOwner)) {
				palet.setOwner(paletteOwner);
			}else{
				Log.error(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "Wrong PALETTE OWNER "+paletteOwner));
				throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "Wrong PALETTE OWNER "+paletteOwner),"Wrong PALETTE OWNER "+paletteOwner);

			}
				} catch (Exception e) {
					if (e.getClass()==MyException.class) {
						Log.error(((MyException)e).getReason());
						throw new MyException(((MyException)e).getReason(),((MyException)e).getReason_user());
					}else{
						Log.error(e.getMessage());
						throw new MyException(e.getMessage(), "Parsing error");
					}
				}*/

	/*		//DEPART
				String depart;
			try {
				// depart=paletArray[1].substring(0, 3).trim();
				depart=ParsingTools.getDepart(paletArray);
			//	 System.out.println("partitions:  "+palettePartitions[1]);
				
				 if (ParsingTools.containingOnlyLetters(depart)) {
					 paletAction.setDeparting(depart);
			}	
				} catch (Exception e) {
					// TODO: handle exception	
					Log.error(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "depart", e));
					throw new MyException(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "depart", e));
				}
					
			
			//POSITION
				palet.setPosition("On Air-->  "+depart);
	*/

			return palet;
		}
		
		
		
		private static String getUCMMessageType(String messageText) throws MyException{
			return ParsingTools.getFirstLineOf(messageText);
		}
		
		private static String setUCMMessageType(String messageText, Message messageModel) throws MyException{
			messageModel.setType(ParsingTools.getFirstLineOf(messageText).trim());
			return ParsingTools.removeFirstLineOf(messageText);
		}
		
		
		
		
		//flight information parsing
		private static String parseAndRemoveFlightInformation(String messageText, PaletAction paletAction) throws MyException, ParseException{
			/*	int indexOfFirstLine=messageText.indexOf("\r\n");
				System.out.println("first line index::"+indexOfFirstLine);*/
				
				String flightInfoString=ParsingTools.getFirstLineOf(messageText);
			//	System.out.println(flightInfoString);
				
				 String[] flightInfoMassive=ParsingTools.splitBySlash(flightInfoString);
				 //CARRIER
				 setCarrier(flightInfoMassive[0],paletAction);
				 //FLIGHT CODE
				 setFlightCode(flightInfoMassive[0],paletAction);	
				 
				 flightInfoMassive=ParsingTools.splitByDot(flightInfoMassive[1]);
				 //FLIGHT DATE
				 setFlightDate(flightInfoMassive[0],paletAction);
				 if (flightInfoMassive.length==2) {
					 //AIRCRAFT
					setAircraft("",paletAction);
					 //ARRIVE
					 setArrive(flightInfoMassive[1],paletAction);
				 }else{
					 //AIRCRAFT
					 setAircraft(flightInfoMassive[1],paletAction);
					 //ARRIVE
					 setArrive(flightInfoMassive[2],paletAction);
				 }

				return ParsingTools.removeFirstLineOf(messageText);
			}

		private static void setFlightDate(String flightInfoString, PaletAction paletAction) throws MyException, ParseException{
			 //FLIGHT DATE
			 try{
				 
				 paletAction.setFlightDate(ParsingTools.toDate(flightInfoString));

			 } catch (MyException e) {
				 // TODO: handle exception	
				Log.error(e.getReason());
				throw new MyException(e.getReason(),e.getReason_user());
		 
			 }	
		}

		private static void setFlightCode(String flightInfoString, PaletAction paletAction) throws MyException{
			 //FLIGHT CODE
			String flightCode="";
			 try{
				 flightCode=flightInfoString.substring(2).trim();
				  
			 } catch (Exception e) {
					Log.error(MyException.getErrorLocation("UCM", "setFlightCode", "Wrong FlightCode "+flightCode));
					throw new MyException(MyException.getErrorLocation("UCM", "setFlightCode", "Wrong FlightCode "+flightCode),"Wrong FlightCode "+flightCode);	
					}	
			 
	//		 if (ParsingTools.containingOnlyDigits(flightCode) && flightCode!=null) {
				 paletAction.setFlightNo(flightCode);
/*			 }else{
				Log.error(MyException.getErrorLocation("UCM", "setFlightCode", "Wrong FlightCode "+flightCode));
				throw new MyException(MyException.getErrorLocation("UCM", "setFlightCode", "Wrong FlightCode "+flightCode),"Wrong FlightCode "+flightCode);
			}*/
		}

		private static void setCarrier(String flightInfoString, PaletAction paletAction) throws MyException{
			 //CARRIER
			 String carrier="";
			 try{
				 carrier=flightInfoString.substring(0, 2).trim();
			 } catch (Exception e) {
				 // TODO: handle exception	
				 Log.error(MyException.getErrorLocation("UCM", "setCarrier", "Wrong Carrier "+carrier, e));
				 throw new MyException(MyException.getErrorLocation("UCM", "setCarrier", "Wrong Carrier "+carrier, e),"Wrong Carrier "+carrier);
			 }
			 paletAction.setCarrier(carrier);

			 
		}

		private static void setAircraft(String flightInfoString, PaletAction paletAction) throws MyException{
			 //AIRCRAFT REGISTRATION
			 try{
				 paletAction.setAircraft(flightInfoString);		 
			 } catch (Exception e) {
				 // TODO: handle exception	
				 Log.error(MyException.getErrorLocation("Parse", "parseFlightInformation", "Wrong AIRCRAFT "+flightInfoString, e));
				 throw new MyException(MyException.getErrorLocation("Parse", "parseFlightInformation", "Wrong AIRCRAFT "+flightInfoString, e),"Wrong AIRCRAFT "+flightInfoString);
			 }	
		}

		private static void setArrive(String flightInfoString, PaletAction paletAction) throws MyException{
			//ARRIVE
			 String arrive="";
			 try{
					arrive=flightInfoString; 
			 } catch (Exception e) {
				 // TODO: handle exception	
				 Log.error(MyException.getErrorLocation("Parse", "parseFlightInformation", "Wrong ARRIVE "+arrive, e));
				 throw new MyException(MyException.getErrorLocation("Parse", "parseFlightInformation", "Wrong ARRIVE "+arrive, e),"Wrong ARRIVE "+arrive);

			 }		

		
			 if (ParsingTools.containingOnlyLetters(arrive)) {
				paletAction.setArriving(arrive);
			}else{
				Log.error(MyException.getErrorLocation("Parse", "parseFlightInformation", "Wrong ARRIVE "+arrive));
				throw new MyException(MyException.getErrorLocation("Parse", "parseFlightInformation", "Wrong ARRIVE "+arrive),"Wrong ARRIVE "+arrive);
			}
		}

		private static String removeUCM(String messageText) throws MyException{


		//	System.out.println("basliq:"+ParsingTools.getFirstLineOf(messageText));
			
			if (ParsingTools.getFirstLineOf(messageText).trim().equals("UCM")) {
				return ParsingTools.removeFirstLineOf(messageText).trim();

			}else{
				Log.error(MyException.getErrorLocation("UCM", "removeUcm", "Message type error"));
				throw new MyException(MyException.getErrorLocation("UCM", "removeUcm", "Message type error"),"Message type error");
			}

		

		}

		private static Set<PaletAction> addPaletAction(Set<PaletAction> paletActionsSet,PaletAction paletAction){
			
			paletActionsSet.add(paletAction);
			return paletActionsSet;
		}
}
