package net.myapp.utils;

import java.util.ArrayList;
import java.util.List;


import net.myapp.dao.form.methods.FormDAO_ADT;

public class StaticVariables {

private static List<String>palet_types=new ArrayList<>();
private static List<String>palet_owners=new ArrayList<>();

public static List<String> getPalet_types() {
	return palet_types;
}

public static void setPalet_types(List<String> palet_types) {
	StaticVariables.palet_types = palet_types;
}

public static List<String> getPalet_owners() {
	System.err.println("--------GET MEthod running---------");
	return palet_owners;
}

public static void setPalet_owners(List<String> palet_owners) {
	StaticVariables.palet_owners = palet_owners;
}	

public static void fillFormList(FormDAO_ADT formDAO){
	formDAO.fillFormList();
}
	
}
