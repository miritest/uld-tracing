package net.myapp.utils;

import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.myapp.utils.messagekinds.*;
import net.myapp.common.logging.impl.Log;
import net.myapp.dao.models.Message;
import net.myapp.dao.models.Palet;
import net.myapp.dao.models.PaletAction;
import net.myapp.exceptions.MyException;

public class Parse  {

	public static Set<Message> parse(Message messageObject) throws MyException, ParseException{
		PaletAction paletAction=new PaletAction();	
		System.out.println(messageObject.getType());
		
		switch (messageObject.getType()) {
		
		case "UCM":
			
			return UCM.parse( messageObject, paletAction);
			
		case "SCM":
			
			return SCM.parse(messageObject, paletAction);
			
		case "LUC":
			return LUC.parse(messageObject, paletAction);
			
		default:
			Log.error(MyException.getErrorLocation("Parse", "parse",messageObject.getType()+" yanlisdir"));
			throw new MyException(MyException.getErrorLocation("Parse", "parse",messageObject.getType()+" yanlisdir"), "Unknown Message Type!");
		}
	}
	
	public static Set<Palet> parse(String messageText) throws MyException{
		Set<Palet> paletsSet=new HashSet();
		String messageType=ParsingTools.getFirstLineOf(messageText);
		
		switch (messageType) {
		case "UCM":
				paletsSet=UCM.parse(messageText, paletsSet);
			return paletsSet;
		case "SCM":
				paletsSet=SCM.parse(messageText, paletsSet);
			return paletsSet;
		default:
			Log.error(MyException.getErrorLocation("Parse", "parse",messageType+" yanlisdir"));
			throw new MyException(MyException.getErrorLocation("Parse", "parse",messageType+" yanlisdir"), "Unknown Message Type!");
		}
		
		
	}

	
}
