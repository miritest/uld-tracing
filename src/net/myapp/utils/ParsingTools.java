package net.myapp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.myapp.common.logging.impl.Log;
import net.myapp.dao.methods.MessageDAO_ADT;
import net.myapp.dao.methods.PaletActionDAO_ADT;
import net.myapp.dao.methods.StandartFlightDAO_ADT;
import net.myapp.dao.models.Message;
import net.myapp.dao.models.Palet;
import net.myapp.dao.models.PaletAction;
import net.myapp.dao.models.StandartFlight;
import net.myapp.exceptions.MyException;


@Component
public class ParsingTools {

	private static StandartFlightDAO_ADT standartFlightDAO;
	
	@Autowired(required=true)
	@Qualifier("StandartFlightDAO")
	private StandartFlightDAO_ADT standartFlightDAO0;
	
	@PostConstruct     
	  private void initStaticDao () {
		standartFlightDAO = this.standartFlightDAO0;
	  }

	public static void test(){
		
		
		StandartFlight standartFlight=null;
		standartFlight=standartFlightDAO.findByFligth("J2101");
		
		String depart=standartFlight.getDeparture();
		String dest=standartFlight.getDestination();
		String flight=standartFlight.getFlightCode();
		System.out.println("depart:"+depart+"\ndestin:"+dest+"\nflight:"+flight);
	}
	
	public static String[] splitBySlash(String message) {
		return message.split("/");
	}
	
	public static String[] splitByDot(String message){
		return message.split("\\.");
	}
	
	public static String[] splitByLine(String message){
		return message.split("\\r\\n");
	}
	
	public static String[] splitByDotT(String message){
		return message.split("\\.T\\d{1,3}");
	} 
		
	public static int[] getCountOfPalettes(String message){
			
		String []counts=message.split("\\/\\d{5}\\w{2}\\.T");
		int[] paletteCounts=new int[counts.length];
		for (int i = 0; i < counts.length; i++) {
			paletteCounts[i]=Integer.valueOf(counts[i]);
		}
		return paletteCounts;
	}
	
	public static Date toDate(String dateString) throws MyException, ParseException{
		
		if (dateString.length()==2) {
			dateString+=new SimpleDateFormat("MMMYYYYhhmm").format(Calendar.getInstance().getTime());
		}else if(dateString.length()==5){
			dateString+=new SimpleDateFormat("YYYYhhmm").format(Calendar.getInstance().getTime());
		}
		//System.out.println(dateString);
		SimpleDateFormat dateFormat=new SimpleDateFormat("ddMMMyyyyhhmm");
		Date date=new Date();
		
		 try{
			 	 date=dateFormat.parse(dateString);
		 } catch (Exception e) {
			 System.err.println(e.getMessage());
			 Log.error(MyException.getErrorLocation("ParsingTools", "toDate", dateString+" yanlisdir"));
			 throw new MyException(MyException.getErrorLocation("ParsingTools", "toDate", dateString+" yanlisdir"),"Wrong Time/Date Format!");

		 }		
		 
		
		System.out.println("date format++"+date);
		return date;
	}
	
	public static String parseDateString(String dateString) throws MyException{
		if (dateString.length()-3>=0) {
			return dateString.substring(0, dateString.length()-3)+"-"+dateString.substring(dateString.length()-3);
		}else{
			Log.error(MyException.getErrorLocation("ParsingTools", "parseDateString", dateString+" yanlisdir"));
			throw new MyException(MyException.getErrorLocation("ParsingTools", "parseDateString", dateString+" yanlisdir"), "Inaccurate Date/Time");
		}
		
	}
	
	public static String parseExceptionString(String exception){

		String[] splitted=exception.split("\\.");
		
		return splitted[splitted.length-1];
	}

	public static boolean containingOnlyLetters(String string){
		Pattern pattern = Pattern.compile(StaticPatterns.getOnlywordpattern());
		Matcher matcher = pattern.matcher(string.trim());
		
		return matcher.matches();
	}

	public static boolean containingOnlyDigits(String string){
		  Pattern pattern = Pattern.compile(StaticPatterns.getOnlydigitpattern());
		  Matcher matcher = pattern.matcher(string.trim());
		  return matcher.matches();
	}
	
	public static String getDepart(String[] palettePartitions, String flightCode, String arriving) throws MyException{
		String depart=null;
		try {
			if (palettePartitions.length==2) {
				depart=palettePartitions[1].substring(0, 3).trim();
			}else{
				StandartFlight standartFlight=standartFlightDAO.findByFligth(flightCode);
				if (standartFlight.getDeparture().trim().equals(arriving.trim())) {
					depart=standartFlight.getDestination();
				}else{
					Log.error(MyException.getErrorLocation("ParsingTools", "getDepart", "For "+flightCode+" "+arriving+"(on msg)!= "+standartFlight.getDeparture()+"(on DB)"));
					throw new MyException(MyException.getErrorLocation("ParsingTools", "getDepart", "For "+flightCode+" "+arriving+"(on msg)!= "+standartFlight.getDeparture()+"(on DB)"),"For "+flightCode+" "+arriving+"(on msg)!= "+standartFlight.getDeparture()+"(on DB)");
				
				}
			}
			
			// System.out.println("partitions:  "+palettePartitions[1]);
			
		
			} catch (Exception e) {
				// TODO: handle exception	
				Log.error(MyException.getErrorLocation("Parse", "parsePaletteOutMSG", "depart", e));
				throw new MyException(MyException.getErrorLocation("ParsingTools", "getDepart", "inaccurate "+depart, e),"inaccurate "+depart);
			}
		return depart;
	}
	
	public static Set<PaletAction> hashMapToSet(Map<String, PaletAction> hashMap){

		return new HashSet<>( hashMap.values());
	}
		
	public static String removeAllNewLines(String message){
		message=message.replaceAll("(\\r\\n)+", "\r\n");
		message=message.replaceAll("(\\n)+", "\n");
		return message;
	}
	
	public static int getBeginIndex(String text, String patternString){

		CharSequence inputStr = text;
		Pattern pattern = Pattern.compile(patternString);
	    Matcher matcher = pattern.matcher(inputStr);
	    int beginIndex=-1;
	    if(matcher.find()){
	    beginIndex=matcher.start();

	    }
	    
	    return beginIndex;
	}
	
	public static int getEndIndex(String text, String patternString){
		CharSequence inputStr = text;
		Pattern pattern = Pattern.compile(patternString);
	    Matcher matcher = pattern.matcher(inputStr);
	    int endIndex=-1;
	    if(matcher.find()){
	    	endIndex=matcher.end();
	    }
	    return endIndex;
	}
	
	public static String getLineOf(String text,int line) throws MyException{
		
		try{
			if(text.contains("\r\n")){
				
				return text.split("\\r\\n")[line-1];
			}else if(text.contains("\n")){
				return text.split("\\n")[line-1];
			}
			else
				return text;
		}catch(Exception e){
			Log.error(MyException.getErrorLocation("ParsingTools", "getLineOf","wrong "+ text, e));
			throw new MyException(MyException.getErrorLocation("ParsingTools", "getLineOf", "wrong "+text, e),"");
		
		}
	}

	public static String getFirstLineOf(String text) throws MyException{
		try{
			if(text.contains("\r\n"))
			return text.substring(0,text.indexOf("\r\n"));
			else if(text.contains("\n")){
				return text.substring(0,text.indexOf("\n"));
			}
			else
				return text;
		}catch(Exception e){
			Log.error(MyException.getErrorLocation("ParsingTools", "getFirstLineOf","wrong "+ text, e));
			throw new MyException(MyException.getErrorLocation("ParsingTools", "getFirstLineOf", "wrong "+text, e),"");
		
		}
	}
	
	public static String removeFirstLineOf(String text) throws MyException{
		try{
			if(text.contains("\r\n"))
				return text.substring(text.indexOf("\r\n")).trim();
			else if(text.contains("\n")){
				return text.substring(text.indexOf("\n")).trim();
			}
				else
					return "";
		}catch (Exception e) {
			Log.error(MyException.getErrorLocation("ParsingTools", "removeFirstLineOf","wrong "+ text, e));
			throw new MyException(MyException.getErrorLocation("ParsingTools", "removeFirstLineOf", "wrong "+text, e),"");}
	}

	public static boolean containsPalet(Set<Palet>paletSet,Palet p){
		for (Palet palet : paletSet) {
			if(palet.getCode().equals(p.getCode()) && palet.getOwner().equals(palet.getOwner()))
				return true;
			
		}
		return false;
		
	}
	
	public static String removeINPalettes(String text){
//		System.out.println("zibil:\n"+text);
		return text.substring(0, text.indexOf(StaticPatterns.getInformat()))+text.substring(text.indexOf(StaticPatterns.getOutformat()));
	}
	
	public static String removeOUTPalettes(String text){
		return	text.substring(0, text.indexOf(StaticPatterns.getOutformat())).trim();
	}

	public static Set<PaletAction> convertMapToSet(Map<String, PaletAction> map){
		return new HashSet(map.values());
 
	}

	public static Message checkMessage(Message message) {
		if (message.getStatus()==null) {
			message.setStatus("parsed");
		}
		return message;
	}
	
	public static String allocateArriving(String flightInfoString){
		if (splitByDot(splitBySlash(flightInfoString)[1]).length==3) {
			return splitByDot(splitBySlash(flightInfoString)[1])[2];
		}else{
			return splitByDot(splitBySlash(flightInfoString)[1])[1];
		}
	}

	public static String allocateFlightCode(String flightInfoString){
		return splitBySlash(flightInfoString)[0].trim();
	}
	
	

	public static int[] getAmountsOfPalettes(String text){
		Pattern pattern=Pattern.compile(StaticPatterns.getScmpalettecountformat());
		Matcher matcher=pattern.matcher(text);
		int[] counts=null;
		if (matcher.find()) {
			String[] splits=text.split(StaticPatterns.getScmpalettecountformat());
			counts=new int[splits.length-1];
			for (int i = 1; i < splits.length; i++) {
				counts[i-1]=Integer.valueOf(splits[i].trim());
				//System.out.println(counts[i]);
			}
		}else{
			System.out.println("scm format yanlisdir!");
		}
		return counts;
	}

	public static void checkPaletActionsSize(Message message) throws MyException{
		if (message.getPaletActions().size()==0) {
			Log.error(MyException.getErrorLocation("ParsingTools", "checkPaletActionsSize", "ZeroPaletActionException"));
			throw new MyException(MyException.getErrorLocation("ParsingTools", "checkPaletActionsSize", "ZeroPaletActionException"),"");
		}
	}

	public static String removeAllNills(String messageText){
	//	System.out.println("1.:"+messageText);
		Pattern pattern=Pattern.compile(StaticPatterns.getNillformat());
		Matcher matcher=pattern.matcher(messageText);
		if (matcher.find()) {
			pattern=Pattern.compile(StaticPatterns.getInnillformat());
			matcher=pattern.matcher(messageText);
			messageText=matcher.replaceAll("").trim();	
			
			pattern=Pattern.compile(StaticPatterns.getOutnillformat());
			matcher=pattern.matcher(messageText);
			messageText=matcher.replaceAll("").trim();
		}
		//System.out.println("2.:"+messageText);
		
		return messageText.trim();

	}

	public static String smoothMessage(String mailMessage) throws MyException{


		mailMessage=ParsingTools.removeAllNewLines(mailMessage);
		int beginIndex;
		int endIndex;

		
		beginIndex=getBeginIndex(mailMessage);
		endIndex=getEndIndex(mailMessage);
		if(beginIndex==-1 || endIndex==-1){
			Log.error(MyException.getErrorLocation("ParsingTools", "smoothMessage", "Wrong begining/ending"));
			throw new MyException(MyException.getErrorLocation("ParsingTools", "smoothMessage", "SCM begining:"+beginIndex+" ending:"+endIndex),"Wrong begining/ending");
		
		}
	//	System.out.println("beginindex:"+beginIndex+" ****** endIndex:"+endIndex);
		//System.out.println("********\n"+mailMessage.substring(beginIndex,endIndex));
		return mailMessage.substring(beginIndex,endIndex).trim();
		
	}

	public static int getEndIndex(String mailMessage){
		int endIndex=1;
		
		if (mailMessage.contains("UCM")) {
			 endIndex=ParsingTools.getEndIndex(mailMessage, StaticPatterns.getUcmoutlastpaletteformat());
			if (endIndex!=-1) {
				return endIndex;
			}
			endIndex=ParsingTools.getEndIndex(mailMessage, StaticPatterns.getUcminlastpaletteformat());
			if (endIndex!=-1) {
				return endIndex;
			}
			endIndex=ParsingTools.getEndIndex(mailMessage, StaticPatterns.getOutnillformat());
			if (endIndex!=-1) {
				return endIndex;
			}
			endIndex=ParsingTools.getEndIndex(mailMessage, StaticPatterns.getInnillformat());
			if (endIndex!=-1) {
				return endIndex;
			}
			
		}else if(mailMessage.contains("SCM")) {
			endIndex=ParsingTools.getEndIndex(mailMessage, StaticPatterns.getScmlastpaletteformat());
			if (endIndex!=-1) {
				return endIndex;
			}
			
		}else if(mailMessage.contains("LUC")) {

			endIndex=ParsingTools.getEndIndex(mailMessage, StaticPatterns.getLuclastpaletteformat());
			if (endIndex!=-1) {
				return endIndex;
			}
		}
		
		
		



		return endIndex;
		
	}

	public static int getBeginIndex(String mailMessage) {
		
		//System.out.println("gelen msj:"+mailMessage);
		
		int beginIndex=-1;
		beginIndex=getBeginIndex(mailMessage, StaticPatterns.getUcmheadingformat());
		if (beginIndex!=-1) {
			return beginIndex;
		}
		beginIndex=getBeginIndex(mailMessage, StaticPatterns.getScmheadigformat());
		if (beginIndex!=-1) {
			return beginIndex;
		}
		beginIndex=getBeginIndex(mailMessage, StaticPatterns.getLucheadingformat());
		if (beginIndex!=-1) {
			return beginIndex;
		}
		return beginIndex;		
	}

	public static Message fillManualMessage(Message message){
		message.setFromEmail("Manually");
		message.setSender("Manually");
		message.setSubject("Manually");
		message.setReceiveDate(Calendar.getInstance().getTime());
		
		return message;
	}

	public static Palet parseSinglePalet(String paletString) throws MyException{
		Palet palet=new Palet();
	
		//if (containingOnlyLetters(paletPart)) {
			palet.setType(paletString.substring(0,3));
/*		}else{
			Log.error(MyException.getErrorLocation("ParsingTools", "parseSinglePalet", "Wrong PALETTE TYPE "+paletPart));
			throw new MyException(MyException.getErrorLocation("ParsingTools", "parseSinglePalet", "Wrong PALETTE TYPE "+paletPart),"Wrong PALETTE TYPE "+paletPart);				
		}*/
			
			
			String codePlusOwner=paletString.substring(3);
			System.out.println("HEY!\n"+codePlusOwner);
			int index=ParsingTools.allocateCodePlusOwner(codePlusOwner);
			System.out.println(codePlusOwner.substring(0, index)+"---"+codePlusOwner.substring(index));
			
			//PALETTE CODE
			palet.setCode(codePlusOwner.substring(0, index));
			//PALETTE OWNER
			palet.setOwner(codePlusOwner.substring(index));
			
				
			
/*		paletPart=paletString.substring(3,8);
		if (containingOnlyDigits(paletPart)) {
			palet.setCode(paletPart);
		}else{
			Log.error(MyException.getErrorLocation("ParsingTools", "parseSinglePalet", "Wrong PALETTE CODE "+paletPart));
			throw new MyException(MyException.getErrorLocation("ParsingTools", "parseSinglePalet", "Wrong PALETTE CODE "+paletPart),"Wrong PALETTE CODE "+paletPart);				
	
		}
		paletPart=paletString.substring(paletString.length()-2);
		if (containingOnlyLetters(paletPart)) {
			
		
		palet.setOwner(paletPart);
		
		}else{
			Log.error(MyException.getErrorLocation("ParsingTools", "parseSinglePalet", "Wrong PALETTE OWNER "+paletPart));
			throw new MyException(MyException.getErrorLocation("ParsingTools", "parseSinglePalet", "Wrong PALETTE OWNER "+paletPart),"Wrong PALETTE OWNER "+paletPart);				
	
		}*/
		return palet;
	}

	public static Palet getPaletFromMessage(String message, String paletString) throws MyException{
		String arriving=allocateArriving(getLineOf(message, 2));
		
		String messageType=getLineOf(message, 3).substring(0,3);

		Palet palet=parseSinglePalet(paletString);

		if (messageType.equals("IN")) {
			palet.setPosition(arriving);
		}else if(messageType.equals("OUT")){
			palet.setPosition(arriving+"->"+getDepart(splitBySlash(subFromStringUntil(4, message, paletString)), allocateFlightCode(getLineOf(message, 2)), arriving));			
		}

		return palet;
	}
	
	private static String subFromStringUntil(int count, String wholeText, String smallText){
		return wholeText.substring(wholeText.indexOf(smallText), wholeText.indexOf(smallText)+smallText.length()+count);

	}

	public static int allocateCodePlusOwner(String codePlusOwner) throws MyException {
		if (!containingOnlyDigits(codePlusOwner.substring(0, 3))) {
			Log.error(MyException.getErrorLocation("ParsingTools", "allocateCodeAndOwner", "Wrong PALETTE CODE "+codePlusOwner));
			throw new MyException(MyException.getErrorLocation("ParsingTools", "allocateCodeAndOwner", "Wrong PALETTE CODE "+codePlusOwner),"Wrong PALETTE Code "+codePlusOwner);				
		}
		
		for (int i = 4; i <= 5; i++) {
			if (!containingOnlyDigits(codePlusOwner.substring(0, i))) {
				return i-1;
			}
		}
		return 5;
	}
	
}
