package net.myapp.dao.form.methods;

import java.util.List;

import net.myapp.dao.form.models.Airport;

public interface AirportDAO_ADT {

	public List<Airport> getAirports();
}
