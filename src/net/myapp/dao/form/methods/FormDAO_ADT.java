
package net.myapp.dao.form.methods;

import java.util.List;

public interface FormDAO_ADT {

	
	public List<String> getOwners();
	public List<String> getTypes();
	public List<String> getSortedTypes();
	public void fillFormList();
	
}
