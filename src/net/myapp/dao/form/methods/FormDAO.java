
package net.myapp.dao.form.methods;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.springframework.transaction.annotation.Transactional;

import net.myapp.dao.form.models.ULD_owner;
import net.myapp.dao.form.models.ULD_type;
import net.myapp.dao.models.Palet;
import net.myapp.utils.StaticVariables;

 class FormDAO implements FormDAO_ADT {

	

	private SessionFactory sessionFactory;
	  
	public void setSessionFactory(SessionFactory sf)
	  {
	    sessionFactory = sf;
	  }
	
	
	@Override
	@Transactional
	public List<String> getOwners() {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  
		  
		  Criteria criteria=session.createCriteria(ULD_owner.class);
		  criteria.setProjection(Projections.distinct(Projections.property("code")));
		  
		  
		  return criteria.list();
		
	
	}


	@Override
	@Transactional
	public List<String> getTypes() {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  Criteria criteria=session.createCriteria(ULD_type.class);
		  criteria.setProjection(Projections.distinct(Projections.property("name")));
		  

		
		return criteria.list();
	}


	@Override
	@Transactional
	public List<String> getSortedTypes() {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  
		  Criteria criteria1=session.createCriteria(ULD_type.class);
		  criteria1.setProjection(Projections.distinct(Projections.property("name")));

		  
		  Criteria criteria2=session.createCriteria(Palet.class);
		  criteria2.setProjection(Projections.distinct(Projections.property("type")));
		  //criteria2.setProjection(Projections.countDistinct("type"));
		  criteria2.addOrder(Order.desc(Projections.countDistinct("type").getPropertyName()));

		  List<String>types=criteria1.list();
		  List<String>palet_types=criteria2.list();

		  for (String type : palet_types) {
			types.remove(type);
		  }
		  
		  palet_types.addAll(types);
		  
		 
		  return palet_types;
	}


	@Override
	@Transactional
	public void fillFormList() {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();

		  
		  
		  Criteria criteria1=session.createCriteria(ULD_type.class);
		  criteria1.setProjection(Projections.distinct(Projections.property("name")));

		  
		  Criteria criteria2=session.createCriteria(Palet.class);
		  criteria2.setProjection(Projections.distinct(Projections.property("type")));
		  //criteria2.setProjection(Projections.countDistinct("type"));
		  criteria2.addOrder(Order.desc(Projections.countDistinct("type").getPropertyName()));

		  List<String>types=criteria1.list();
		  List<String>palet_types=criteria2.list();

		  for (String type : palet_types) {
			types.remove(type);
		  }
		  
		  palet_types.addAll(types);
		  StaticVariables.setPalet_types(palet_types);
		  
		  
		  
		  
		  Criteria criteriaOwner=session.createCriteria(ULD_owner.class);
		  criteriaOwner.setProjection(Projections.distinct(Projections.property("code")));
		  
		  StaticVariables.setPalet_owners(criteriaOwner.list());
	}
	
	
	
	

}
