package net.myapp.dao.form.methods;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.Transformers;
import org.springframework.transaction.annotation.Transactional;

import net.myapp.dao.form.models.Airport;
import net.myapp.dao.models.Message;

class AirportDAO implements AirportDAO_ADT{

	private SessionFactory sessionFactory;
	  
	public void setSessionFactory(SessionFactory sf)
	  {
	    sessionFactory = sf;
	  }
	
	@Override
	@Transactional
	public List<Airport> getAirports() {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  Criteria criteria=session.createCriteria(Airport.class);
		  ProjectionList projectionList=Projections.projectionList();
		  projectionList.add(Projections.property("iata").as("iata"));
		  projectionList.add(Projections.property("name").as("name"));
		  projectionList.add(Projections.property("country").as("country"));
		  criteria.setProjection(projectionList);
		  criteria.setResultTransformer(Transformers.aliasToBean(Airport.class));

		  

		  //criteria.setMaxResults(500);
	return criteria.list();
	}

}
