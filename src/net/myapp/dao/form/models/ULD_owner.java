package net.myapp.dao.form.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="uld_owner")
public class ULD_owner {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "airline_ID")
	private int ID;
	
	@Column(name="name",length = 50,nullable=false)
	private String name;
	
	
	@Column(name="code",length = 2,nullable=false)
	private String code;


	public ULD_owner() {
		super();
	}


	public ULD_owner(String name, String code) {
		super();
		this.name = name;
		this.code = code;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public int getID() {
		return ID;
	}
	
	
	
	
	
}
