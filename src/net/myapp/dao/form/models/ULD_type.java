package net.myapp.dao.form.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="uld_type")
public class ULD_type {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "type_ID")
	private int ID;
	
	@Column(name="name",length = 3,nullable=false)
	private String name;

	public ULD_type() {
		super();
	}

	public ULD_type(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getID() {
		return ID;
	}
	
	
	
	
}
