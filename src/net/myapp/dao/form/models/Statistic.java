package net.myapp.dao.form.models;

public class Statistic {
	private int value;
	private Colors color;
	private String label;
	
	public enum Colors {
		Red("#FF6F69"),
		Yellow("#FDF206"),
		Grey("#808080"),
		Pink("#A366A3"),
		DarkBlue("#03396C"),
		Blue("#C6E2FF"),
		Green("#15BA67");
		
		String value;
		private Colors(String value){
			this.value=value;
		}
	}
	
	
	public Statistic(int value, Colors colors, String label) {
		super();
		this.value = value;
		this.color = colors;
		this.label = label;
	}
	public Statistic() {
		super();
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public Colors getColor() {
		return color;
	}
	public void setColor(Colors color) {
		this.color = color;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public static Colors getColor(int i){
		i=i%Colors.values().length;
		return Colors.values()[i];
	}
	
	
	

}

