package net.myapp.dao.form.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="airport")
public class Airport {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "airport_ID")
	private int ID;
	
	@Column(name="name",length = 50,nullable=false)
	private String name;
	
	@Column(name="icao",length = 4,nullable=true)
	private String icao;
	
	@Column(name="iata",length = 3,nullable=true)
	private String iata;
	
	@Column(name="code",length = 2,nullable=false)
	private String code;
	
	@Column(name="country",length = 50,nullable=false)
	private String country;

	public Airport() {
		super();
	}

	public Airport(String name, String icao, String iata, String code, String country) {
		super();
		this.name = name;
		this.icao = icao;
		this.iata = iata;
		this.code = code;
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcao() {
		return icao;
	}

	public void setIcao(String icao) {
		this.icao = icao;
	}

	public String getIata() {
		return iata;
	}

	public void setIata(String iata) {
		this.iata = iata;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getID() {
		return ID;
	}
	
	
	





}
