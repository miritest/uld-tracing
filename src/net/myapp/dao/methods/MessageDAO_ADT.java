package net.myapp.dao.methods;

import java.text.ParseException;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.hibernate.Session;

import net.myapp.dao.models.ActionError;
import net.myapp.dao.models.Message;
import net.myapp.dao.models.Palet;
import net.myapp.dao.models.PaletAction;
import net.myapp.dao.required.columns.MessageColumns;
import net.myapp.exceptions.MyException;

public interface MessageDAO_ADT {

	public void insert(Message message);
	public void update(Message message);
	public void delete(Message message);
	public Message getMessagesByID(int id);
	public void insertParsedUCM_OUT(Message message) throws MyException;
	public void insertParsedUCM_IN(Message message) throws MyException;
	public void insertParsedSCM(Message message) throws MyException;
	public List getMessagesByCriteria(Message message,MessageColumns columns,int page);
	
	/**
	This method creates new palet and solves all 'Not Found Error' about this palet. 
	 * @throws MyException 
	@param palet a palet which should be created
	
	@return true or false by depending on completion process successfully or unsuccessfully
	*/
	public boolean solveErrorByCreatingPalet(Palet palet,Palet oldPalet) throws MyException;
	

	/**
	This method connects palet to another palet which ID is paletID. 
	 * @throws MyException 
	@param palet a palet which should be connected
	@param paletID an ID of palet which exists in DB.

	@return true or false by depending on completion process successfully or unsuccessfully
	*/
	
	
	public boolean solveErrorByConnectingPalet(Palet palet,Palet connectedPalet) throws MyException;

	public boolean ignoreError(int errorID);
}
