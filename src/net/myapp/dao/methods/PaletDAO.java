package net.myapp.dao.methods;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.Transformers;
import org.springframework.transaction.annotation.Transactional;

import net.myapp.dao.models.Message;
import net.myapp.dao.models.ActionError;
import net.myapp.dao.models.Palet;
import net.myapp.dao.models.PaletAction;
import net.myapp.dao.required.columns.PaletColumns;
import net.myapp.exceptions.MyException;
import net.myapp.utils.Utils;


class PaletDAO implements PaletDAO_ADT {
	
	 private SessionFactory sessionFactory;
	  
		public void setSessionFactory(SessionFactory sf)
		  {
		    sessionFactory = sf;
		  }
		
	@Override
	@Transactional
	public void update(Palet palet) {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  session.update(palet);
	}
	
	
	@Override
	@Transactional
	public void insert(Palet palet) {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();

		  session.save(palet);
	}
	
	

	@Override
	@Transactional
	public void delete(Palet palet) {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  session.delete(palet);
	}
	@Override
	@Transactional
	public List<Palet> getPaletsByCriteria(String format, PaletColumns columns) {
		// TODO Auto-generated method stub
		StringBuilder hql=new StringBuilder("Select ID as ID ");
		 if(columns!=null){
			
		
			  if(columns.isType())
			  {   
				  hql.append(",type as type ");
			  }
			  if(columns.isCode())
			  {
				  hql.append(",code as code ");
			  }
			  if(columns.isOwner())
			  {
				  hql.append(",owner as owner ");
			  }
			  if(columns.isChecked())
			  {
				  hql.append(",checked ");
			  }
			  if(columns.isActive())
			  {
				  hql.append(", active ");
			  }
			  if(columns.isReason())
			  {
				  hql.append(", reason ");
			  }
			  if(columns.isInsert_Date())
			  {
				  hql.append(",insert_Date ");
			  }
			  if(columns.isPosition())
			  {
				  hql.append(",position ");
			  }

			  
			  }
			  
		     hql.append(" from Palet where concat(type,code,owner) like '%"+format+"%'");
		     
		     Query query=sessionFactory.getCurrentSession().createQuery(hql.toString());
			 query.setResultTransformer(Transformers.aliasToBean(Palet.class));

		return query.list();
	}

	
	@Override
	@Transactional
	public List<Palet> getPaletsByCriteria(Palet palet,PaletColumns columns, int page)   {
		// TODO Auto-generated method stub
		
		
		  Session session = sessionFactory.getCurrentSession();
		  Criteria criteria=session.createCriteria(Palet.class);
		  

		  if(palet!=null){
		  
		  if(!Utils.isNull(palet.getID()))
		  {
			  criteria.add(Restrictions.eq("ID",palet.getID()));
		  }
		  
		  if(!Utils.isNull(palet.getType()))
		  {
			  criteria.add(Restrictions.eq("type",palet.getType()));
		   }
		   
		  if(!Utils.isNull(palet.getCode()))
		  {
			  criteria.add(Restrictions.like("code", palet.getCode(), MatchMode.ANYWHERE));
		  }
		  
		  if(!Utils.isNull(palet.getOwner()))
		   {
			  criteria.add(Restrictions.eq("owner",palet.getOwner()));
		   }
		 // criteria.add(Restrictions.eq("checked",palet.isChecked()));
		 // criteria.add(Restrictions.eq("active",palet.isActive()));

		  if(!Utils.isNull(palet.getReason()))
		   {
			  criteria.add(Restrictions.eq("reason",palet.getReason()));
		   }
		  
		  if(!Utils.isNull(palet.getPosition())){
			  criteria.add(Restrictions.eq("position",palet.getPosition()));
		  }
		  
		
		  
		  }
		  
		  
		  if(columns!=null){
		  ProjectionList projectionList=Projections.projectionList();
		
		  if(columns.isID())
		  {
			  projectionList.add(Projections.property("ID").as("ID"));

		  }
		  if(columns.isType())
		  {   
			  projectionList.add(Projections.property("type").as("type"));

		  }
		  if(columns.isCode())
		  {
			  projectionList.add(Projections.property("code").as("code"));

		  }
		  if(columns.isOwner())
		  {
			  projectionList.add(Projections.property("owner").as("owner"));

		  }
		  if(columns.isChecked())
		  {
			  projectionList.add(Projections.property("checked").as("checked"));

		  }
		  if(columns.isActive())
		  {
			  projectionList.add(Projections.property("active").as("active"));

		  }
		  if(columns.isReason())
		  {
			  projectionList.add(Projections.property("reason").as("reason"));

		  }
		  if(columns.isInsert_Date())
		  {
			  projectionList.add(Projections.property("insert_date").as("insert_date"));

		  }
		  if(columns.isPosition())
		  {
			  projectionList.add(Projections.property("position").as("position"));

		  }

		  criteria.setProjection(projectionList);
		  
		  }
		  
		  if(page>0){
		  criteria.setFirstResult((page-1)*10);
		  criteria.setMaxResults(10);
		  }
		  
		  
		  criteria.setResultTransformer(Transformers.aliasToBean(Palet.class));

		 return (List<Palet>)criteria.list();
		  
	}
	
	

	@Override
	@Transactional
	public void DeleteALL() {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  Query query1=session.createQuery("DELETE from PaletAction");
		//  Query query2=session.createQuery("DELETE from Palet");
		  Query query3=session.createQuery("DELETE from Message");


		query1.executeUpdate();
		//query2.executeUpdate();
		query3.executeUpdate();

		
	}

	@Override
	@Transactional
	public List<Palet> getPaletDetailedInfo(Palet palet) {
		// TODO Auto-generated method stub
		 Session session = sessionFactory.getCurrentSession();
		 String hqlQuery="Select p.type,p.code,p.owner,p.position,p.active,p.insert_date,"
			  		+ "m.ID,m.subject,m.receiveDate,m.fromEmail,m.type,m.sender,m.status,"
			  		+ "a.carrier,a.flightNo,a.flightDate,a.arriving,a.departing "
			  		+ "from Palet as p "
			  		+ "LEFT JOIN  p.paletActions as a "
			  		+ "LEFT JOIN  a.message as m "
			  		+ "where 1=1 ";
		/* Query query=session.createQuery
				  ("Select p.type,p.code,p.owner,p.position,p.active,p.insert_date,"
				  		+ "m.ID,m.subject,m.receiveDate,m.fromEmail,m.type,m.sender,m.status,"
				  		+ "a.carrier,a.flightNo,a.flightDate,a.arriving,a.departing "
				  		+ "from Palet as p "
				  		+ "LEFT JOIN  p.paletActions as a "
				  		+ "LEFT JOIN  a.message as m "
				  		+ "where p.ID=:id "
				  		);*/
		  
		  //burda
		 
		 Criteria criteria = session.createCriteria(Palet.class, "p");
		 criteria.createAlias("p.paletActions", "paletActions",JoinType.LEFT_OUTER_JOIN);
		 criteria.createAlias("paletActions.message", "message",JoinType.LEFT_OUTER_JOIN);


		
		 if(palet!=null){
			  
			  if(!Utils.isNull(palet.getID()))
			  {
				  criteria.add(Restrictions.eq("p.ID",palet.getID()));
			  }
			  
			  if(!Utils.isNull(palet.getType()))
			  {
				  criteria.add(Restrictions.eq("p.type",palet.getType()));
			   }
			   
			  if(!Utils.isNull(palet.getCode()))
			  {
				  criteria.add(Restrictions.like("p.code", palet.getCode(), MatchMode.ANYWHERE));
			  }
			  
			  if(!Utils.isNull(palet.getOwner()))
			   {
				  criteria.add(Restrictions.eq("p.owner",palet.getOwner()));
			   }
			 // criteria.add(Restrictions.eq("checked",palet.isChecked()));
			 // criteria.add(Restrictions.eq("active",palet.isActive()));

			  if(!Utils.isNull(palet.getReason()))
			   {
				  criteria.add(Restrictions.eq("p.reason",palet.getReason()));
			   }
			  
			  if(!Utils.isNull(palet.getPosition())){
				  criteria.add(Restrictions.eq("p.position",palet.getPosition()));
			  }
			  
			
			  
			  }
			  
		  
		  //silme bu commentleri pis gunde lazim olar )
		/* List<Object[]>resultList=(List<Object[]>)query.list();
			 
		  
		  
		  Palet palet=new Palet();
		  palet.setType((String)resultList.get(0)[0]);
		  palet.setCode((String)resultList.get(0)[1]);
		  palet.setOwner((String)resultList.get(0)[2]);
		  palet.setPosition((String)resultList.get(0)[3]);
		  palet.setActive((boolean)resultList.get(0)[4]);
		  palet.setInsert_date((Date)resultList.get(0)[5]);
		  
		  Set<PaletAction>paletActions=new HashSet<PaletAction>();
		  
			 for (Object[] result : resultList) {
				 Message message=new Message();
				 message.setID((int)result[6]);
				 message.setSubject((String)result[7]);
				 message.setReceiveDate((Date)result[8]);
				 message.setFromEmail((String)result[9]);
				 message.setType((String)result[10]);
				 message.setSender((String)result[11]);
				 message.setStatus((String)result[12]);
				 
				 PaletAction paletAction=new PaletAction();
				 paletAction.setCarrier((String)result[13]);
				 paletAction.setFlightNo((String)result[14]);
				 paletAction.setFlightDate((Date)result[15]);
				 paletAction.setArriving((String)result[16]);
				 paletAction.setDeparting((String)result[17]);
				 
				 paletAction.setMessage(message);
				 paletActions.add(paletAction);
				    

				    
				}
		
			 
			*/
		  criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		return   criteria.list();
	}

	
	
	

}
