package net.myapp.dao.methods;


import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.Transformers;

import com.mysql.fabric.xmlrpc.base.Param;

import net.myapp.dao.models.Message;
import net.myapp.dao.models.ActionError;
import net.myapp.dao.models.Palet;
import net.myapp.dao.models.PaletAction;
import net.myapp.dao.required.columns.MessageColumns;
import net.myapp.exceptions.MyException;
import net.myapp.utils.Parse;
import net.myapp.utils.Utils;

 class MessageDAO implements MessageDAO_ADT {

	private SessionFactory sessionFactory;
	  
	public void setSessionFactory(SessionFactory sf)
	  {
	    sessionFactory = sf;
	  }

	
	
	@Override
	@Transactional
	public void insert(Message message) {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  session.save(message);
		
	}

	@Override
	@Transactional
	public void update(Message message) {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  session.update(message);
		
	}

	@Override
	@Transactional
	public void delete(Message message) {
		// TODO Auto-generated metihod stub
		  Session session = sessionFactory.getCurrentSession();
		  session.delete(message);
		
	}

	@Override
	@Transactional
	public List<Message> getMessagesByCriteria(Message message,MessageColumns columns,int page) {
		
		  Session session = sessionFactory.getCurrentSession();
		  Criteria criteria = session.createCriteria(Message.class);
		 
		 if(message!=null){
		
			 if(!Utils.isNull(message.getID()))
			 {
				 criteria.add(Restrictions.eq("ID", message.getID()));
			 }
			 
			 if(!Utils.isNull(message.getFromEmail()))
			 {
				 criteria.add(Restrictions.eq("fromEmail",message.getFromEmail()));
				 
			 }
			 
			 if(!Utils.isNull(message.getText()))
			 {
				 criteria.add(Restrictions.eq("text", message.getText()));
			 }
			 
			 if(!Utils.isNull(message.getType()))
			 {
				 criteria.add(Restrictions.eq("type", message.getType()));
			 }
			 
			 if(!Utils.isNull(message.getReceiveDate()))
			 {
				 criteria.add(Restrictions.eq("receiveDate",message.getReceiveDate()));
			 }
			 
			 if(!Utils.isNull(message.getStatus()))
			 {
				 criteria.add(Restrictions.eq("status", message.getStatus()));
			 }
			 
			 if(!Utils.isNull(message.getReason()))
			 {
				 criteria.add(Restrictions.eq("reason",message.getReason()));
			 }
			 
			 if(!Utils.isNull(message.getReason_user()))
			 {
				 criteria.add(Restrictions.eq("reason_user", message.getReason_user()));
			 }
			 
			 if(!Utils.isNull(message.getInsertDate()))
			 {
				 criteria.add(Restrictions.eq("insertDate",message.getInsertDate()));
			 }
			 
			 if(!Utils.isNull(message.getSender()))
			 {
				 criteria.add(Restrictions.eq("sender", message.getSender()));
			 }
			 
			 if(!Utils.isNull(message.getSubject()))
			 {
				 criteria.add(Restrictions.eq("subject",message.getSubject()));
			 }
			 
		 }
		  
		  if(columns!=null){
			  ProjectionList projectionList=Projections.projectionList();
			  
			  if(columns.isID())
			  {
				  projectionList.add(Projections.property("ID").as("ID"));

			  }
			  
			  if(columns.isType())
			  {
				  projectionList.add(Projections.property("type").as("type"));

			  }
				
			  if(columns.isText())
			  {
				  projectionList.add(Projections.property("text").as("text"));

			  }  
			  
			  if(columns.isReceiveDate())
			  {
				  projectionList.add(Projections.property("receiveDate").as("receiveDate"));

			  }
			  
			  if(columns.isStatus())
			  {
				  projectionList.add(Projections.property("status").as("status"));

			  }				
			  
			  if(columns.isReason())
			  {
				  projectionList.add(Projections.property("reason").as("reason"));

			  }
			  
			  if(columns.isReason_user())
			  {
				  projectionList.add(Projections.property("reason_user").as("reason_user"));

			  }
			  
			  if(columns.isInsert_date())
			  {
				  projectionList.add(Projections.property("insertDate").as("insertDate"));

			  }
			  
			  if(columns.isSender())
			  {
				  projectionList.add(Projections.property("sender").as("sender"));

			  }

			  if(columns.isSubject())
			  {
				  projectionList.add(Projections.property("subject").as("subject"));

			  }
			  
			  if(columns.isStatus())
			  {
				  projectionList.add(Projections.property("fromEmail").as("fromEmail"));

			  }
			  criteria.setProjection(projectionList);

		  }		
			  
		  if(page>0){
			  criteria.setFirstResult((page-1)*50);
			  criteria.setMaxResults(50);
			  }
			  
			  
		  criteria.setResultTransformer(Transformers.aliasToBean(Message.class));

		
		  return criteria.list();		
	}
	
	
	@Override
	@Transactional
	public Message getMessagesByID(int id) {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  Query query=session.createQuery
				  ("from Message as m "
				  		+ "FETCH ALL PROPERTIES "
				  		+ "LEFT JOIN FETCH m.paletActions as act "
				  		+ "LEFT JOIN FETCH act.actionErrorSet as error "
				  		+ "where m.ID=:id");
		  
		  
		  
		  if(!Utils.isNull(id))
		  {
			  query.setParameter("id",id);
		  }
		 

		  return (Message) query.uniqueResult();
		
	}




	
	@Override
	@Transactional
	public void insertParsedUCM_OUT(Message message) throws MyException {
		// TODO Auto-generated method stub
		if(message.getPaletActions().size()==0) 
			throw new MyException("Null PaletAction","");
		else
		{
			Session session = sessionFactory.getCurrentSession();
			
			boolean warning=false;
			
			for (PaletAction paletAction : message.getPaletActions()) {
			  for (Iterator<Palet> iterator = paletAction.getPaletSet().iterator(); iterator.hasNext();) {
				  Palet palet = (Palet) iterator.next();
				  Criteria criteria=session.createCriteria(Palet.class);
				  criteria.add(Restrictions.eq("code",palet.getCode()));
				  criteria.add(Restrictions.eq("owner", palet.getOwner()));
				 
				   Palet p=(Palet) criteria.uniqueResult();
				  //palet yoxlanilir
			 	  if(p==null)
			 	  { 
			 		 iterator.remove();
			 		 ActionError actionError=new ActionError(paletAction, palet.getType()+palet.getCode()+palet.getOwner(),"OUT", paletAction.getFlightNo(), "-", "ULD not found");
			 		 paletAction.addError(actionError);
			 		 warning=true;

			 	  }
			 	  else 
			 	  {
			 		  palet.setID(p.getID());
			 		  session.merge(palet);
			 	  }
			}
			paletAction.setMessage(message);
			
		}
			if(warning)
			message.setStatus("warning");

		session.save(message);

	}

	}

	@Override
	@Transactional
	public void insertParsedUCM_IN(Message message) throws MyException{
		// TODO Auto-generated method stub
		if(message.getPaletActions().size()==0)
			throw new MyException("Null PaletAction","");
			
		else
		{
			Session session = sessionFactory.getCurrentSession();
			boolean warning=false;
			
			//IN de yalniz 1 paletAction olur,sadece mesaj paletAction-siz geler bile deye for un icindedi
			// bunun yerine paletAction.get(0) yaz islet
		 //mesajin OUT qarsiligini tapmaga calis
		 PaletAction paletAction=message.getPaletActions().iterator().next();
				
		 
		 Query query=session.createQuery
					  ("select m.text from Message as m "
					  		+ "JOIN  m.paletActions as action"
					  		+ " where action.flightNo=:flightNo AND action.carrier=:carrier");
			query.setMaxResults(1);
			query.setParameter("flightNo", paletAction.getFlightNo());
			query.setParameter("carrier",paletAction.getCarrier());
		  
		  
		  String matchedMessage=(String) query.uniqueResult();
		  //eger tapildisa,baza ile yoxla palet bazada var ya yox

		  if(Utils.isNull(matchedMessage))
		  {
			  message.setPaletActions(null);
			  message.setStatus("warning");
			  message.setReason("Matching Error-OUT not found");
			  message.setReason_user("Matching Error-OUT not found");
			  session.save(message);
		  }
		  else {
		  Set<Palet>matchedPalets=Parse.parse(matchedMessage);
		  
		  for (Iterator<Palet> iterator = paletAction.getPaletSet().iterator(); iterator.hasNext();) {
				Palet palet = (Palet) iterator.next();
				//palet yoxlanilir bazada var ya yox
				Criteria criteria=session.createCriteria(Palet.class);
				  criteria.add(Restrictions.eq("code",palet.getCode()));
				  criteria.add(Restrictions.eq("owner", palet.getOwner()));
				 
			
				   Palet p=(Palet) criteria.uniqueResult();
				   //yoxdusa
			 	  if(p==null)
			 	  { 
			 		 iterator.remove();
			 		 ActionError messageError=new ActionError(paletAction, palet.getType()+palet.getCode()+palet.getOwner(),"IN", paletAction.getFlightNo(), "-", "ULD not found");
			 		 paletAction.addError(messageError);
			 		 warning=true;
			 	  }
			 	  //varsa
			 	  else 
			 	  {
			 		  //OUT MESAJININ paletleri ile yoxla ki orda var ya yox
			 		  
					 //varsa
			 		  boolean var=false;
			 		  for (Iterator<Palet> iterator2 = matchedPalets.iterator(); iterator2.hasNext();) {
						Palet matchedpalet = (Palet) iterator2.next();
				
			 		  if(matchedpalet.getCode().equals(palet.getCode()) && matchedpalet.getOwner().equals(palet.getOwner()))
			 		  //if(matchedMessage.contains((palet.getCode()+palet.getOwner())))
			 		  {   var=true;
			 		      iterator2.remove();
			 			  palet.setID(p.getID());
			 			  session.merge(palet);
			 			  break;
			 		  }
			 		  }
			 		  //yoxdusa
			 		  if(!var) 
			 		  {
				 		 iterator.remove();
				 		 ActionError messageError=new ActionError(paletAction, palet.getType()+palet.getCode()+palet.getOwner(),"IN", paletAction.getFlightNo(), "-", "ULD not matched with OUT message");
				 		 paletAction.addError(messageError);
				 		 warning=true;
			 		  }
			 		  
			 	  }

			}
		 
			paletAction.setMessage(message);

		
			if(warning)message.setStatus("warning");
			session.save(message);
		  }
	}

	}
	@Override
	@Transactional
	public void insertParsedSCM(Message message) throws MyException {
		// TODO Auto-generated method stub
		if(message.getPaletActions().size()==0)
			throw new MyException("Null PaletAction", "");
		else
		{
			Session session = sessionFactory.getCurrentSession();
			boolean warning=false;
			//find which palets are in SCM position
			String HQL="Select ID as ID,type as type,code as code,owner as owner,position as position from Palet where position=:position";
			Query findPaletsByPosition=session.createQuery(HQL);
			findPaletsByPosition.setParameter("position", message.getPaletActions().iterator().next().getArriving());
			
			findPaletsByPosition.setResultTransformer(Transformers.aliasToBean(Palet.class));

			List<Palet>paletsFromDB=(List<Palet>)findPaletsByPosition.list();
			//-------------------
			
			Set<Palet>paletSet=new HashSet<>();
			//check positions of palets with List which are came from DB
			PaletAction paletAction=message.getPaletActions().iterator().next();
			  for (Iterator<Palet> iterator = paletAction.getPaletSet().iterator(); iterator.hasNext();) {
				Palet palet = (Palet) iterator.next();
				
				int positionOnList=paletsFromDB.indexOf(palet);
				
				
				//check existence of palet in List which are came from DB
			 	  if(positionOnList==-1) 
			 	  {
			 		 //if not, update palet position on DB
			 		 String hql="Update Palet set position=:position Where code=:code AND owner=:owner";
			 		 Query updatePaletPosition=session.createQuery(hql);
			 		 updatePaletPosition.setParameter("position",message.getPaletActions().iterator().next().getArriving());
			 		 updatePaletPosition.setParameter("code", palet.getCode());
			 		 updatePaletPosition.setParameter("owner",palet.getOwner());
			 		 int result=updatePaletPosition.executeUpdate();
			 		 if(result!=0){paletSet.add(palet);}
			 		 else
			 		 {
			 			 ActionError messageError=new ActionError(paletAction, palet.getType()+palet.getCode()+palet.getOwner(),"SCM", paletAction.getFlightNo(), "-", "ULD not found");
				 		 paletAction.addError(messageError);
				 		 warning=true;
			 		 }
			 		 
			 	  }
			 	  else {
			 		  paletsFromDB.remove(palet);
			 	  }
			}
			  
			  for (Palet p : paletsFromDB) {
			 		 ActionError messageError=new ActionError(paletAction, p.getType()+p.getCode()+p.getOwner(),"SCM", paletAction.getFlightNo(), "-", "ULD should be");
			 		 paletAction.addError(messageError);
			 		 warning=true;
				}
			  //--------------------------------
			paletAction.setPaletSet(paletSet);
			paletAction.setMessage(message);
			 
		if(warning)message.setStatus("warning");
		session.save(message);
	}
  }
	
	
	
	@Override
	@Transactional
	public boolean solveErrorByCreatingPalet(Palet palet,Palet oldPalet) throws MyException {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		
		
		
		Query findPositionQuery=session.createQuery("SELECT palet_action.arriving,palet_action.departing,message.ID FROM PaletAction as palet_action "+ 
				" LEFT OUTER JOIN palet_action.actionErrorSet as error"+ 
				" LEFT OUTER JOIN palet_action.message as message "+ 
				" WHERE error.palet=:palet"+
				" ORDER BY message.receiveDate "+
				" DESC ");
		findPositionQuery.setMaxResults(1);
		
		findPositionQuery.setParameter("palet", oldPalet.getType()+oldPalet.getCode()+oldPalet.getOwner());


		Object[] position=(Object[])findPositionQuery.uniqueResult();
		
		if(!Utils.isNull(position[1]))
		{
			palet.setPosition(position[0]+"->"+position[1]);
		}
		else 
		{
			palet.setPosition((String)position[0]);
		}
	
		int messageID=(int) position[2];
		
		Criteria criteria=session.createCriteria(Palet.class);
		  criteria.add(Restrictions.eq("code",palet.getCode()));
		  criteria.add(Restrictions.eq("owner", palet.getOwner()));
		 
		   Palet p=(Palet) criteria.uniqueResult();
		  
		   if(p!=null){
			throw  new MyException(MyException.getErrorLocation("PaletDAO", "solveErrorByCreatingPalet", "Palet exists in DB,You cannot insert again."), "Palet exists,You cannot create again.");
		   }
		   session.save(palet);
		
		Query findActionIDs=session.createQuery("SELECT act.ID FROM PaletAction as act"+ 
				" LEFT OUTER JOIN act.actionErrorSet as action_error"+
				" WHERE action_error.error='ULD not found' AND action_error.palet=:palet");
		findActionIDs.setParameter("palet", oldPalet.getType()+oldPalet.getCode()+oldPalet.getOwner());

		List<Integer>IDs=findActionIDs.list();
		
		for (Integer integer : IDs) {
			
			
		Query insertPaletActionConnQuery=session.createSQLQuery("INSERT INTO `palet_action_conn`(`palet_ID`, `action_ID`) "
				+ "VALUES ("+palet.getID()+","+integer+")");
		
		insertPaletActionConnQuery.executeUpdate();
		}
		
		
	    Query  updateErrorQuery=session.createQuery("Update ActionError set solved=1,palet=? where palet=? AND error='ULD not found'");
	    updateErrorQuery.setParameter(0, oldPalet.getType()+oldPalet.getCode()+oldPalet.getOwner()+"->"+palet.getType()+palet.getCode()+palet.getOwner());
	    updateErrorQuery.setParameter(1, oldPalet.getType()+oldPalet.getCode()+oldPalet.getOwner());
	   
	    updateErrorQuery.executeUpdate();
	    
	    Query AllErrorsSolvedOrNot=session.createQuery("Select message.ID from Message as message "
	    		+ "LEFT OUTER JOIN message.paletActions as action "
	    		+ "LEFT OUTER JOIN action.actionErrorSet as error "
	    		+ "Where error.solved=0 AND message.ID="+messageID);
	   
	    int result=AllErrorsSolvedOrNot.list().size();
	    System.out.println(result);
	    if(result==0){
	    	Query updateMessageStatus=session.createQuery("UPDATE Message set status='solved',palet=:palet Where ID="+messageID);
	    updateMessageStatus.executeUpdate();
	    }
		return true;
	}



	@Override
	@Transactional
	public boolean solveErrorByConnectingPalet(Palet palet,Palet connectedPalet) throws MyException {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		
		 Criteria criteria=session.createCriteria(Palet.class);
		  criteria.add(Restrictions.eq("code",connectedPalet.getCode()));
		  criteria.add(Restrictions.eq("owner", connectedPalet.getOwner()));
		 
		   Palet p=(Palet) criteria.uniqueResult();
		  
		   if(p==null){
			throw  new MyException(MyException.getErrorLocation("PaletDAO", "solveErrorByConnectingPalet", "Palet exists in DB,You cannot insert again."), "Palet exists,You cannot create again.");
		   }
		
		
		
		Query findActionIDs=session.createQuery("SELECT act.ID FROM PaletAction as act"+ 
				" LEFT OUTER JOIN act.actionErrorSet as action_error"+
				" WHERE action_error.error='ULD not found' AND action_error.palet=:palet AND action_error.solved=0");
		findActionIDs.setParameter("palet", palet.getType()+palet.getCode()+palet.getOwner());

		List<Integer>IDs=findActionIDs.list();
	
		System.out.println(IDs.size());
		
		for (Integer integer : IDs) {
			
		
		Query insertPaletActionConnQuery=session.createSQLQuery("INSERT INTO `palet_action_conn`(`palet_ID`, `action_ID`) "
				+ "VALUES ("+p.getID()+","+integer+")");
		
		insertPaletActionConnQuery.executeUpdate();
		}
		
			Query  updateErrorQuery=session.createQuery("Update ActionError set solved=1,palet=? where palet=? AND error='ULD not found'");
		    updateErrorQuery.setParameter(0, palet.getType()+palet.getCode()+palet.getOwner()+"->"+connectedPalet.getType()+connectedPalet.getCode()+connectedPalet.getOwner());
		    updateErrorQuery.setParameter(1, palet.getType()+palet.getCode()+palet.getOwner());
		    updateErrorQuery.executeUpdate();
		
		
		return true;
	}



	@Override
	@Transactional
	public boolean ignoreError(int errorID) {
		// TODO Auto-generated method stub
		
			Session session=sessionFactory.getCurrentSession();
			session.delete(session.load(ActionError.class, errorID));
		
		return true;
	}

}