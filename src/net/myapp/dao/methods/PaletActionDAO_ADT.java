package net.myapp.dao.methods;

import java.util.List;

import net.myapp.dao.models.Palet;
import net.myapp.dao.models.PaletAction;

public interface PaletActionDAO_ADT {

	public void insert(PaletAction paletAction);
	public void update(PaletAction paletAction);
	public void delete(PaletAction paletAction);
	public List<Palet> getPaletsByCriteria(PaletAction paletAction);
	
}
