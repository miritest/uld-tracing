package net.myapp.dao.methods;

import java.util.List;

import net.myapp.dao.models.Message;
import net.myapp.dao.models.ActionError;
import net.myapp.dao.models.Palet;
import net.myapp.dao.models.PaletAction;
import net.myapp.dao.required.columns.PaletColumns;
import net.myapp.exceptions.MyException;

public interface PaletDAO_ADT {

	public void update(Palet palet);
	public void insert(Palet palet);
	public void delete(Palet palet);
	public List<Palet> getPaletsByCriteria(Palet palet,PaletColumns paletColumns,int page);
	public List<Palet>getPaletsByCriteria(String format,PaletColumns paletColumns);
	public List<Palet> getPaletDetailedInfo(Palet palet);
	public void DeleteALL();
}
