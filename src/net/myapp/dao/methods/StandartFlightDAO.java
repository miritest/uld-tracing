package net.myapp.dao.methods;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.myapp.dao.models.StandartFlight;

public class StandartFlightDAO  implements StandartFlightDAO_ADT{

	 
	 private SessionFactory sessionFactory;
	  
	 
		public void setSessionFactory(SessionFactory sf)
		  {
		    sessionFactory = sf;
		  }
		
	@Override
	@Transactional
	public StandartFlight findByFligth(String code) {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  Criteria criteria=session.createCriteria(StandartFlight.class);
		  criteria.add(Expression.eq("flightCode",code));
		
		  StandartFlight standartFlight=(StandartFlight) criteria.uniqueResult();
		return standartFlight;
	}

}
