package net.myapp.dao.methods;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.springframework.transaction.annotation.Transactional;
import net.myapp.dao.models.PaletAction;

class PaletActionDAO implements PaletActionDAO_ADT {
	private SessionFactory sessionFactory;
	  
	public void setSessionFactory(SessionFactory sf)
	  {
	    sessionFactory = sf;
	  }

	@Override
	@Transactional
	public void insert(PaletAction paletAction) {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  session.save(paletAction);
	}

	@Override
	@Transactional
	public void update(PaletAction paletAction) {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  session.update(paletAction);
	}

	@Override
	@Transactional
	public void delete(PaletAction paletAction) {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  session.delete(paletAction);
		
	}

	@Override
	@Transactional
	public List getPaletsByCriteria(PaletAction paletAction) {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.getCurrentSession();
		  Criteria criteria=session.createCriteria(PaletAction.class);
		  
		  criteria.add(Expression.eq("ID",paletAction.getID()));
		  criteria.add(Expression.eq("carrier", paletAction.getCarrier()));
		  criteria.add(Expression.eq("flightNo",paletAction.getFlightNo()));
		  criteria.add(Expression.eq("flightDate",paletAction.getFlightDate()));
		  criteria.add(Expression.eq("aircraft",paletAction.getAircraft()));
		  criteria.add(Expression.eq("arriving", paletAction.getArriving()));
		  criteria.add(Expression.eq("departing",paletAction.getDeparting()));
		  
		  return criteria.list();
	}
	
}
