package net.myapp.dao.models;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.CreationTimestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="message")
public class Message {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "message_ID")
	private int ID;
	
	@Column(name="type",length = 3,nullable=false)
	private String type;
	
	@JsonIgnore
	@Column(name="text",nullable=false)
	private String text;
	
	@Column(name="receiveDate",nullable=false)
	private Date receiveDate;
	
	@Column(name="status",length = 10,nullable=false)
	private String status;
	
	@JsonIgnore
	@Column(name="reason",nullable=true)
	private String reason;
	

	@JsonIgnore
	@Column(name="reason_user",nullable=true,length=100)
	private String reason_user;
	
	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	//bazaya dusme vaxti
	private Date insertDate;
	
	@Column(name="sender",length = 30,nullable=false)
	private String sender;
	
	@Column(name="subject",nullable=true)
	private String subject;
	
	@Column(name="fromEmail",length = 30,nullable=false)
	private String fromEmail;
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "message")
    @Cascade({CascadeType.SAVE_UPDATE,CascadeType.DELETE,CascadeType.REFRESH})
	private Set<PaletAction> paletActions;
	
	
	
	/*@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "message",targetEntity=MessageError.class)
    @Cascade({CascadeType.SAVE_UPDATE,CascadeType.DELETE,CascadeType.REFRESH})
	private Set<MessageError> messageErrorSet;
	
	*/
	
	public Message(String type, String text, Date date, String status, String reason,String sender,String fromEmail,String subject) {
		super();
		this.type = type;
		this.text = text;
		this.status = status;
		this.reason = reason;
		this.sender=sender;
		this.fromEmail=fromEmail;
		this.subject=subject;
		this.receiveDate=date;

	}

	public Message() {
		super();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getText() {
		return text.trim();
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
		
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getID() {
		return ID;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public Date getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFromEmail() {
		return fromEmail;
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public Set<PaletAction> getPaletActions() {
		return paletActions;
	}

	public void setPaletActions(Set<PaletAction> paletActions) {
		this.paletActions = paletActions;
	}
/*
	public Set<MessageError> getMessageErrorSet() {
		return messageErrorSet;
	}

	public void setMessageErrorSet(Set<MessageError> messageErrorSet) {
		this.messageErrorSet = messageErrorSet;
	}
	
	public void addError(MessageError error){
		if(this.messageErrorSet==null){messageErrorSet=new HashSet<MessageError>();}
		this.messageErrorSet.add(error);
	}
*/
	
	public static List<Message> sortByDate(List<Message>messageList){
		Collections.sort(messageList, new Comparator<Message>(){
		     public int compare(Message m1, Message m2){
		         if(m1.receiveDate == m2.receiveDate)
		             return 0;
		         return m1.receiveDate.compareTo(m2.receiveDate);
		     }
		});
		
		 return messageList;
				
	}

	public String getReason_user() {
		return reason_user;
	}

	public void setReason_user(String reason_user) {
		this.reason_user = reason_user;
	}
	
	public void addPaletAction(PaletAction paletAction){
		if(this.paletActions==null){paletActions=new HashSet<PaletAction>();}
		this.paletActions.add(paletAction);
	}
	

	
}
