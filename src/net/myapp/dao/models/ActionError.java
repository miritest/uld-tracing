package net.myapp.dao.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="action_error")
public class ActionError {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "error_ID")
	private int ID;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="paletAction_ID",nullable=false)
	private PaletAction paletAction;
	
	@Column(name="palet",length = 25,nullable=false)
	private String palet;
	
	@Column(name="action_type",length = 3,nullable=false)
	private String action_type;
	
	@Column(name="flightNo",length = 5,nullable=true)
	private String flightNo;
	
	@Column(name="palet_location",length = 3,nullable=false)
	private String palet_location;
	
	@Column(name="error",nullable=false)
	private String error;

	@Column(name="solved",nullable=true)
	private boolean solved;
	
	
	public ActionError(PaletAction paletAction, String palet, String action_type, String flightNo,
			String palet_location, String text) {
		super();
		
		this.paletAction = paletAction;
		this.palet = palet;
		this.action_type = action_type;
		this.flightNo = flightNo;
		this.palet_location = palet_location;
		this.error = text;
	}

	public ActionError() {
		super();
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}



	public PaletAction getPaletAction() {
		return paletAction;
	}

	public void setPaletAction(PaletAction paletAction) {
		this.paletAction = paletAction;
	}

	public String getPalet() {
		return palet;
	}

	public void setPalet(String palet) {
		this.palet = palet;
	}

	public String getAction_type() {
		return action_type;
	}

	public void setAction_type(String action_type) {
		this.action_type = action_type;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getPalet_location() {
		return palet_location;
	}

	public void setPalet_location(String palet_location) {
		this.palet_location = palet_location;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean isSolved() {
		return solved;
	}

	public void setSolved(boolean solved) {
		this.solved = solved;
	}

	
	
	
	

}
