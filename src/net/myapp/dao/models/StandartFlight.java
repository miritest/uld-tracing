package net.myapp.dao.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="standartFlights")
public class StandartFlight {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "flight_ID")
	private int ID;
	
	@Column(name="Flight",length = 6,nullable=false)
	private String flightCode;

	@Column(name="departure",length = 3,nullable=false)
	private String departure;
	
	@Column(name="destination",length = 3,nullable=false)
	private String destination;

	public StandartFlight(String flightCode, String departure, String destination) {
		super();
		this.flightCode = flightCode;
		this.departure = departure;
		this.destination = destination;
	}

	public StandartFlight() {
		super();
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getFlightCode() {
		return flightCode;
	}

	public void setFlightCode(String flightCode) {
		this.flightCode = flightCode;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	
	
}
