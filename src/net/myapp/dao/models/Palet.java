package net.myapp.dao.models;

import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="palet")
public class Palet {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "palet_ID")
	private int ID;
	
	@Column(name="type",length = 3,nullable=false)
	private String type;
	
	@Column(name="code",length = 5,nullable=false)
	private String code;
	
	@Column(name="owner",length = 2,nullable=false)
	private String owner;
	
	private boolean checked;
	private boolean active;
	
	@Column(name = "reason", nullable = true)
	String reason;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="insert_date",updatable=false)
	private Date insert_date;
	
	
	@Column(name = "position",length = 15, nullable = true)
	String position;
	
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "paletSet")
	private Set<PaletAction> paletActions;

	
	

	public Palet(String type, String code, String owner, boolean checked, boolean active, String reason,
			String position, Set<PaletAction> paletActions) {
		super();
		this.type = type;
		this.code = code;
		this.owner = owner;
		this.checked = checked;
		this.active = active;
		this.reason = reason;
		this.position = position;
		this.paletActions = paletActions;
	}



	public Palet() {
		super();
	}



	public int getID() {
		return ID;
	}

	

	public void setID(int iD) {
		ID = iD;
	}



	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Set<PaletAction> getPaletActions() {
		return paletActions;
	}

	public void setPaletActions(Set<PaletAction> paletActions) {
		this.paletActions = paletActions;
	}
	

	public String getPosition() {
		return position;
	}



	public void setPosition(String position) {
		this.position = position;
	}



	public Date getInsert_date() {
		return insert_date;
	}



	public void setInsert_date(Date insert_date) {
		this.insert_date = insert_date;
	}

	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if(obj instanceof Palet){
			Palet p=(Palet)obj;
			if(this.code.equals(p.code) && this.owner.equals(p.owner))return true;
		}
		return false;
	}


	


	public void SetPalet(Palet palet){
		this.setCode(palet.getCode());
		this.setOwner(palet.getOwner());
		this.setActive(palet.isActive());
		this.setChecked(palet.isChecked());
		this.setType(palet.getType());
		this.setReason(palet.getReason());
	}
	/*public static String toJson(List<Palet>paletList){
		JSONObject json=new JSONObject();
		JSONArray array = new JSONArray();
		JSONObject jsonObject=new JSONObject();

		for (Palet palet : paletList) {
			if(palet.ID!=0){jsonObject.put("ID",palet.ID);}
			if(palet.type!=null){jsonObject.put("type", palet.type);}
			if(palet.code!=null){jsonObject.put("code",palet.code);}
			if(palet.owner!=null){jsonObject.put("owner", palet.owner);}
			if(palet.reason!=null){jsonObject.put("reason",palet.reason);}
			if(palet.insert_date!=null){jsonObject.put("insert_date",palet.insert_date);}
			if(palet.position!=null){jsonObject.put("position", palet.position);}
			jsonObject.put("active", palet.active);
			jsonObject.put("checked",palet.checked);
			array.add(jsonObject);
		}
		json.put("palets", array);
		
		
		
		return json.toJSONString();
		
	}
	*/
	
	
}
