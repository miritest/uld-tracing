package net.myapp.dao.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import net.myapp.utils.ParsingTools;

@Entity
@Table(name="palet_action")
public class PaletAction {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "action_ID")
	private int ID;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "message_ID", nullable = false)
	private Message message;
	
	@OneToMany(fetch = FetchType.LAZY,mappedBy="paletAction")
    @Cascade({CascadeType.SAVE_UPDATE,CascadeType.DELETE,CascadeType.REFRESH})
	private Set<ActionError> actionErrorSet;
	
	
	
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "palet_action_conn",joinColumns = {
			@JoinColumn(name = "action_ID", nullable = false) },
			inverseJoinColumns = { @JoinColumn(name = "palet_ID",
					nullable = false) })
	private Set<Palet> paletSet=new HashSet<Palet>();
	
	@Column(name="carrier",length = 2,nullable=true)
	private String carrier;
	
	@Column(name="flightNo",length = 5,nullable=true)
	private String flightNo;
    
	@Temporal(TemporalType.DATE)
	@Column(name="flightDate", nullable=true)
	private Date flightDate;
	
	@Column(name="aircraft",length = 10,nullable=true)
	private String aircraft;
	
	@Column(name = "arriving", nullable = true,length = 3)
	private String arriving;
	
	@Column(name = "departing", nullable = true,length = 3)
	private String departing;
	
	
	

	public PaletAction(Set<Palet> palets, String carrier, String flightNo, Date flightDate, String aircraft,
			String arriving, String departing) {
		super();
		this.paletSet = palets;
		this.carrier = carrier;
		this.flightNo = flightNo;
		this.flightDate = flightDate;
		this.aircraft = aircraft;
		this.arriving = arriving;
		this.departing = departing;
	}




	public PaletAction() {
		super();
		
		
	}

	public PaletAction(PaletAction paletAction){
		this.paletSet=paletAction.getPaletSet();
		this.carrier = paletAction.getCarrier();
		this.flightNo = paletAction.getFlightNo();
		this.flightDate = paletAction.getFlightDate();
		this.aircraft = paletAction.getAircraft();
		this.arriving = paletAction.getArriving();
		this.departing = paletAction.getDeparting();
	}

	public int getID() {
		return ID;
	}

	public Set<Palet> getPaletSet() {
		return paletSet;
	}

	public void setPaletSet(Set<Palet> palets) {
		this.paletSet = palets;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
		//this.flightDate = null;

	}

	public String getAircraft() {
		return aircraft;
	}

	public void setAircraft(String aircraft) {
		this.aircraft = aircraft;
	}

	public String getArriving() {
		return arriving;
	}

	public void setArriving(String arriving) {
		this.arriving = arriving;
	}

	public String getDeparting() {
		return departing;
	}

	public void setDeparting(String departing) {
		this.departing = departing;
	}
	
	public void addPalet(Palet palet){
		paletSet.add(palet);
	}
	
	public static PaletAction create(PaletAction paletAction, String depart){
		paletAction=new PaletAction();
		if (ParsingTools.containingOnlyLetters(depart)) {
			paletAction.setDeparting(depart);

		}
		return paletAction;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public void setID(int iD) {
		ID = iD;
	}
	
	
	public Set<ActionError> getActionErrorSet() {
		return actionErrorSet;
	}




	public void setActionErrorSet(Set<ActionError> actionErrorSet) {
		this.actionErrorSet = actionErrorSet;
	}
	
	public void addError(ActionError error){
		if(this.actionErrorSet==null){actionErrorSet=new HashSet<ActionError>();}
		this.actionErrorSet.add(error);
	}



	@Override
	public String toString() {
		System.out.println("ID="+this.ID);
		System.out.println("arriving="+this.arriving);
		System.out.println("departing="+this.departing);
		System.out.println("carrier="+this.carrier);
		System.out.println("flightNo="+this.flightNo);
		System.out.println("aircraft="+this.aircraft);

		return super.toString();
	}
}
