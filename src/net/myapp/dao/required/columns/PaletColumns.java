package net.myapp.dao.required.columns;

public class PaletColumns {

	private boolean ID;
	private boolean type;
	private boolean code;
	private boolean owner;
	private boolean checked;
	private boolean active;
	private boolean reason;
	private boolean insert_Date;
	private boolean position;
	
	public PaletColumns(boolean iD, boolean type, boolean code, boolean owner, boolean checked, boolean active,
			boolean reason, boolean insert_Date, boolean position) {
		super();
		ID = iD;
		this.type = type;
		this.code = code;
		this.owner = owner;
		this.checked = checked;
		this.active = active;
		this.reason = reason;
		this.insert_Date = insert_Date;
		this.position = position;
	}

	public PaletColumns() {
		super();
	}

	public boolean isID() {
		return ID;
	}

	public void setID(boolean iD) {
		ID = iD;
	}

	public boolean isType() {
		return type;
	}

	public void setType(boolean type) {
		this.type = type;
	}

	public boolean isCode() {
		return code;
	}

	public void setCode(boolean code) {
		this.code = code;
	}

	public boolean isOwner() {
		return owner;
	}

	public void setOwner(boolean owner) {
		this.owner = owner;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isReason() {
		return reason;
	}

	public void setReason(boolean reason) {
		this.reason = reason;
	}

	public boolean isInsert_Date() {
		return insert_Date;
	}

	public void setInsert_Date(boolean insert_Date) {
		this.insert_Date = insert_Date;
	}

	public boolean isPosition() {
		return position;
	}

	public void setPosition(boolean position) {
		this.position = position;
	}
	
	
	
}
