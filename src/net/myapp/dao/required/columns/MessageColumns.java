package net.myapp.dao.required.columns;

public class MessageColumns {

	private boolean ID;
	private boolean type;
	private boolean text;
	private boolean receiveDate;
	private boolean status;
	private boolean reason;
	private boolean reason_user;
	private boolean insert_date;
	private boolean sender;
	private boolean subject;
	private boolean fromEmail;
	public MessageColumns(boolean iD, boolean type, boolean text, boolean receiveDate, boolean status, boolean reason,
			boolean reason_user, boolean insert_date, boolean sender, boolean subject, boolean fromEmail) {
		super();
		ID = iD;
		this.type = type;
		this.text = text;
		this.receiveDate = receiveDate;
		this.status = status;
		this.reason = reason;
		this.reason_user = reason_user;
		this.insert_date = insert_date;
		this.sender = sender;
		this.subject = subject;
		this.fromEmail = fromEmail;
	}
	public MessageColumns() {
		super();
	}
	public boolean isID() {
		return ID;
	}
	public void setID(boolean iD) {
		ID = iD;
	}
	public boolean isType() {
		return type;
	}
	public void setType(boolean type) {
		this.type = type;
	}
	public boolean isText() {
		return text;
	}
	public void setText(boolean text) {
		this.text = text;
	}
	public boolean isReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(boolean receiveDate) {
		this.receiveDate = receiveDate;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public boolean isReason() {
		return reason;
	}
	public void setReason(boolean reason) {
		this.reason = reason;
	}
	public boolean isReason_user() {
		return reason_user;
	}
	public void setReason_user(boolean reason_user) {
		this.reason_user = reason_user;
	}
	public boolean isInsert_date() {
		return insert_date;
	}
	public void setInsert_date(boolean insert_date) {
		this.insert_date = insert_date;
	}
	public boolean isSender() {
		return sender;
	}
	public void setSender(boolean sender) {
		this.sender = sender;
	}
	public boolean isSubject() {
		return subject;
	}
	public void setSubject(boolean subject) {
		this.subject = subject;
	}
	public boolean isFromEmail() {
		return fromEmail;
	}
	public void setFromEmail(boolean fromEmail) {
		this.fromEmail = fromEmail;
	}
	
	
	
}
