package net.myapp.web.filters;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;

public class RequestEncodingFilter implements javax.servlet.Filter
{
  public RequestEncodingFilter() {}
  
  private String encoding = "UTF-8";
  
  @Override
public void destroy() {}
  
  @Override
public void doFilter(ServletRequest request, javax.servlet.ServletResponse response, FilterChain chain) throws java.io.IOException, ServletException {
    request.setCharacterEncoding(encoding);
    chain.doFilter(request, response);
  }
  
  @Override
public void init(FilterConfig config) throws ServletException {
    encoding = config.getInitParameter("requestEncoding");
  }
}
