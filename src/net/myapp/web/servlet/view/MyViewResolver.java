package net.myapp.web.servlet.view;

import java.util.Locale;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import net.myapp.common.logging.impl.Log;
import net.myapp.common.web.holders.RequestHelper;

public class MyViewResolver extends InternalResourceViewResolver
{
  private String defaultViewParent = "view/jsp/";
  private String ajaxContentPath = null;
  private String defaultViewTemplate = "main";
  private String defaultViewTemplate_admin = "main_admin";





  public MyViewResolver() {}
  




  @Override
public View resolveViewName(String viewName, Locale locale)
    throws Exception
  {
    if ((!viewName.startsWith("redirect:")) && (!viewName.startsWith("forward:")))
    {
      String gppViewParent = defaultViewParent;
      


      if (RequestHelper.isAJAXRequest())
      {
        viewName = gppViewParent + ajaxContentPath + viewName;
        Log.debug("ajax view name : " + viewName);




      }
      
      else {
        System.out.println("main" + viewName);
       
        RequestHelper.setAttribute("partial", viewName);
        Log.debug("view partial  name" + viewName);
        if(viewName.startsWith("admin")){
            viewName = gppViewParent + defaultViewTemplate_admin;

        }
        else {
        viewName = gppViewParent + defaultViewTemplate;}
      }
    }
    





    return super.resolveViewName(viewName, locale);
  }
}
