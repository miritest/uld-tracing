package net.myapp.exceptions;

public class MyException extends Exception{
	

	
	public static String getErrorLocation(String className, String methodName, String additional, Exception e){
		String[] exceptionArray=e.toString().split("\\.");

		if (additional.equals("")) {
			return "class>>"+className+" method>>"+methodName+" exception>>"+exceptionArray[exceptionArray.length-1];
		}
		return "class>>"+className+" method>>"+methodName+" additional>>"+additional+" exception>>"+exceptionArray[exceptionArray.length-1];
	}

	public static String getErrorLocation(String className, String methodName, String additional){

		if (additional.equals("")) {
			return "class>>"+className+" method>>"+methodName;
		}
		return "class>>"+className+" method>>"+methodName+" additional>>"+additional;
	}

	
	
	
	private String reason,reason_user;

	public MyException(String reason, String reason_user){
		this.reason=reason;
		this.reason_user=reason_user;
		
	}
	
	public String getReason() {
		return reason;
	}



	public String getReason_user() {
		return reason_user;
	}


	
	
	
	
}
